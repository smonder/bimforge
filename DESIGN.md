# BIMFORGE DESIGN


## Structure
Bimforge is going to consist of many modules written in languages like C, C++,
Rust, etc, then bound together using python since it is going to allow the use
of plug-ins written by users or community.


## Modules

### Core Module
This is the primary module that is going to provide primary info about the
application like version, what works what not, project context, etc.

### GUI module
This module is going to provide all the necessary UI components like windows,
panels, sidebars, popovers, dialogues, work-surfaces, etc.

### Graphics module
This module is going to provide a rendering pipeline for drawing elements on
screen.

### Project Module
This is the module responsible for handling projects, it is going to provide
methods and classes to handle different project components.

### Plugins Module
This module is going to be responsible for connecting our application with
plugins written by community. In another word, this is the module responsible
for loading plugins at runtime.


## Builtin Plugins

### inspector-page
The purpose of this plugin is to inspect the project tree.

### object-page
This plugin is responsible for modifying the properties of the active object.

### obj-importer
This plugin provides mechanism to read and import 3D models from obj files.

### render-layers-page
This plugin is for debugging only. The purpose of this plugin is to inspect the
render layers of the graphics pipeline.

### tests
This plugin is for debugging only and testing the graphics pipeline.

### test-workspace
This plugin is for debugging only and will be removed soon.


