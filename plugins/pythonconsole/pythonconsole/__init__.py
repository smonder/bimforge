# -*- coding: utf-8 -*-

# __init__.py -- plugin object
#
# Copyright (C) 2006 - Steve Frécinaux
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

# Parts from "Interactive Python-GTK Console" (stolen from epiphany's console.py)
#     Copyright (C), 1998 James Henstridge <james@daa.com.au>
#     Copyright (C), 2005 Adam Hooper <adamh@densi.com>
# Bits from gedit Python Console Plugin
#     Copyrignt (C), 2005 Raphaël Slinckx

import gi

gi.require_version("Bimforge", "1.0")

from gi.repository import GObject, Gtk, Bimforge, Peas, Panel
from .console import PythonConsole
from .config import PythonConsoleConfigWidget

try:
    import gettext

    gettext.bindtextdomain("bimforge")
    gettext.textdomain("bimforge")
    _ = gettext.gettext
except:
    _ = lambda s: s


class PythonConsolePlugin(GObject.Object, Bimforge.WorkspaceAddin):
    __gtype_name__ = "PythonConsolePlugin"

    workspace = GObject.Property(type=Bimforge.Workspace)

    def __init__(self):
        GObject.Object.__init__(self)

    def do_load(self, workspace):
        self.workspace = workspace
        self._console = PythonConsole(
            namespace={
                "__builtins__": __builtins__,
                "Bimforge": Bimforge,
                "workspace": self.workspace,
            }
        )
        self._console.eval(
            'print("You can access the main workspace through '
            "'workspace' :\\n%s\" % workspace)",
            False,
        )
        self._console.set_visible(True)
        workspace.add_page(self._console, Panel.Area.BOTTOM)

    def do_unload(self, workspace):
        self._console.stop()
        # workspace.remove_bottom_panel(self._console)

    def do_update_state(self):
        pass

    def do_create_configure_widget(self):
        config_widget = PythonConsoleConfigWidget(self.plugin_info.get_data_dir())

        return config_widget.configure_widget()


# ex:et:ts=4:

