# BIMFORGE

I first started Bimforge as a general-purposes graphic simulator designed to test
ideas and theories.
Lately, I have decided to make it into a BIM application for linux platforms since
the lake of that type of applications on linux at first and that I wanted to dive
deeply through how BIM applications are mode and how they function.

## What is BIM?
BIM is short for **Building information modeling** which is a process involving
the generation and management of digital representations of physical and
functional characteristics of places.

Building information models (BIMs) are computer files (often but not always in
proprietary formats and containing proprietary data) which can be extracted,
exchanged or networked to support decision-making regarding a built asset.

BIM software is used by individuals, businesses and government agencies who plan,
design, construct, operate and maintain buildings and diverse physical
infrastructures, such as water, refuse, electricity, gas, communication utilities,
roads, railways, bridges, ports and tunnels.

The concept of BIM has been in development since the 1970s, but it only became an
agreed term in the early 2000s. The development of standards and the adoption of
BIM has progressed at different speeds in different countries.
Standards developed in the United Kingdom from 2007 onwards have formed the basis
of the international standard ISO 19650, launched in January 2019.

More info on [Wikipedia](https://en.wikipedia.org/wiki/Building_information_modeling) 

## NOTES
1. Because **Bimforge** is meant to be running primarily on **GNOME**, it copies from
   GNOME Projects. Some lines of code in this project is either **COPIED** or **MODIFIED**
   from GNOME Projects like **Builder**, **TODO**, etc. in order to keep the consistency
   with the Environment.

2. **Bimforge** is written in **C** however language bindings is planned in further
   developments of the project.

3. **Bimforge** by default is implementing **OpenGL** as graphics API. However,
   **Vulkan** support is going to be added in further development.

## Features

 * A simple BIM application focused on a solid default experience
 * Desktop integration including dark-mode and saving session state

## Dependencies

 * GTK 4.11 or newer
 * [Libadwaita 1.4-beta or newer](https://gitlab.gnome.org/GNOME/libadwaita)
 * [Libpanel](https://gitlab.gnome.org/chergert/libpanel)
 * epoxygl

Refer to the [io.sam.bimforge.json](https://gnome.gitlab.org/smonder/bimforge/blob/master/io.sam.bimforge.json) Flatpak manifest for additional details.

## Try it Out!

### Building

We use the Meson (and thereby Ninja) build system for Bimforge. The quickest
way to get going is to do the following:

```sh
meson . ./build
ninja -C ./build
ninja -C ./build install
```

For build options see [meson_options.txt](./meson_options.txt).
