option('development', type: 'boolean', value: true, description: 'If this is a development build')
option('tracing', type: 'boolean', value: true, description: 'Enable application tracing')
