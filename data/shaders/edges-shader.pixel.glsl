// PIXEL_SHADER:
// edges-shader.pixel.glsl:
in vec4 v_Color;
out vec4 a_Color;

void main ()
{
    a_Color = v_Color;
}
