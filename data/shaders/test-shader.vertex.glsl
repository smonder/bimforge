// VERTEX_SHADER:
// test-shader.vertex.glsl:
layout (location = 0) in vec3 a_Position;
layout (location = 1) in vec3 a_Normal;
layout (location = 2) in vec2 a_TextureCoords;

uniform mat4 u_Model;
uniform mat4 u_ViewProjection;
out vec2 v_TextureCoords;
out vec3 v_Normal;

void main ()
{
  v_TextureCoords = a_TextureCoords;
  v_Normal = a_Normal;

  gl_Position = u_ViewProjection * u_Model * vec4 (a_Position, 1.0f);
}
