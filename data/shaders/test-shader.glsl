// VERTEX_SHADER:
// test-shader.glsl:
layout (location = 0) in vec2 a_Position;
layout (location = 1) in vec2 a_TextureCoords;

uniform mat4 u_Model;
uniform mat4 u_ViewProjection;
uniform vec4 a_Color;

out vec4 v_Color;
out vec2 v_TextureCoords;

void main ()
{
  v_Color = a_Color;
  v_TextureCoords = a_TextureCoords;

  gl_Position = u_ViewProjection * u_Model * vec4 (a_Position, 0.0f, 1.0f);
}

// PIXEL_SHADER:
// test-shader.glsl:
in vec4 v_Color;
in vec2 v_TextureCoords;
out vec4 a_Color;

uniform sampler2D u_TextureSample;

void main ()
{
  vec4 tex_Color = texture2D (u_TextureSample, v_TextureCoords);

  a_Color = tex_Color * v_Color;
}
