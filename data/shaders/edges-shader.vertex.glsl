// VERTEX_SHADER:
// edges-shader.vertex.glsl:
layout (location = 0) in vec3 a_Position;
layout (location = 1) in vec4 a_BVColor;

uniform vec4 u_Color;
uniform mat4 u_Model;
uniform mat4 u_ViewProjection;

out vec4 v_Color;
out vec4 v_BVColor;

void main ()
{
    if (a_BVColor.w == 0.0f)
        v_Color = u_Color;
    else
        v_Color = a_BVColor;

    gl_Position = u_ViewProjection * u_Model * vec4 (a_Position, 1.0f);
}
