// PIXEL_SHADER:
// test-shader.pixel.glsl:
in vec2 v_TextureCoords;
in vec3 v_Normal;
out vec4 a_Color;

uniform sampler2D u_TextureSample;

void main ()
{
  vec4 tex_Color = texture (u_TextureSample, v_TextureCoords);

  a_Color = tex_Color;
  // a_Color = vec4 (v_TextureCoords, 0.0f, 1.0f);
}
