/*
 * fp-visualizeui-workspace-addin.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "fp-visualizeui-workspace-addin"

#include "config.h"
#include "bimforge-debug.h"

#include "fp-visualizeui-workspace-addin.h"
#include "fp-viewport3d.h"

struct _FpVisualizeuiWorkspaceAddin
{
  GObject parent_instance;

  BimforgeViewport * viewport_3d;
};


static void
fp_visualizeui_workspace_addin_real_load (BimforgeWorkspaceAddin *addin,
                                          BimforgeWorkspace      *workspace)
{
  FpVisualizeuiWorkspaceAddin *self = (FpVisualizeuiWorkspaceAddin *)addin;

  g_assert (FP_IS_VISUALIZEUI_WORKSPACE_ADDIN (self));
  g_assert (BIMFORGE_IS_WORKSPACE (workspace));

  self->viewport_3d = fp_viewport3d_new ();
  bimforge_workspace_add_viewport (workspace, self->viewport_3d);
}

static void
fp_visualizeui_workspace_addin_real_unload (BimforgeWorkspaceAddin *addin,
                                            BimforgeWorkspace      *workspace)
{
  FpVisualizeuiWorkspaceAddin *self = (FpVisualizeuiWorkspaceAddin *)addin;

  g_assert (FP_IS_VISUALIZEUI_WORKSPACE_ADDIN (self));
  g_assert (BIMFORGE_IS_WORKSPACE (workspace));
}


static void
workspace_addin_iface_init (BimforgeWorkspaceAddinInterface *iface)
{
  iface->load = fp_visualizeui_workspace_addin_real_load;
  iface->unload = fp_visualizeui_workspace_addin_real_unload;
}


G_DEFINE_FINAL_TYPE_WITH_CODE (FpVisualizeuiWorkspaceAddin, fp_visualizeui_workspace_addin, G_TYPE_OBJECT,
                               G_IMPLEMENT_INTERFACE (BIMFORGE_TYPE_WORKSPACE_ADDIN, workspace_addin_iface_init))

static void
fp_visualizeui_workspace_addin_class_init (FpVisualizeuiWorkspaceAddinClass *klass)
{
}

static void
fp_visualizeui_workspace_addin_init (FpVisualizeuiWorkspaceAddin *self)
{
}


