/*
 * fp-viewport3d.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "fp-viewport3d"

#include "config.h"
#include "bimforge-debug.h"

#include <glib/gi18n.h>

#include "fp-viewport3d.h"

struct _FpViewport3D
{
  BimforgeViewport parent_instance;

  /* Canvas event controllers */
  GtkGestureDrag * drag_gesture;
  GtkGestureClick * click_gesture;
  GtkEventControllerScroll * scroll_event;

  /* Forger */
  ForgerContext * context;
  ForgerScene * scene;
  ForgerCamera * camera;
  ForgerContainer * test_entities_container;
  ForgerMaterial * material;
  ForgerMaterial * edges_material;
  ForgerMaterial * spline_material;
};

G_DEFINE_FINAL_TYPE (FpViewport3D, fp_viewport3d, BIMFORGE_TYPE_VIEWPORT)

enum {
  PROP_0,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];
static guint counter;

BimforgeViewport *
fp_viewport3d_new (void)
{
  return g_object_new (FP_TYPE_VIEWPORT3D, NULL);
}



static void
on_click_gesture_pressed_cb (BimforgeCanvas    *canvas,
                             gint             n_press,
                             gdouble          x,
                             gdouble          y,
                             GtkGestureClick *click_gesture)
{
  g_return_if_fail (BIMFORGE_IS_CANVAS (canvas));
  g_return_if_fail (GTK_IS_GESTURE_CLICK (click_gesture));

  bimforge_engine_event_click_pressed (bimforge_canvas_get_engine (canvas), n_press, x, y, click_gesture);
}


static void
_fp_viewport3d_setup_viewport_menu (FpViewport3D * self)
{
  self->viewport_menu = g_menu_new ();
  g_menu_append (self->viewport_menu, "Create Quad", "viewport3d.create-quad");
  g_menu_append (self->viewport_menu, "Create Container", "viewport3d.create-container");
  g_menu_append (self->viewport_menu, "Create Rectangle", "viewport3d.create-rectangle");
}

static void
prepare_shader (FpViewport3D * self)
{
  if (!self->mesh_shader)
    self->mesh_shader =
      bimforge_shader_new_separate_res ("Mesh Shader",
                                      "/io/sam/bimforge/shaders/test-shader.vertex.glsl",
                                      "/io/sam/bimforge/shaders/test-shader.pixel.glsl");

  if (!self->spline_shader)
    self->spline_shader =
      bimforge_shader_new_separate_res ("Spline Shader",
                                      "/io/sam/bimforge/shaders/edges-shader.vertex.glsl",
                                      "/io/sam/bimforge/shaders/edges-shader.pixel.glsl");
}

static void
on_canvas_realize_cb (FpViewport3D *self,
                      BimforgeCanvas *canvas)
{
  BimforgeContext * context;

  BIMFORGE_ENTRY;

  g_assert (FP_IS_VIEWPORT3D (self));
  g_assert (BIMFORGE_IS_CANVAS (canvas));

  context = bimforge_viewport_get_context (BIMFORGE_VIEWPORT (self));

  /* Add Cameras */
  self->default_camera = bimforge_camera_new ("Default Camera",
                                            "The default camera that is created with context.",
                                            45.0f, -1.0f, 1.0f);
  bimforge_context_add_entity (context, BIMFORGE_ENTITY (self->default_camera));
  bimforge_context_bind_camera (context, self->default_camera);

  self->mouse_picker = bimforge_mouse_picker_new_for_context (context);
  bimforge_context_bind_tool (context, BIMFORGE_EVENT_HANDLER (self->mouse_picker));


  GtkGestureClick *click_gesture;

  click_gesture = (GtkGestureClick *)gtk_gesture_click_new ();
  gtk_widget_add_controller (GTK_WIDGET (self), GTK_EVENT_CONTROLLER (click_gesture));
  gtk_event_controller_set_propagation_phase (GTK_EVENT_CONTROLLER (click_gesture), GTK_PHASE_CAPTURE);
  g_signal_connect_swapped (click_gesture, "pressed", G_CALLBACK (on_click_gesture_pressed_cb), self->canvas);

  BimforgeEntity *parent = bimforge_container_new ("Hello Parent");
  bimforge_context_add_entity (context, bimforge_container_new ("hello 1"));
  bimforge_context_add_entity (context, bimforge_container_new ("hello 2"));
  bimforge_context_add_entity (context, parent);
  bimforge_context_add_entity (context, bimforge_container_new ("hello 4"));
  bimforge_context_add_entity (context, bimforge_container_new ("hello 5"));

  bimforge_container_append_child (BIMFORGE_CONTAINER (parent), bimforge_container_new ("hello child 1"));
  bimforge_container_append_child (BIMFORGE_CONTAINER (parent), bimforge_container_new ("hello child 2"));
  bimforge_container_append_child (BIMFORGE_CONTAINER (parent), bimforge_container_new ("hello child 3"));
  bimforge_container_append_child (BIMFORGE_CONTAINER (parent), bimforge_container_new ("hello child 4"));

  BIMFORGE_EXIT;
}

static void
on_context_set_cb (BimforgeViewport * viewport)
{
  FpViewport3D *self = (FpViewport3D *)viewport;
  BimforgeEngine *engine;

  g_assert (FP_IS_VIEWPORT3D (self));

  engine = bimforge_canvas_get_engine (BIMFORGE_CANVAS (self->canvas));
  bimforge_engine_set_context (engine,
                             bimforge_viewport_get_context (viewport));
}

static GMenu *
fp_viewport3d_real_get_header_menu (BimforgeViewport *viewport)
{
  FpViewport3D *self = (FpViewport3D *)viewport;

  g_assert (FP_IS_VIEWPORT3D (self));
  return self->viewport_menu;
}

static void
fp_viewport3d_root (GtkWidget *widget)
{
  FpViewport3D *self = (FpViewport3D *)widget;
  GtkRoot *root;

  g_assert (FP_IS_VIEWPORT3D (self));

  root = gtk_widget_get_root (widget);

  _fp_viewport3d_init_actions (self, GTK_WIDGET (root));
}

static void
fp_viewport3d_finalize (GObject *object)
{
  FpViewport3D *self = (FpViewport3D *)object;

  g_clear_object (&self->default_camera);
  g_clear_object (&self->mouse_picker);

  G_OBJECT_CLASS (fp_viewport3d_parent_class)->finalize (object);
}

static void
fp_viewport3d_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  FpViewport3D *self = FP_VIEWPORT3D (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
fp_viewport3d_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  FpViewport3D *self = FP_VIEWPORT3D (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
fp_viewport3d_class_init (FpViewport3DClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  BimforgeViewportClass *viewport_class = BIMFORGE_VIEWPORT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = fp_viewport3d_finalize;
  object_class->get_property = fp_viewport3d_get_property;
  object_class->set_property = fp_viewport3d_set_property;

  widget_class->root = fp_viewport3d_root;

  viewport_class->get_header_menu = fp_viewport3d_real_get_header_menu;

  counter = 0;
}

static void
fp_viewport3d_init (FpViewport3D *self)
{
  self->canvas = bimforge_canvas_new ();

  bimforge_viewport_set_title (BIMFORGE_VIEWPORT (self), _("3D Viewport"));
  bimforge_viewport_set_icon_name (BIMFORGE_VIEWPORT (self), "folder-pictures-symbolic");
  bimforge_viewport_set_content (BIMFORGE_VIEWPORT (self), self->canvas);

  g_signal_connect (self, "context-set", G_CALLBACK (on_context_set_cb), NULL);
  g_signal_connect_swapped (self->canvas, "realize", G_CALLBACK (on_canvas_realize_cb), self);

  _fp_viewport3d_setup_viewport_menu (self);
}


/*
 * -----< FP_VIEWPORT3D ACTIONS >----- *
 */
static void
fp_viewport3d_create_rectangle_action (GSimpleAction *action,
                                       GVariant      *parameter,
                                       gpointer       user_data)
{
  FpViewport3D *self = user_data;
  BimforgeContext * context;
  BimforgeEntity *rectangle;

  BIMFORGE_ENTRY;

  g_assert (FP_IS_VIEWPORT3D (self));

  gtk_gl_area_make_current (GTK_GL_AREA (self->canvas));

  if (!self->spline_shader)
    prepare_shader (self);

  context = bimforge_viewport_get_context (BIMFORGE_VIEWPORT (self));
  rectangle = bimforge_context_create_rectangle (context,
                                               g_strdup_printf ("Rectangle-%d", counter),
                                               &(GdkRGBA){1.0f, 0.0f, 0.0f, 1.0f},
                                               self->spline_shader,
                                               &(GdkRectangle){0, 0, 200, 400});
  bimforge_context_add_entity (context, rectangle);
  counter ++;
  BIMFORGE_EXIT;
}

static void
fp_viewport3d_create_quad_action (GSimpleAction *action,
                                  GVariant      *parameter,
                                  gpointer       user_data)
{
  FpViewport3D *self = user_data;
  BimforgeContext * context;
  BimforgeEntity *quad;

  BIMFORGE_ENTRY;

  g_assert (FP_IS_VIEWPORT3D (self));

  gtk_gl_area_make_current (GTK_GL_AREA (self->canvas));

  if (!self->mesh_shader)
    prepare_shader (self);

  context = bimforge_viewport_get_context (BIMFORGE_VIEWPORT (self));
  quad = bimforge_context_create_quad (context,
                                     g_strdup_printf ("Mountains-%d", counter),
                                     self->mesh_shader,
                                     "/io/sam/bimforge/textures/mountains.svg",
                                     0.0f, 0.0f, 200.0f);
  bimforge_context_add_entity (context, quad);
  counter ++;
  BIMFORGE_EXIT;
}

static void
fp_viewport3d_create_container_action (GSimpleAction *action,
                                       GVariant      *parameter,
                                       gpointer       user_data)
{
  FpViewport3D *self = user_data;
  BimforgeContext *context;
  BimforgeEntity *container_test;
  BimforgeEntity *quad;

  BIMFORGE_ENTRY;

  g_assert (FP_IS_VIEWPORT3D (self));

  gtk_gl_area_make_current (GTK_GL_AREA (self->canvas));

  context = bimforge_viewport_get_context (BIMFORGE_VIEWPORT (self));
  container_test = bimforge_container_new ("Group01");

  if (!self->mesh_shader)
    prepare_shader (self);

  quad = bimforge_context_create_quad (context,
                                     g_strdup_printf ("contained-%d", counter),
                                     self->mesh_shader,
                                     "/io/sam/bimforge/textures/mountains.svg",
                                     200.0f, -200.0f, 150.0f);

  bimforge_container_append_child (BIMFORGE_CONTAINER (container_test),
                                        quad);
  bimforge_context_add_entity (context, container_test);
  counter ++;

  BIMFORGE_EXIT;
}

static const GActionEntry viewport3d_actions[] = {
	{ "create-quad", fp_viewport3d_create_quad_action },
	{ "create-container", fp_viewport3d_create_container_action },
  { "create-rectangle", fp_viewport3d_create_rectangle_action },
};

void
_fp_viewport3d_init_actions (FpViewport3D *self,
                             GtkWidget    *root)
{
  GSimpleActionGroup * actions;

  g_assert (FP_IS_VIEWPORT3D (self));

  actions = g_simple_action_group_new ();
  g_action_map_add_action_entries (G_ACTION_MAP (actions),
                                   viewport3d_actions,
                                   G_N_ELEMENTS (viewport3d_actions),
                                   self);

  gtk_widget_insert_action_group (root,
                                  "viewport3d",
                                  G_ACTION_GROUP (actions));
}

