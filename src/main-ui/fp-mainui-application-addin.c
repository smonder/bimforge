/*
 * fp-mainui-application-addin.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "fp-mainui-application-addin"

#include "config.h"
#include "bimforge-debug.h"

#include <libforger.h>

#include "fp-mainui-application-addin.h"

struct _FpMainuiApplicationAddin
{
  GObject parent_instance;

  BimforgeApplication * app;
};


static void
fp_mainui_application_addin_activate (BimforgeApplicationAddin *addin,
                                      BimforgeApplication      *app)
{
  FpMainuiApplicationAddin *self = (FpMainuiApplicationAddin *)addin;
  ForgerContext *context;
  PanelWorkspace *window;
  PanelWorkbench *wb;

  g_assert (FP_IS_MAINUI_APPLICATION_ADDIN (self));
  g_assert (BIMFORGE_IS_APPLICATION (app));

  context = forger_context_new ();
  wb = panel_workbench_new ();
	window = PANEL_WORKSPACE (bimforge_workspace_new (context));
  panel_workbench_add_workspace (wb, window);
  panel_workbench_focus_workspace (wb, window);

}

static void
fp_mainui_application_addin_load (BimforgeApplicationAddin *addin,
                                  BimforgeApplication      *app)
{
  FpMainuiApplicationAddin *self = (FpMainuiApplicationAddin *)addin;

  g_assert (FP_IS_MAINUI_APPLICATION_ADDIN (self));
  g_assert (BIMFORGE_IS_APPLICATION (app));

  self->app = app;
}

static void
fp_mainui_application_addin_unload (BimforgeApplicationAddin *addin,
                                    BimforgeApplication      *app)
{
  FpMainuiApplicationAddin *self = (FpMainuiApplicationAddin *)addin;

  g_assert (FP_IS_MAINUI_APPLICATION_ADDIN (self));
  g_assert (BIMFORGE_IS_APPLICATION (app));

  self->app = NULL;
}

static void
bimforge_app_addin_iface_init (BimforgeApplicationAddinInterface *iface)
{
  iface->load = fp_mainui_application_addin_load;
  iface->unload = fp_mainui_application_addin_unload;
  iface->activate = fp_mainui_application_addin_activate;
}

G_DEFINE_FINAL_TYPE_WITH_CODE (FpMainuiApplicationAddin, fp_mainui_application_addin, G_TYPE_OBJECT,
                               G_IMPLEMENT_INTERFACE (BIMFORGE_TYPE_APPLICATION_ADDIN, bimforge_app_addin_iface_init))

static void
fp_mainui_application_addin_class_init (FpMainuiApplicationAddinClass *klass)
{
}

static void
fp_mainui_application_addin_init (FpMainuiApplicationAddin *self)
{
}
