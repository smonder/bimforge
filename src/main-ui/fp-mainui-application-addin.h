/*
 * fp-mainui-application-addin.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <libbimforge.h>

G_BEGIN_DECLS

#define FP_TYPE_MAINUI_APPLICATION_ADDIN (fp_mainui_application_addin_get_type())

G_DECLARE_FINAL_TYPE (FpMainuiApplicationAddin, fp_mainui_application_addin, FP, MAINUI_APPLICATION_ADDIN, GObject)

G_END_DECLS
