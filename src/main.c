/*
 * main.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bimforge"

#include "config.h"

#include <girepository.h>
#include <graphene.h>
#include <glib/gi18n.h>
#include <libbimforge.h>

int
main (int   argc,
      char *argv[])
{
	BimforgeApplication * app;
  const gchar * desktop;
	int ret;

  /* Set up gettext translations */
	bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

  /* Setup various application name/id defaults. */
  g_set_prgname ("bimforge");
  g_set_application_name (_("Bimforge"));

  /* Log some info so it shows up in logs */
  g_message ("Bimforge %s (%s)", PACKAGE_VERSION, BIMFORGE_BUILD_IDENTIFIER);

  /* Log what desktop is being used to simplify tracking down
   * quirks in the future.
   */
  desktop = g_getenv ("XDG_CURRENT_DESKTOP");
  if (desktop == NULL)
    desktop = "unknown";

  g_message ("Initializing with %s desktop and GTK+ %d.%d.%d.",
             desktop,
             gtk_get_major_version (),
             gtk_get_minor_version (),
             gtk_get_micro_version ());


  bimforge_init ();

  app = bimforge_application_new ();
  g_application_add_option_group (G_APPLICATION (app), g_irepository_get_option_group ());
  ret = g_application_run (G_APPLICATION (app), argc, argv);
  /* Force disposal of the application (to help catch cleanup
   * issues at shutdown) and then (hopefully) finalize the app.
   */
  g_object_run_dispose (G_OBJECT (app));
  g_clear_object (&app);

	return ret;
}
