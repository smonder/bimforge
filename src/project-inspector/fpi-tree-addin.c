/*
 * fpi-tree-addin.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "fpi-tree-addin"

#include "config.h"
#include "bimforge-debug.h"

#include <glib/gi18n.h>

#include "fpi-tree-addin.h"
#include "fpi-context-tree.h"

struct _FpiTreeAddin
{
  GObject parent_instance;

  BimforgeTree * context_tree;
  ForgerContext * context;
};

typedef struct
{
  ForgerEntity *entity;
  BimforgeTreeNode *node;
} FindEntityNode;




static BimforgeTreeNode *
create_entity_node (ForgerEntity *entity)
{
  BimforgeTreeNode *child;

  g_assert (FORGER_IS_ENTITY (entity));

  child = bimforge_tree_node_new ();
  bimforge_tree_node_set_item (child, G_OBJECT (entity));
  bimforge_tree_node_set_title (child, bimforge_entity_dup_name (entity));
  bimforge_tree_node_set_icon_name (child, bimforge_entity_get_icon_name (entity));
  bimforge_tree_node_set_destroy_item (child, TRUE);

  if (FORGER_IS_CONTAINER (entity))
    {
      bimforge_tree_node_set_children_possible (child, TRUE);
      bimforge_tree_node_set_expanded_icon_name (child,
                                               bimforge_container_get_expanded_icon_name (BIMFORGE_CONTAINER (entity)));
    }

  return g_steal_pointer (&child);
}

static void
create_node_foreach_entity (ForgerEntity   *entity,
                            BimforgeTreeNode *parent)
{
  BimforgeTreeNode *child;

  g_assert (FORGER_IS_ENTITY (entity));
  g_assert (BIMFORGE_IS_TREE_NODE (parent));

  child = create_entity_node (entity);
  bimforge_tree_node_insert_before (child, parent, NULL);
}

static void
fpi_tree_addin_build_children_async (BimforgeTreeAddin     *addin,
                                     BimforgeTreeNode      *node,
                                     GCancellable        *cancellable,
                                     GAsyncReadyCallback  callback,
                                     gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (BIMFORGE_IS_TREE_ADDIN (addin));
  g_assert (BIMFORGE_IS_TREE_NODE (node));

  task = g_task_new (addin, cancellable, callback, user_data);
  g_task_set_source_tag (task, fpi_tree_addin_build_children_async);
  g_task_set_task_data (task, g_object_ref (node), g_object_unref);

  if (bimforge_tree_node_holds (node, BIMFORGE_TYPE_CONTEXT))
    {
      ForgerContext *context = bimforge_tree_node_get_item (node);
      g_autoptr(BimforgeTreeNode) root_node = NULL;

      root_node = create_entity_node (FORGER_ENTITY (context));
      bimforge_tree_node_set_title (root_node, _("PROJECT"));
      bimforge_tree_node_set_icon_name (root_node, "view-list-symbolic");
      bimforge_tree_node_set_expanded_icon_name (root_node, "view-list-symbolic");
      bimforge_tree_node_set_is_header (root_node, TRUE);


      bimforge_container_foreach_child (BIMFORGE_CONTAINER (context),
                                      (GFunc)create_node_foreach_entity,
                                      node);
    }
  else if (bimforge_tree_node_holds (node, BIMFORGE_TYPE_CONTAINER))
    {
      ForgerEntity *container = bimforge_tree_node_get_item (node);

      bimforge_container_foreach_child (BIMFORGE_CONTAINER (container),
                                      (GFunc)create_node_foreach_entity,
                                      node);

      g_task_return_boolean (task, TRUE);

      return;
    }

  g_task_return_boolean (task, TRUE);
}

static gboolean
fpi_tree_addin_build_children_finish (BimforgeTreeAddin  *addin,
                                      GAsyncResult     *result,
                                      GError          **error)
{
  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (BIMFORGE_IS_TREE_ADDIN (addin));
  g_assert (G_IS_TASK (result));

  return g_task_propagate_boolean (G_TASK (result), error);
}

static gboolean
fpi_tree_addin_node_activated (BimforgeTreeAddin *addin,
                               BimforgeTree      *tree,
                               BimforgeTreeNode  *node)
{
  FpiTreeAddin *self = (FpiTreeAddin *)addin;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (FPI_IS_TREE_ADDIN (self));
  g_assert (BIMFORGE_IS_TREE_NODE (node));

  if (bimforge_tree_node_holds (node, FORGER_TYPE_ENTITY))
    {
      ForgerEntity *entity = bimforge_tree_node_get_item (node);

      /* Ignore directories, we want to expand them */
      if (FORGER_IS_CONTAINER (entity))
        return FALSE;

      forger_context_select_entity (self->context, entity);
      return TRUE;
    }

  return FALSE;
}

static BimforgeTreeNodeVisit
traverse_cb (BimforgeTreeNode *node,
             gpointer        user_data)
{
  FindEntityNode *find = user_data;

  if (bimforge_tree_node_get_parent (node) == NULL)
    return BIMFORGE_TREE_NODE_VISIT_CHILDREN;

  if (bimforge_tree_node_holds (node, FORGER_TYPE_ENTITY))
    {
      ForgerEntity *entity = bimforge_tree_node_get_item (node);

      if (find->entity == entity)
        {
          find->node = node;
          return BIMFORGE_TREE_NODE_VISIT_BREAK;
        }

      if (FORGER_IS_CONTAINER (entity) &&
          forger_container_contains (FORGER_CONTAINER (entity), find->entity))
        return BIMFORGE_TREE_NODE_VISIT_CHILDREN;
    }

  return BIMFORGE_TREE_NODE_VISIT_CONTINUE;
}

static BimforgeTreeNode *
find_entity_node (BimforgeTree   *tree,
                  ForgerEntity *entity)
{
  BimforgeTreeNode *root;
  FindEntityNode find;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (BIMFORGE_IS_TREE (tree));
  g_assert (FORGER_IS_ENTITY (entity));

  root = bimforge_tree_get_root (tree);

  find.entity = entity;
  find.node = NULL;

  bimforge_tree_node_traverse (root,
                             G_PRE_ORDER,
                             G_TRAVERSE_ALL,
                             -1,
                             traverse_cb,
                             &find);

  return find.node;
}

static BimforgeTreeNode *
find_child (BimforgeTreeNode *node,
            ForgerEntity   *entity)
{
  for (BimforgeTreeNode *child = bimforge_tree_node_get_first_child (node);
       child != NULL;
       child = bimforge_tree_node_get_next_sibling (child))
    {
      g_autoptr(ForgerEntity) e = NULL;

      if (!bimforge_tree_node_holds (child, FORGER_TYPE_ENTITY))
        continue;

      e = bimforge_tree_node_get_item (child);

      if (e == entity)
        return child;
    }

  return NULL;
}

static int
node_compare (BimforgeTreeNode *node,
              BimforgeTreeNode *child)
{
  gint cmp;
  const gchar *child_name, *node_name;

  g_assert (BIMFORGE_IS_TREE_NODE (node));
  g_assert (BIMFORGE_IS_TREE_NODE (child));

  child_name = bimforge_tree_node_get_title (child);
  node_name = bimforge_tree_node_get_title (node);

  cmp = g_strcmp0 (child_name, node_name);

  return cmp > 0 ? cmp : 0;
}

static void
on_context_entities_foreach_cb (ForgerEntity *entity,
                                gpointer      user_data)
{
  BimforgeTreeNode *parent = user_data;
  BimforgeTreeNode *node;

  if (!(node = find_child (parent, entity)))
    {
      node = create_entity_node (entity);
      bimforge_tree_node_insert_sorted (parent, node, node_compare);
    }

  parent = node;
  g_message ("visit %s", bimforge_tree_node_get_title (parent));
}

static void
fpi_tree_addin_add_entity (FpiTreeAddin *self,
                           ForgerEntity *entity)
{
  BimforgeTreeNode *parent;

  BIMFORGE_ENTRY;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (FPI_IS_TREE_ADDIN (self));
  g_assert (FORGER_IS_ENTITY (entity));

  parent = bimforge_tree_get_root (self->context_tree);

  forger_context_foreach_entity (self->context,
                                 (GFunc)on_context_entities_foreach_cb,
                                 parent);

  BIMFORGE_EXIT;
}

static void
fpi_tree_addin_remove_entity (FpiTreeAddin *self,
                              ForgerEntity *entity)
{
  BimforgeTreeNode *selected;

  BIMFORGE_ENTRY;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (FPI_IS_TREE_ADDIN (self));
  g_assert (FORGER_IS_ENTITY (entity));

  if ((selected = find_entity_node (self->context_tree, entity)))
    {
      BimforgeTreeNode *parent = bimforge_tree_node_get_parent (selected);

      bimforge_tree_node_remove (parent, selected);
    }

  BIMFORGE_EXIT;
}

static void
on_context_modified_cb (FpiTreeAddin  *self,
                        gboolean       removed,
                        ForgerEntity  *entity,
                        ForgerContext *context)
{
  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (FPI_IS_TREE_ADDIN (self));
  g_assert (FORGER_IS_ENTITY (entity));
  g_assert (FORGER_IS_CONTEXT (context));

  if (removed)
    fpi_tree_addin_remove_entity (self, g_object_ref (entity));
  else
    fpi_tree_addin_add_entity (self, g_object_ref (entity));
}

static gboolean
invalidate_in_idle_cb (BimforgeTree *tree)
{
  bimforge_tree_invalidate_all (tree);
  fpi_context_tree_expand_entities (FPI_CONTEXT_TREE (tree));
  return G_SOURCE_REMOVE;
}

static void
fpi_tree_addin_load (BimforgeTreeAddin *addin,
                     BimforgeTree      *tree)
{
  FpiTreeAddin *self = (FpiTreeAddin *)addin;
  BimforgeWorkspace * ws;

  g_assert (FPI_IS_TREE_ADDIN (self));
  g_assert (BIMFORGE_IS_TREE (tree));

  self->context_tree = tree;
  ws = BIMFORGE_WORKSPACE (gtk_widget_get_ancestor (GTK_WIDGET (self->context_tree), BIMFORGE_TYPE_WORKSPACE));

  g_assert (BIMFORGE_IS_WORKSPACE (ws));
  self->context = bimforge_workspace_get_context (ws);

  g_signal_connect_swapped (self->context,
                            "context-modified",
                            G_CALLBACK (on_context_modified_cb),
                            self);

  if (self->context_tree != NULL)
    g_idle_add_full (G_PRIORITY_LOW,
                     (GSourceFunc) invalidate_in_idle_cb,
                     g_object_ref (self->context_tree),
                     g_object_unref);
}

static void
fpi_tree_addin_unload (BimforgeTreeAddin *addin,
                       BimforgeTree      *tree)
{
  FpiTreeAddin *self = (FpiTreeAddin *)addin;

  g_assert (FPI_IS_TREE_ADDIN (self));
  g_assert (BIMFORGE_IS_TREE (tree));
}

static GdkContentProvider *
fpi_tree_addin_node_draggable (BimforgeTreeAddin *addin,
                               BimforgeTreeNode  *node)
{
  if (bimforge_tree_node_holds (node, FORGER_TYPE_ENTITY))
    {
      ForgerEntity *entity = bimforge_tree_node_get_item (node);

      return gdk_content_provider_new_typed (FORGER_TYPE_ENTITY,
                                             g_object_ref (entity));
    }

  return NULL;
}

static GdkDragAction
fpi_tree_addin_node_droppable (BimforgeTreeAddin *addin,
                               GtkDropTarget   *drop_target,
                               BimforgeTreeNode  *drop_node,
                               GArray          *gtypes)
{
  GdkContentFormats *formats;
  GType container_type = BIMFORGE_TYPE_CONTAINER;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (FPI_IS_TREE_ADDIN (addin));
  g_assert (GTK_IS_DROP_TARGET (drop_target));
  g_assert (!drop_node || BIMFORGE_IS_TREE_NODE (drop_node));
  g_assert (gtypes != NULL);

  g_array_append_val (gtypes, container_type);

  /* Must drop on an entity */
  if (drop_node == NULL ||
      !bimforge_tree_node_holds (drop_node, FORGER_TYPE_ENTITY))
    return 0;

  /* Make sure it's a BIMFORGE_TYPE_CONTAINER */
  if (!(formats = gtk_drop_target_get_formats (drop_target)) ||
      !gdk_content_formats_contain_gtype (formats, container_type))
    return 0;

  return GDK_ACTION_COPY | GDK_ACTION_MOVE;
}


static BimforgeTreeNodeVisit
find_parent_traverse_cb (BimforgeTreeNode *node,
                         gpointer        user_data)
{
  FindEntityNode *find = user_data;

  if (bimforge_tree_node_get_parent (node) == NULL)
    return BIMFORGE_TREE_NODE_VISIT_CHILDREN;

  if (bimforge_tree_node_holds (node, FORGER_TYPE_ENTITY))
    {
      ForgerEntity *entity = bimforge_tree_node_get_item (node);

      if (FORGER_IS_CONTAINER (entity) &&
          bimforge_container_has_child (BIMFORGE_CONTAINER (entity), find->entity))
        return BIMFORGE_TREE_NODE_VISIT_BREAK;
    }

  return BIMFORGE_TREE_NODE_VISIT_CONTINUE;
}

static BimforgeTreeNode *
find_entity_container_node (BimforgeTree   *tree,
                            ForgerEntity *entity)
{
  BimforgeTreeNode *root;
  FindEntityNode find;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (BIMFORGE_IS_TREE (tree));
  g_assert (FORGER_IS_ENTITY (entity));

  root = bimforge_tree_get_root (tree);

  find.entity = entity;
  find.node = NULL;

  bimforge_tree_node_traverse (root,
                             G_PRE_ORDER,
                             G_TRAVERSE_ALL,
                             -1,
                             find_parent_traverse_cb,
                             &find);

  return find.node;
}

static void
fpi_tree_addin_node_dropped_async (BimforgeTreeAddin     *addin,
                                   GtkDropTarget       *drop_target,
                                   BimforgeTreeNode      *drop_node,
                                   GCancellable        *cancellable,
                                   GAsyncReadyCallback  callback,
                                   gpointer             user_data)
{
  FpiTreeAddin *self = (FpiTreeAddin *)addin;
  g_autoptr(GTask) task = NULL;
  const GValue *value;
  GdkDrop *drop;
  GdkDragAction action;
  ForgerEntity *src_entity;
  ForgerEntity *drop_entity;
  BimforgeTreeNode *src_container;

  BIMFORGE_ENTRY;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (FPI_IS_TREE_ADDIN (self));
  g_assert (BIMFORGE_IS_TREE_NODE (drop_node));
  g_assert (bimforge_tree_node_holds (drop_node, FORGER_TYPE_ENTITY));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, fpi_tree_addin_node_dropped_async);

  value = gtk_drop_target_get_value (drop_target);
  drop = gtk_drop_target_get_current_drop (drop_target);
  action = gdk_drop_get_actions (drop);

  g_assert (G_IS_VALUE (value));
  g_assert (gdk_drag_action_is_unique (action));

  if (G_VALUE_HOLDS (value, FORGER_TYPE_ENTITY))
    src_entity = g_value_get_object (value);

  drop_entity = FORGER_ENTITY (bimforge_tree_node_get_item (drop_node));

  g_assert (FORGER_IS_CONTAINER (drop_entity));

  if (action == GDK_ACTION_MOVE &&
      (src_container = find_entity_container_node (self->context_tree, src_entity)) &&
      bimforge_tree_node_holds (src_container, BIMFORGE_TYPE_CONTAINER) &&
      bimforge_container_has_child (BIMFORGE_CONTAINER (src_container), src_entity))
    {
      g_warning ("MOVING ENTITY");
      bimforge_container_move_child (BIMFORGE_CONTAINER (src_container),
                                   src_entity,
                                   BIMFORGE_CONTAINER (drop_entity));
    }
  else if (action == GDK_ACTION_COPY)
    {
      g_warning ("COPYING ENTITY");
      bimforge_container_copy_child (BIMFORGE_CONTAINER (src_container),
                                   src_entity,
                                   BIMFORGE_CONTAINER (drop_entity));
    }
  else
    {
      g_task_return_boolean (task, FALSE);
      BIMFORGE_EXIT;
    }

  g_task_return_boolean (task, TRUE);
  BIMFORGE_EXIT;
}

static gboolean
fpi_tree_addin_node_dropped_finish (BimforgeTreeAddin  *addin,
                                    GAsyncResult     *result,
                                    GError          **error)
{
  gboolean ret;

  BIMFORGE_ENTRY;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (FPI_IS_TREE_ADDIN (addin));
  g_assert (G_IS_TASK (result));

  ret = g_task_propagate_boolean (G_TASK (result), error);

  BIMFORGE_RETURN (ret);
}

static void
fpi_tree_addin_iface_init (BimforgeTreeAddinInterface *iface)
{
  iface->load = fpi_tree_addin_load;
  iface->unload = fpi_tree_addin_unload;
  iface->build_children_async = fpi_tree_addin_build_children_async;
  iface->build_children_finish = fpi_tree_addin_build_children_finish;
  iface->node_activated = fpi_tree_addin_node_activated;
  iface->node_draggable = fpi_tree_addin_node_draggable;
  iface->node_droppable = fpi_tree_addin_node_droppable;
  iface->node_dropped_async = fpi_tree_addin_node_dropped_async;
  iface->node_dropped_finish = fpi_tree_addin_node_dropped_finish;
}


G_DEFINE_FINAL_TYPE_WITH_CODE (FpiTreeAddin, fpi_tree_addin, G_TYPE_OBJECT,
                               G_IMPLEMENT_INTERFACE (BIMFORGE_TYPE_TREE_ADDIN, fpi_tree_addin_iface_init))

static void
fpi_tree_addin_class_init (FpiTreeAddinClass *klass)
{
}

static void
fpi_tree_addin_init (FpiTreeAddin *self)
{
}
