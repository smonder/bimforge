/*
 * fpi-context-tree.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "fpi-context-tree"

#include "config.h"
#include "bimforge-debug.h"

#include "fpi-context-tree.h"

struct _FpiContextTree
{
  BimforgeTree parent_instance;
};

G_DEFINE_FINAL_TYPE (FpiContextTree, fpi_context_tree, BIMFORGE_TYPE_TREE)


/*
 * -----< G_OBJECT CLASS IMPLEMENTATION >----- *
 */

static void
fpi_context_tree_class_init (FpiContextTreeClass *klass)
{
}

static void
fpi_context_tree_init (FpiContextTree *self)
{
}


/*
 * -----< CONSTRUCTORS >----- *
 */
GtkWidget *
fpi_context_tree_new (void)
{
  return g_object_new (FPI_TYPE_CONTEXT_TREE, NULL);
}


/*
 * -----< UTILITIES >----- *
 */
static BimforgeTreeNodeVisit
locate_context_nodes (BimforgeTreeNode *node,
                      gpointer        user_data)
{
  BimforgeTreeNode **out_node = user_data;

  if (bimforge_tree_node_holds (node, FORGER_TYPE_ENTITY))
    {
      *out_node = node;
      return BIMFORGE_TREE_NODE_VISIT_BREAK;
    }

  return BIMFORGE_TREE_NODE_VISIT_CONTINUE;
}

void
fpi_context_tree_expand_entities (FpiContextTree *self)
{
  BimforgeTreeNode *root;
  BimforgeTreeNode *node = NULL;

  g_return_if_fail (FPI_IS_CONTEXT_TREE (self));

  if (!(root = bimforge_tree_get_root (BIMFORGE_TREE (self))))
    return;

  bimforge_tree_node_traverse (root,
                             G_PRE_ORDER,
                             G_TRAVERSE_ALL,
                             1,
                             locate_context_nodes,
                             &node);

  if (node != NULL)
    bimforge_tree_expand_node (BIMFORGE_TREE (self), node);
}

static void
fpi_context_tree_expand_cb (GObject      *object,
                            GAsyncResult *result,
                            gpointer      user_data)
{
  FpiContextTree *self = (FpiContextTree *)object;
  g_autoptr(BimforgeTreeNode) root = user_data;
  g_autoptr(GError) error = NULL;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (FPI_IS_CONTEXT_TREE (self));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (BIMFORGE_IS_TREE_NODE (root));

  if (bimforge_tree_expand_node_finish (BIMFORGE_TREE (self), result, &error))
    fpi_context_tree_expand_entities (self);
}

void
fpi_context_tree_set_context (FpiContextTree *self,
                              ForgerContext  *context)
{
  g_autoptr(BimforgeTreeNode) root = NULL;

  BIMFORGE_ENTRY;

  g_return_if_fail (BIMFORGE_IS_MAIN_THREAD ());
  g_return_if_fail (FPI_IS_CONTEXT_TREE (self));
  g_return_if_fail (!context || FORGER_IS_CONTEXT (context));

  if (!context)
    BIMFORGE_EXIT;

  root = bimforge_tree_node_new ();
  bimforge_tree_node_set_item (root, context);
  bimforge_tree_set_root (BIMFORGE_TREE (self), root);

  bimforge_tree_expand_node_async (BIMFORGE_TREE (self),
                                 root, NULL,
                                 fpi_context_tree_expand_cb,
                                 g_object_ref (root));
}


static BimforgeTreeNode *
fpi_context_tree_get_entities (FpiContextTree *self)
{
  BimforgeTreeNode *entities = NULL;

  g_assert (FPI_IS_CONTEXT_TREE (self));

  bimforge_tree_node_traverse (bimforge_tree_get_root (BIMFORGE_TREE (self)),
                             G_PRE_ORDER,
                             G_TRAVERSE_ALL,
                             1,
                             locate_context_nodes,
                             &entities);

  return entities;
}

typedef struct
{
  FpiContextTree * tree;
  BimforgeTreeNode * node;
  ForgerEntity *   entity;
} Reveal;

static void reveal_next (Reveal *r);

static void
reveal_free (Reveal *r)
{
  g_clear_object (&r->tree);
  g_clear_object (&r->node);
  g_clear_object (&r->entity);
  g_free (r);
}

static void
reveal_next_cb (GObject      *object,
                GAsyncResult *result,
                gpointer      user_data)
{
  BimforgeTree *tree = (BimforgeTree *)object;
  Reveal *r = user_data;

  g_assert (BIMFORGE_IS_TREE (tree));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (r != NULL);
  g_assert (FPI_IS_CONTEXT_TREE (r->tree));
  g_assert (BIMFORGE_IS_TREE_NODE (r->node));
  g_assert (FORGER_IS_ENTITY (r->entity));

  if (!bimforge_tree_expand_node_finish (tree, result, NULL))
    {
      reveal_free (r);
      return;
    }

  for (BimforgeTreeNode *child = bimforge_tree_node_get_first_child (r->node);
       child != NULL;
       child = bimforge_tree_node_get_next_sibling (child))
    {
      ForgerEntity *entity;

      if (bimforge_tree_node_holds (child, FORGER_TYPE_ENTITY) &&
          (entity = bimforge_tree_node_get_item (child)) &&
          FORGER_IS_ENTITY (entity))
      {
        if (FORGER_IS_CONTAINER (entity))
          {
            g_set_object (&r->node, child);
            reveal_next (r);
            return;
          }
        else
          {
            bimforge_tree_set_selected_node (BIMFORGE_TREE (r->tree), child);
            gtk_widget_grab_focus (GTK_WIDGET (r->tree));
            break;
          }
      }
    }

  reveal_free (r);
}

static void
reveal_next (Reveal *r)
{
  g_autoptr(ForgerEntity) entity = NULL;

  g_assert (r != NULL);
  g_assert (FPI_IS_CONTEXT_TREE (r->tree));
  g_assert (BIMFORGE_IS_TREE_NODE (r->node));
  g_assert (FORGER_IS_ENTITY (r->entity));

  if (!bimforge_tree_node_holds (r->node, FORGER_TYPE_ENTITY) ||
      !(entity = bimforge_tree_node_get_item (r->node)) ||
      !FORGER_IS_ENTITY (entity))
    goto failure;

  if (FORGER_IS_CONTAINER (entity))
    {
      /* If this node cannot have children, then there is no way we
       * can expect to find the child there.
       */
      if (!bimforge_tree_node_get_children_possible (r->node))
        goto failure;

      bimforge_tree_expand_node_async (BIMFORGE_TREE (r->tree),
                                     r->node,
                                     NULL,
                                     reveal_next_cb,
                                     r);

      return;
    }
  else
    {
      bimforge_tree_set_selected_node (BIMFORGE_TREE (r->tree), r->node);
      gtk_widget_grab_focus (GTK_WIDGET (r->tree));
    }

failure:
  reveal_free (r);
}

void
fpi_context_tree_reveal (FpiContextTree *self,
                         ForgerEntity   *entity)
{
  BimforgeTreeNode *entities;
  Reveal *r;

  g_return_if_fail (FPI_IS_CONTEXT_TREE (self));
  g_return_if_fail (!entity || FORGER_IS_ENTITY (entity));

  if (entity == NULL)
    return;

  entities = fpi_context_tree_get_entities (self);

  if (!BIMFORGE_IS_TREE_NODE (entities))
    return;

  r = g_new0 (Reveal, 1);
  r->tree = g_object_ref (self);
  r->node = g_object_ref (entities);
  r->entity = g_object_ref (entity);

  reveal_next (g_steal_pointer (&r));
}
