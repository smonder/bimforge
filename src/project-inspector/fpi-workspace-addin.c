/*
 * fpi-workspace-addin.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "fpi-workspace-addin"

#include "config.h"
#include "bimforge-debug.h"

#include <glib/gi18n.h>

#include "fpi-workspace-addin.h"
#include "fpi-context-tree.h"

struct _FpiWorkspaceAddin
{
  BimforgePage parent_instance;

  BimforgeTree * context_tree;
};


static void
fpi_workspace_addin_load (BimforgeWorkspaceAddin *addin,
                          BimforgeWorkspace      *workspace)
{
  FpiWorkspaceAddin *self = (FpiWorkspaceAddin *)addin;
  ForgerContext *context;

  g_assert (FPI_IS_WORKSPACE_ADDIN (self));
  g_assert (BIMFORGE_IS_WORKSPACE (workspace));

  self->context_tree = g_object_new (FPI_TYPE_CONTEXT_TREE,
                                     "kind", "project-inspector",
                                     "vexpand", TRUE,
                                     NULL);

  context = bimforge_workspace_get_context (workspace);

  fpi_context_tree_set_context (FPI_CONTEXT_TREE (self->context_tree), context);

  bimforge_workspace_add_page (workspace, BIMFORGE_PAGE (self), PANEL_AREA_START);
  bimforge_page_append_child (BIMFORGE_PAGE (self), GTK_WIDGET (self->context_tree));
  fpi_context_tree_reveal (FPI_CONTEXT_TREE (self->context_tree),
                           FORGER_ENTITY (context));
}

static void
fpi_workspace_addin_unload (BimforgeWorkspaceAddin *addin,
                            BimforgeWorkspace      *workspace)
{
  FpiWorkspaceAddin *self = (FpiWorkspaceAddin *)addin;

  g_assert (FPI_IS_WORKSPACE_ADDIN (self));
  g_assert (BIMFORGE_IS_WORKSPACE (workspace));
}

static void
workspace_addin_iface_init (BimforgeWorkspaceAddinInterface *iface)
{
  iface->load = fpi_workspace_addin_load;
  iface->unload = fpi_workspace_addin_unload;
}

G_DEFINE_FINAL_TYPE_WITH_CODE (FpiWorkspaceAddin, fpi_workspace_addin, BIMFORGE_TYPE_PAGE,
                               G_IMPLEMENT_INTERFACE (BIMFORGE_TYPE_WORKSPACE_ADDIN, workspace_addin_iface_init))


static gboolean
fpi_workspace_addin_grab_focus (GtkWidget *widget)
{
  return gtk_widget_grab_focus (GTK_WIDGET (FPI_WORKSPACE_ADDIN (widget)->context_tree));
}

static void
fpi_workspace_addin_dispose (GObject *object)
{
  FpiWorkspaceAddin *self = (FpiWorkspaceAddin *)object;
  GtkWidget *child = NULL;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (self))) != NULL)
    gtk_widget_unparent (child);

  G_OBJECT_CLASS (fpi_workspace_addin_parent_class)->dispose (object);
}

static void
fpi_workspace_addin_class_init (FpiWorkspaceAddinClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = fpi_workspace_addin_dispose;

  widget_class->grab_focus = fpi_workspace_addin_grab_focus;
}

static void
fpi_workspace_addin_init (FpiWorkspaceAddin *self)
{
  bimforge_page_set_title (BIMFORGE_PAGE (self), _("Project Inspector"));
  bimforge_page_set_icon_name (BIMFORGE_PAGE (self), "view-list-symbolic");
}
