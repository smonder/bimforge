/* bimforge-types.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(BIMFORGE_INSIDE) && !defined(BIMFORGE_COMPILATION)
#error "Only <libbimforge.h> can be included directly."
#endif

#include <glib.h>

G_BEGIN_DECLS

typedef enum
{
  BIMFORGE_TREE_NODE_VISIT_BREAK    = 0,
  BIMFORGE_TREE_NODE_VISIT_CONTINUE = 0x1,
  BIMFORGE_TREE_NODE_VISIT_CHILDREN = 0x3,
} BimforgeTreeNodeVisit;

typedef enum
{
  BIMFORGE_TREE_NODE_FLAGS_NONE       = 0,
  BIMFORGE_TREE_NODE_FLAGS_DESCENDANT = 1 << 0,
  BIMFORGE_TREE_NODE_FLAGS_ADDED      = 1 << 1,
  BIMFORGE_TREE_NODE_FLAGS_CHANGED    = 1 << 2,
  BIMFORGE_TREE_NODE_FLAGS_REMOVED    = 1 << 3,
} BimforgeTreeNodeFlags;

G_END_DECLS
