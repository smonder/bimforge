/*
 * bimforge-application-addin.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(BIMFORGE_INSIDE) && !defined(BIMFORGE_COMPILATION)
#error "Only <libbimforge.h> can be included directly."
#endif

#include "bimforge-application.h"
#include "bimforge-version-macros.h"

G_BEGIN_DECLS

#define BIMFORGE_TYPE_APPLICATION_ADDIN (bimforge_application_addin_get_type ())

BIMFORGE_AVAILABLE_IN_ALL
G_DECLARE_INTERFACE (BimforgeApplicationAddin, bimforge_application_addin, BIMFORGE, APPLICATION_ADDIN, GObject)

struct _BimforgeApplicationAddinInterface
{
  GTypeInterface parent;

  void    (* load)     (BimforgeApplicationAddin * addin,
                        BimforgeApplication *      app);

  void    (* unload)   (BimforgeApplicationAddin * addin,
                        BimforgeApplication *      app);

  void    (* activate) (BimforgeApplicationAddin * addin,
                        BimforgeApplication *      app);
};



BIMFORGE_AVAILABLE_IN_ALL
void    bimforge_application_addin_load     (BimforgeApplicationAddin * self,
                                           BimforgeApplication *      app);

BIMFORGE_AVAILABLE_IN_ALL
void    bimforge_application_addin_unload   (BimforgeApplicationAddin * self,
                                           BimforgeApplication *      app);

BIMFORGE_AVAILABLE_IN_ALL
void    bimforge_application_addin_activate (BimforgeApplicationAddin * self,
                                           BimforgeApplication *      app);

G_END_DECLS
