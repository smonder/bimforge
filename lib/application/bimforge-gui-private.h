/*
 * bimforge-page-private.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "bimforge-page.h"
#include "bimforge-viewport.h"

G_BEGIN_DECLS

GList *     _bimforge_page_get_link                (BimforgePage *        self);
void        _bimforge_page_set_context             (BimforgePage *        self,
                                                    ForgerContext *       context);

GList *     _bimforge_viewport_get_link            (BimforgeViewport *    self);
void        _bimforge_viewport_set_context         (BimforgeViewport *    self,
                                                    ForgerContext *       context);

G_END_DECLS
