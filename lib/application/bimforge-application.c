/*
 * bimforge-application.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bimforge-application"

#include "config.h"

#include <girepository.h>
#include <libbimforge.h>

#include "bimforge-application-private.h"
#include "bimforge-workspace.h"

G_DEFINE_TYPE (BimforgeApplication, bimforge_application, PANEL_TYPE_APPLICATION)


G_GNUC_NULL_TERMINATED
static gboolean
bimforge_application_load_all_typelibs (GError **error, ...)
{
  g_autoptr(GString) msg = g_string_new (NULL);
  const char *typelib;
  gboolean had_failure = FALSE;
  va_list args;

  va_start (args, error);
  while ((typelib = va_arg (args, const char *)))
    {
      const char *version = va_arg (args, const char *);
      g_autoptr(GError) local_error = NULL;

      if (!g_irepository_require (NULL, typelib, version, 0, &local_error))
        {
          if (msg->len)
            g_string_append (msg, "; ");
          g_string_append (msg, local_error->message);
          had_failure = TRUE;
        }
    }
  va_end (args);

  if (had_failure)
    {
      g_set_error_literal (error,
                           G_IREPOSITORY_ERROR,
                           G_IREPOSITORY_ERROR_TYPELIB_NOT_FOUND,
                           msg->str);
      return FALSE;
    }

  return TRUE;
}

static void
bimforge_application_load_typelibs (BimforgeApplication *self)
{
  g_autoptr(GError) error = NULL;

  BIMFORGE_ENTRY;

  g_assert (BIMFORGE_IS_APPLICATION (self));

  g_irepository_prepend_search_path (TYPELIB_DIR);

  if (!bimforge_application_load_all_typelibs (&error,
                                             "Gio", "2.0",
                                             "GLib", "2.0",
                                             "Gtk", "4.0",
                                             "Bimforge", "1.0",
                                             NULL))
    {
      g_critical ("Cannot enable Python 3 plugins: %s", error->message);
      self->loaded_typelibs = FALSE;
    }
  else
    self->loaded_typelibs = TRUE;

  BIMFORGE_EXIT;
}

static void
on_application_activate_cb (PeasExtensionSet *set,
                            PeasPluginInfo   *info,
                            GObject          *extension,
                            gpointer          user_data)
{
  BimforgeApplication *self = user_data;
  BimforgeApplicationAddin *addin = (BimforgeApplicationAddin*)extension;

  g_assert (BIMFORGE_IS_APPLICATION (self));
  g_assert (BIMFORGE_IS_APPLICATION_ADDIN (addin));
  g_assert (PEAS_IS_EXTENSION_SET (set));
  g_assert (info != NULL);

  bimforge_application_addin_activate (addin, self);
}

static void
bimforge_application_activate (GApplication *app)
{
  BimforgeApplication *self = (BimforgeApplication *)app;
	GtkWindow *window;

	g_assert (BIMFORGE_IS_APPLICATION (self));

	window = gtk_application_get_active_window (GTK_APPLICATION (app));
	gtk_window_present (GTK_WINDOW (window));

  bimforge_init_styling ();

  if (self->addins != NULL)
    peas_extension_set_foreach (self->addins, on_application_activate_cb, self);
}

static void
bimforge_application_startup (GApplication *app)
{
  BimforgeApplication *self = (BimforgeApplication *)app;

  g_assert (BIMFORGE_IS_APPLICATION (self));

  G_APPLICATION_CLASS (bimforge_application_parent_class)->startup (app);

  _bimforge_application_load_plugins (self);
}

static void
bimforge_application_shutdown (GApplication *app)
{
  BimforgeApplication *self = (BimforgeApplication *)app;

  g_assert (BIMFORGE_IS_APPLICATION (self));

  _bimforge_application_unload_addins (self);

  g_clear_pointer (&self->plugin_settings, g_hash_table_unref);

  G_APPLICATION_CLASS (bimforge_application_parent_class)->shutdown (app);
}

static void
bimforge_application_dispose (GObject *object)
{
  BimforgeApplication *self = (BimforgeApplication *)object;

  g_clear_pointer (&self->plugin_settings, g_hash_table_unref);
  g_clear_pointer (&self->plugin_gresources, g_hash_table_unref);
  g_clear_pointer (&self->css_providers, g_hash_table_unref);
  g_clear_object (&self->addins);

  G_OBJECT_CLASS (bimforge_application_parent_class)->dispose (object);
}

static void
bimforge_application_class_init (BimforgeApplicationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  object_class->dispose = bimforge_application_dispose;

	app_class->activate = bimforge_application_activate;
  app_class->startup = bimforge_application_startup;
  app_class->shutdown = bimforge_application_shutdown;
}

static void
bimforge_application_init (BimforgeApplication *self)
{
  self->plugin_gresources = g_hash_table_new_full (g_str_hash, g_str_equal, g_free,
                                                   (GDestroyNotify)g_resource_unref);
  self->css_providers = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_object_unref);

  g_application_set_default (G_APPLICATION (self));
  gtk_window_set_default_icon_name ("io.sam.bimforge");

  /* Make sure we've loaded typelibs into process for early access */
  bimforge_application_load_typelibs (self);

  _bimforge_application_init_actions (self);
}


BimforgeApplication *
bimforge_application_new (void)
{
  BimforgeApplication * app;

	app =  g_object_new (BIMFORGE_TYPE_APPLICATION,
	                     "application-id", BIMFORGE_APP_ID,
	                     "flags", G_APPLICATION_DEFAULT_FLAGS,
                       "resource-base-path", "/io/sam/bimforge",
	                     NULL);

  _bimforge_application_load_plugins_for_startup (app);
  _bimforge_application_load_addins (app);

  return g_steal_pointer (&app);
}


/* COPIED FROM GNOME BUILDER:
 * _ide_application_add_resources ();
 * by: Chritian Hergert
 */
static GtkCssProvider *
get_css_provider (BimforgeApplication *self,
                  const char        *key)
{
  GtkCssProvider *ret;

  g_assert (BIMFORGE_IS_APPLICATION (self));
  g_assert (key != NULL);

  if (!(ret = g_hash_table_lookup (self->css_providers, key)))
    {
      ret = gtk_css_provider_new ();
      gtk_style_context_add_provider_for_display (gdk_display_get_default (),
                                                  GTK_STYLE_PROVIDER (ret),
                                                  GTK_STYLE_PROVIDER_PRIORITY_USER-1);
      g_hash_table_insert (self->css_providers, g_strdup (key), ret);
    }

  return ret;
}

void
_bimforge_application_add_resources (BimforgeApplication *self,
                                   const char        *resource_path)
{
  g_autoptr(GError) error = NULL;
  g_autofree gchar *menu_path = NULL;
  g_autofree gchar *css_path = NULL;

  g_assert (BIMFORGE_IS_APPLICATION (self));
  g_assert (resource_path != NULL);

  /* We use interned strings for hash table keys */
  resource_path = g_intern_string (resource_path);

  if (g_str_has_prefix (resource_path, "resource://"))
    {
      g_autoptr(GBytes) bytes = NULL;

      css_path = g_build_filename (resource_path + strlen ("resource://"), "style.css", NULL);
      bytes = g_resources_lookup_data (css_path, 0, NULL);

      if (bytes != NULL)
        {
          GtkCssProvider *provider = get_css_provider (self, resource_path);
          g_debug ("Loading CSS from resource path %s", css_path);
          gtk_css_provider_load_from_resource (provider, css_path);
        }
    }
  else
    {
      css_path = g_build_filename (resource_path, "style.css", NULL);

      if (g_file_test (css_path, G_FILE_TEST_IS_REGULAR))
        {
          GtkCssProvider *provider = get_css_provider (self, resource_path);
          g_debug ("Loading CSS from file path %s", css_path);
          gtk_css_provider_load_from_path (provider, css_path);
        }
    }
}


