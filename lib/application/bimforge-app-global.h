/*
 * bimforge-app-global.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(BIMFORGE_INSIDE) && !defined(BIMFORGE_COMPILATION)
#error "Only <libbimforge.h> can be included directly."
#endif

#include <gio/gio.h>

#include "bimforge-version-macros.h"

G_BEGIN_DECLS

#define BIMFORGE_IS_MAIN_THREAD() (g_thread_self() == bimforge_get_main_thread())

BIMFORGE_AVAILABLE_IN_ALL
GThread * bimforge_get_main_thread (void);

BIMFORGE_AVAILABLE_IN_1_0
GObject * g_object_clone (GObject *src);


static inline gboolean
bimforge_error_ignore (const GError *error)
{
  return g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED) ||
         g_error_matches (error, G_IO_ERROR, G_IO_ERROR_NOT_SUPPORTED);
}

G_END_DECLS
