/* bimforge-page.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bimforge-page"

#include "config.h"
#include "bimforge-debug.h"

#include "bimforge-gui-private.h"

typedef struct
{
  GList link;

  ForgerContext * context;

  gchar * title;
  gchar * icon_name;

  GtkBox * contents;
} BimforgePagePrivate;


static void  buildable_iface_init (GtkBuildableIface *iface);

G_DEFINE_ABSTRACT_TYPE_WITH_CODE (BimforgePage, bimforge_page, GTK_TYPE_WIDGET,
                                  G_ADD_PRIVATE (BimforgePage)
                                  G_IMPLEMENT_INTERFACE (GTK_TYPE_BUILDABLE, buildable_iface_init))

enum {
  PROP_0,
  PROP_CONTEXT,
  PROP_TITLE,
  PROP_ICON_NAME,
  PROP_ORIENTATION,
  N_PROPS
};

enum {
  SIGNAL_CONTEXT_SET,
  N_SIGNALS
};

static GParamSpec *properties [N_PROPS];
static guint signals [N_SIGNALS];
static GtkBuildableIface *parent_buildable_iface;



/*
 * -----< GTK_BUILDABLE INTERFACE IMPLEMENTATION >----- *
 */
static void
bimforge_page_add_child (GtkBuildable *buildable,
                     GtkBuilder   *builder,
                     GObject      *child,
                     const gchar  *type)
{
  BimforgePage *self = (BimforgePage *)buildable;
  BimforgePagePrivate *priv = bimforge_page_get_instance_private (self);

  g_assert (BIMFORGE_IS_PAGE (self));

  if (!GTK_IS_WIDGET (child))
    g_assert_not_reached ();

  if (g_strcmp0 (type, "content") == 0)
    gtk_box_append (priv->contents, GTK_WIDGET (child));
  else
    parent_buildable_iface->add_child (buildable, builder, child, type);
}

static void
buildable_iface_init (GtkBuildableIface *iface)
{
  parent_buildable_iface = g_type_interface_peek_parent (iface);

  iface->add_child = bimforge_page_add_child;
}


/*
 * -----< G_OBJECT CLASS IMPLEMENTATION >----- *
 */
static void
bimforge_page_finalize (GObject *object)
{
  BimforgePage *self = (BimforgePage *)object;
  BimforgePagePrivate *priv = bimforge_page_get_instance_private (self);

  g_clear_pointer (&priv->title, g_free);
  g_clear_pointer (&priv->icon_name, g_free);

  g_clear_weak_pointer (&priv->context);

  G_OBJECT_CLASS (bimforge_page_parent_class)->finalize (object);
}

static void
bimforge_page_dispose (GObject *object)
{
  BimforgePage *self = (BimforgePage *)object;
  BimforgePagePrivate *priv = bimforge_page_get_instance_private (self);

  g_clear_pointer ((GtkWidget **)&priv->contents, gtk_widget_unparent);

  G_OBJECT_CLASS (bimforge_page_parent_class)->dispose (object);
}

static void
bimforge_page_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  BimforgePage *self = BIMFORGE_PAGE (object);

  switch (prop_id)
    {
    case PROP_TITLE:
      g_value_set_string (value, bimforge_page_get_title (self));
      break;

    case PROP_ICON_NAME:
      g_value_set_string (value, bimforge_page_get_icon_name (self));
      break;

    case PROP_CONTEXT:
      g_value_set_object (value, bimforge_page_get_context (self));
      break;

    case PROP_ORIENTATION:
      g_value_set_enum (value, bimforge_page_get_orientation (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bimforge_page_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  BimforgePage *self = BIMFORGE_PAGE (object);

  switch (prop_id)
    {
    case PROP_TITLE:
      bimforge_page_set_title (self, g_value_get_string (value));
      break;

    case PROP_ICON_NAME:
      bimforge_page_set_icon_name (self, g_value_get_string (value));
      break;

    case PROP_ORIENTATION:
      bimforge_page_set_orientation (self, g_value_get_enum (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bimforge_page_class_init (BimforgePageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = bimforge_page_dispose;
  object_class->finalize = bimforge_page_finalize;
  object_class->get_property = bimforge_page_get_property;
  object_class->set_property = bimforge_page_set_property;

  /**
   * BimforgePage:context:
   *
   * The "context" property is the #ForgerContext linked with %self.
   *
   * Since: 0.1
   */
  properties [PROP_CONTEXT] =
    g_param_spec_object ("context", NULL, NULL,
                         FORGER_TYPE_CONTEXT,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  /**
   * BimforgePage:title:
   *
   * The "title" property is the title of the page that is
   * shown on top of it in the #BimforgeSurface that the page
   * is attached to.
   *
   * Since: 0.1
   */
  properties [PROP_TITLE] =
    g_param_spec_string ("title",
                         "Title",
                         "The title of the page.",
                         "Page",
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  /**
   * BimforgePage:icon-name:
   *
   * The "icon-name" property is the icon name of the page that is
   * shown on the page switcher of the #BimforgeSurface that the page
   * is attached to.
   *
   * Since: 0.1
   */
  properties [PROP_ICON_NAME] =
    g_param_spec_string ("icon-name",
                         "Icon Name",
                         "The icon name of the page.",
                         "document-page-setup-symbolic",
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  /**
   * BimforgePage:orientation:
   *
   * The "orientation" property is the orientation of the page layout
   * whether it is horizontal or vertical.
   *
   * Since: 0.1
   */
  properties [PROP_ORIENTATION] =
    g_param_spec_enum ("orientation",
                       "Orientation",
                       "The orientation of the page layout.",
                       GTK_TYPE_ORIENTATION, GTK_ORIENTATION_VERTICAL,
                       (G_PARAM_READWRITE | G_PARAM_CONSTRUCT | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  /**
   * BimforgePage::context-set:
   *
   * The "context-set" signal is emitted after the context property
   * of @self is being set.
   *
   * Since: 0.1
   */
  signals [SIGNAL_CONTEXT_SET] =
    g_signal_newv ("context-set",
                   G_TYPE_FROM_CLASS (klass),
                   G_SIGNAL_RUN_LAST | G_SIGNAL_NO_HOOKS | G_SIGNAL_NO_RECURSE,
                   NULL, NULL, NULL, g_cclosure_marshal_generic,
                   G_TYPE_NONE, 0, NULL);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
  gtk_widget_class_set_accessible_role (widget_class, GTK_ACCESSIBLE_ROLE_GROUP);
  gtk_widget_class_set_css_name (widget_class, "page");
}

static void
bimforge_page_init (BimforgePage *self)
{
  BimforgePagePrivate *priv = bimforge_page_get_instance_private (self);

  priv->link.data = self;

  priv->contents = g_object_new (GTK_TYPE_BOX,
                                 "orientation", GTK_ORIENTATION_VERTICAL,
                                 "spacing", 0,
                                 "margin-start", 6,
                                 "margin-end", 6,
                                 "margin-top", 6,
                                 "margin-bottom", 6,
                                 NULL);

  gtk_widget_set_parent (GTK_WIDGET (priv->contents), GTK_WIDGET (self));
  gtk_widget_add_css_class (GTK_WIDGET (self), "compact");
  gtk_widget_add_css_class (GTK_WIDGET (self), "navigation-sidebar");
}


/*
 * -----< PROPERTIES >----- *
 */
/**
 * bimforge_page_get_context:
 * @self: a #BimforgePage.
 *
 * Gets the #ForgerContext that @self is linked to.
 *
 * Returns: (transfer full) (nullable): a #ForgerContext
 */
ForgerContext *
bimforge_page_get_context (BimforgePage *self)
{
  BimforgePagePrivate *priv = bimforge_page_get_instance_private (self);

  g_return_val_if_fail (BIMFORGE_IS_PAGE (self), NULL);

  return priv->context;
}

/**
 * bimforge_page_get_title:
 * @self: a #BimforgePage.
 *
 * Gets the "title" property of @self.
 *
 * Returns: (transfer full): a #gchar.
 *
 * Since: 0.1
 */
gchar *
bimforge_page_get_title (BimforgePage *self)
{
  BimforgePagePrivate *priv = bimforge_page_get_instance_private (self);

  g_return_val_if_fail (BIMFORGE_IS_PAGE (self), NULL);

  return priv->title;
}

/**
 * bimforge_page_set_title:
 * @self: a #BimforgePage.
 * @title: a #gchar.
 *
 * Sets the "title" property of @self.
 *
 * Since: 0.1
 */
void
bimforge_page_set_title (BimforgePage  *self,
                       const gchar *title)
{
  BimforgePagePrivate *priv = bimforge_page_get_instance_private (self);

  g_return_if_fail (BIMFORGE_IS_PAGE (self));
  g_return_if_fail (title);

  if (g_strcmp0 (priv->title, title) != 0)
    {
      g_free (priv->title);
      priv->title = g_strdup (title);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_TITLE]);
    }
}

/**
 * bimforge_page_get_icon_name:
 * @self: a #BimforgePage.
 *
 * Gets the "icon-name" property of @self.
 *
 * Returns: (transfer full): a #gchar.
 *
 * Since: 0.1
 */
gchar *
bimforge_page_get_icon_name (BimforgePage *self)
{
  BimforgePagePrivate *priv = bimforge_page_get_instance_private (self);

  g_return_val_if_fail (BIMFORGE_IS_PAGE (self), NULL);

  return priv->icon_name;
}

/**
 * bimforge_page_set_icon_name:
 * @self: a #BimforgePage.
 * @icon_name: a #gchar.
 *
 * Sets the "icon-name" property of @self.
 *
 * Since: 0.1
 */
void
bimforge_page_set_icon_name (BimforgePage  *self,
                           const gchar *icon_name)
{
  BimforgePagePrivate *priv = bimforge_page_get_instance_private (self);

  g_return_if_fail (BIMFORGE_IS_PAGE (self));
  g_return_if_fail (icon_name);

  if (g_strcmp0 (priv->icon_name, icon_name) != 0)
    {
      g_free (priv->icon_name);
      priv->icon_name = g_strdup (icon_name);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ICON_NAME]);
    }
}

GtkOrientation
bimforge_page_get_orientation (BimforgePage *self)
{
  BimforgePagePrivate *priv = bimforge_page_get_instance_private (self);
  g_return_val_if_fail (BIMFORGE_IS_PAGE (self), 0);
  return gtk_orientable_get_orientation (GTK_ORIENTABLE (priv->contents));
}

void
bimforge_page_set_orientation (BimforgePage     *self,
                             GtkOrientation  setting)
{
  BimforgePagePrivate *priv = bimforge_page_get_instance_private (self);
  g_return_if_fail (BIMFORGE_IS_PAGE (self));
  gtk_orientable_set_orientation (GTK_ORIENTABLE (priv->contents), setting);
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ORIENTATION]);
}

/**
 * bimforge_page_append_child:
 * @self: a #BimforgePage.
 * @child: a #GtkWidget.
 *
 * Adds @child to the end of @self.
 * This function allows us to add a #GtkWidget to @self.
 *
 * Since: 0.1
 */
void
bimforge_page_append_child (BimforgePage *self,
                          GtkWidget  *child)
{
  BimforgePagePrivate *priv = bimforge_page_get_instance_private (self);

  g_return_if_fail (BIMFORGE_IS_PAGE (self));
  g_return_if_fail (GTK_IS_WIDGET (child));

  gtk_box_append (priv->contents, child);
}


/*
 * -----< PRIVATE API >----- *
 */
GList *
_bimforge_page_get_link (BimforgePage *self)
{
  BimforgePagePrivate *priv = bimforge_page_get_instance_private (self);
  g_return_val_if_fail (BIMFORGE_IS_PAGE (self), NULL);
  return &priv->link;
}

void
_bimforge_page_set_context (BimforgePage  *self,
                            ForgerContext *context)
{
  BimforgePagePrivate *priv = bimforge_page_get_instance_private (self);

  g_return_if_fail (BIMFORGE_IS_PAGE (self));
  g_return_if_fail (FORGER_IS_CONTEXT (context));

  if (priv->context == context)
    return;

  if (g_set_weak_pointer (&priv->context, context))
    g_signal_emit (self, signals [SIGNAL_CONTEXT_SET], 0);
}

