/*
 * bimforge-application-addin.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bimforge-application-addin"

#include "config.h"
#include "bimforge-debug.h"

#include "bimforge-application-addin.h"

G_DEFINE_INTERFACE (BimforgeApplicationAddin, bimforge_application_addin, G_TYPE_OBJECT)

static void
bimforge_application_addin_default_init (BimforgeApplicationAddinInterface *iface)
{
}


void
bimforge_application_addin_load (BimforgeApplicationAddin *self,
                               BimforgeApplication      *app)
{
  if (BIMFORGE_APPLICATION_ADDIN_GET_IFACE (self)->load)
    BIMFORGE_APPLICATION_ADDIN_GET_IFACE (self)->load (self, app);
}

void
bimforge_application_addin_unload (BimforgeApplicationAddin *self,
                                 BimforgeApplication      *app)
{
  if (BIMFORGE_APPLICATION_ADDIN_GET_IFACE (self)->unload)
    BIMFORGE_APPLICATION_ADDIN_GET_IFACE (self)->unload (self, app);
}

void
bimforge_application_addin_activate (BimforgeApplicationAddin *self,
                                   BimforgeApplication      *app)
{
  if (BIMFORGE_APPLICATION_ADDIN_GET_IFACE (self)->activate)
    BIMFORGE_APPLICATION_ADDIN_GET_IFACE (self)->activate (self, app);
}
