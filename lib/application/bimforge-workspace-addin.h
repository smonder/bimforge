/*
 * bimforge-workspace-addin.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(BIMFORGE_INSIDE) && !defined(BIMFORGE_COMPILATION)
#error "Only <libbimforge.h> can be included directly."
#endif

#include "bimforge-workspace.h"

#include "bimforge-version-macros.h"

G_BEGIN_DECLS

#define BIMFORGE_TYPE_WORKSPACE_ADDIN (bimforge_workspace_addin_get_type ())

BIMFORGE_AVAILABLE_IN_ALL
G_DECLARE_INTERFACE (BimforgeWorkspaceAddin, bimforge_workspace_addin, BIMFORGE, WORKSPACE_ADDIN, GObject)

struct _BimforgeWorkspaceAddinInterface
{
    GTypeInterface parent;

    void    (* load)     (BimforgeWorkspaceAddin * addin,
                          BimforgeWorkspace *      workspace);

    void    (* unload)   (BimforgeWorkspaceAddin * addin,
                          BimforgeWorkspace *      workspace);
};


BIMFORGE_AVAILABLE_IN_ALL
void       bimforge_workspace_addin_load             (BimforgeWorkspaceAddin * self,
                                                    BimforgeWorkspace *      workspace);
BIMFORGE_AVAILABLE_IN_ALL
void       bimforge_workspace_addin_unload           (BimforgeWorkspaceAddin * self,
                                                    BimforgeWorkspace *      workspace);

G_END_DECLS
