/*
 * bimforge-workspace-addin.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bimforge-workspace-addin"

#include "config.h"
#include "bimforge-debug.h"

#include "bimforge-workspace-addin.h"

G_DEFINE_INTERFACE (BimforgeWorkspaceAddin, bimforge_workspace_addin, G_TYPE_OBJECT)

static void
bimforge_workspace_addin_default_init (BimforgeWorkspaceAddinInterface *iface)
{
}


void
bimforge_workspace_addin_load (BimforgeWorkspaceAddin *self,
                             BimforgeWorkspace      *workspace)
{
  g_return_if_fail (BIMFORGE_IS_WORKSPACE_ADDIN (self));
  g_return_if_fail (BIMFORGE_IS_WORKSPACE (workspace));

  if (BIMFORGE_WORKSPACE_ADDIN_GET_IFACE (self)->load)
    BIMFORGE_WORKSPACE_ADDIN_GET_IFACE (self)->load (self, workspace);
}

void
bimforge_workspace_addin_unload (BimforgeWorkspaceAddin *self,
                               BimforgeWorkspace      *workspace)
{
  g_return_if_fail (BIMFORGE_IS_WORKSPACE_ADDIN (self));
  g_return_if_fail (BIMFORGE_IS_WORKSPACE (workspace));

  if (BIMFORGE_WORKSPACE_ADDIN_GET_IFACE (self)->unload)
    BIMFORGE_WORKSPACE_ADDIN_GET_IFACE (self)->unload (self, workspace);
}
 
