/*
 * bimforge-workspace.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#if !defined(BIMFORGE_INSIDE) && !defined(BIMFORGE_COMPILATION)
#error "Only <libbimforge.h> can be included directly."
#endif

#include <libpanel.h>

#include "bimforge-page.h"
#include "bimforge-viewport.h"
#include "bimforge-version-macros.h"

G_BEGIN_DECLS

#define BIMFORGE_TYPE_WORKSPACE (bimforge_workspace_get_type())

BIMFORGE_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (BimforgeWorkspace, bimforge_workspace, BIMFORGE, WORKSPACE, PanelDocumentWorkspace)

struct _BimforgeWorkspaceClass
{
  PanelDocumentWorkspaceClass parent_class;
};


BIMFORGE_AVAILABLE_IN_ALL
BimforgeWorkspace * bimforge_workspace_new              (ForgerContext *    context) G_GNUC_WARN_UNUSED_RESULT;

BIMFORGE_AVAILABLE_IN_ALL
ForgerContext *     bimforge_workspace_get_context      (BimforgeWorkspace *  self);

BIMFORGE_AVAILABLE_IN_ALL
GMenuModel *      bimforge_workspace_get_main_menu    (BimforgeWorkspace *  self);
BIMFORGE_AVAILABLE_IN_ALL
void              bimforge_workspace_set_main_menu    (BimforgeWorkspace *  self,
                                                     GMenuModel *       menu);

BIMFORGE_AVAILABLE_IN_ALL
void              bimforge_workspace_add_page         (BimforgeWorkspace *  self,
                                                     BimforgePage *       page,
                                                     PanelArea          area);
BIMFORGE_AVAILABLE_IN_ALL
void              bimforge_workspace_foreach_page     (BimforgeWorkspace *  self,
                                                     GFunc              func_,
                                                     gpointer           user_data);

BIMFORGE_AVAILABLE_IN_ALL
void              bimforge_workspace_add_viewport     (BimforgeWorkspace *  self,
                                                     BimforgeViewport *   viewport);
BIMFORGE_AVAILABLE_IN_ALL
void              bimforge_workspace_foreach_viewport (BimforgeWorkspace *  self,
                                                     GFunc              func_,
                                                     gpointer           user_data);
BIMFORGE_AVAILABLE_IN_ALL
void              bimforge_workspace_focus_viewport   (BimforgeWorkspace *  self,
                                                     BimforgeViewport *   viewport);
BIMFORGE_AVAILABLE_IN_ALL
BimforgeViewport *  bimforge_workspace_get_active_viewport (BimforgeWorkspace * self);


G_END_DECLS
