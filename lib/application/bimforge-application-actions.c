/*
 * bimforge-application-actions.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bimforge-application-actions"

#include "config.h"
#include "bimforge-debug.h"

#include "bimforge-application-private.h"


static void   bimforge_application_about_action (GSimpleAction * action,
                                               GVariant *      parameter,
                                               gpointer        user_data);

static void   bimforge_application_quit_action  (GSimpleAction * action,
                                               GVariant *      parameter,
                                               gpointer        user_data);

static const GActionEntry app_actions[] = {
	{ "quit", bimforge_application_quit_action },
	{ "about", bimforge_application_about_action },
};

void
_bimforge_application_init_actions (BimforgeApplication *self)
{
	g_action_map_add_action_entries (G_ACTION_MAP (self),
	                                 app_actions,
	                                 G_N_ELEMENTS (app_actions),
	                                 self);
	gtk_application_set_accels_for_action (GTK_APPLICATION (self),
	                                       "app.quit",
	                                       (const char *[]) { "<primary>q", NULL });
}


static void
bimforge_application_about_action (GSimpleAction *action,
                                 GVariant      *parameter,
                                 gpointer       user_data)
{
	static const char *developers[] = {"Salim Monder", NULL};
	BimforgeApplication *self = user_data;
	GtkWindow *window = NULL;

	g_assert (BIMFORGE_IS_APPLICATION (self));

	window = gtk_application_get_active_window (GTK_APPLICATION (self));

	adw_show_about_window (window,
	                       "application-name", PACKAGE_NAME,
	                       "application-icon", "io.sam.bimforge",
	                       "developer-name", "Salim Monder",
	                       "version", "0.1.0",
	                       "developers", developers,
	                       "copyright", "© 2023 Salim Monder",
	                       NULL);
}

static void
bimforge_application_quit_action (GSimpleAction *action,
                                GVariant      *parameter,
                                gpointer       user_data)
{
	BimforgeApplication *self = user_data;

	g_assert (BIMFORGE_IS_APPLICATION (self));

	g_application_quit (G_APPLICATION (self));
}
