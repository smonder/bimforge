/*
 * bimforge-application-plugins.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bimforge-application-plugins"

#include "config.h"
#include "bimforge-debug.h"

#include "bimforge-application-addin.h"
#include "bimforge-application-private.h"

static gboolean
_is_flatpak (void)
{
    return g_file_test ("/.flatpak-info", G_FILE_TEST_EXISTS);
}

static void
changed_plugin_cb (GSettings      *settings,
                   const gchar    *key,
                   PeasPluginInfo *info)
{
  PeasEngine *engine;

  g_assert (G_IS_SETTINGS (settings));
  g_assert (key != NULL);
  g_assert (info != NULL);

  engine = peas_engine_get_default ();

  if (!g_settings_get_boolean (settings, key))
    peas_engine_unload_plugin (engine, info);
  else
    peas_engine_load_plugin (engine, info);
}

static GSettings *
plugin_get_settings (BimforgeApplication *self,
                     PeasPluginInfo    *plugin_info)
{
  GSettings *settings;
  const gchar *module_name;

  g_assert (BIMFORGE_IS_MAIN_THREAD());
  g_assert (BIMFORGE_IS_APPLICATION (self));
  g_assert (plugin_info != NULL);

  module_name = peas_plugin_info_get_module_name (plugin_info);

  if G_UNLIKELY (self->plugin_settings == NULL)
    self->plugin_settings =
      g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_object_unref);

  if (!(settings = g_hash_table_lookup (self->plugin_settings, module_name)))
    {
      g_autofree gchar *path = NULL;

      path = g_strdup_printf ("/io/sam/bimforge/plugins/%s/", module_name);
      settings = g_settings_new_with_path ("io.sam.bimforge.plugin", path);
      g_hash_table_insert (self->plugin_settings, g_strdup (module_name), settings);

      g_signal_connect (settings,
                        "changed::enabled",
                        G_CALLBACK (changed_plugin_cb),
                        plugin_info);
    }

  return settings;
}

static gboolean
can_load_plugin (BimforgeApplication *self,
                 PeasPluginInfo    *plugin_info,
                 GHashTable        *circular)
{
  PeasEngine *engine = peas_engine_get_default ();
  const char *module_name;
  const char * const *deps;
  GSettings *settings;

  g_assert (BIMFORGE_IS_MAIN_THREAD());
  g_assert (BIMFORGE_IS_APPLICATION (self));
  g_assert (circular != NULL);

  if (plugin_info == NULL)
    return FALSE;

  module_name = peas_plugin_info_get_module_name (plugin_info);

  if (g_hash_table_contains (circular, module_name))
    {
      g_warning ("Circular dependency found in module %s", module_name);
      return FALSE;
    }

  g_hash_table_add (circular, (gpointer)module_name);

  /* Make sure the plugin has not been disabled in settings. */
  settings = plugin_get_settings (self, plugin_info);
  if (!g_settings_get_boolean (settings, "enabled"))
    return FALSE;

  /*
   * If this plugin has dependencies, we need to check that the dependencies
   * can also be loaded.
   */
  if ((deps = peas_plugin_info_get_dependencies (plugin_info)))
    {
      for (guint i = 0; deps[i]; i++)
        {
          PeasPluginInfo *dep = peas_engine_get_plugin_info (engine, deps[i]);

          if (!can_load_plugin (self, dep, circular))
            return FALSE;
        }
    }

  g_hash_table_remove (circular, (gpointer)module_name);

  return TRUE;
}

static void
load_plugin_resources (BimforgeApplication *self,
                       PeasEngine        *engine,
                       PeasPluginInfo    *plugin_info)
{
  g_autofree gchar *gresources_path = NULL;
  g_autofree gchar *gresources_basename = NULL;
  const gchar *module_dir;
  const gchar *module_name;

  g_assert (BIMFORGE_IS_MAIN_THREAD());
  g_assert (BIMFORGE_IS_APPLICATION (self));
  g_assert (plugin_info != NULL);
  g_assert (PEAS_IS_ENGINE (engine));

  module_dir = peas_plugin_info_get_module_dir (plugin_info);
  module_name = peas_plugin_info_get_module_name (plugin_info);
  gresources_basename = g_strdup_printf ("%s.gresource", module_name);
  gresources_path = g_build_filename (module_dir, gresources_basename, NULL);

  if (g_file_test (gresources_path, G_FILE_TEST_IS_REGULAR))
    {
      g_autofree gchar *resource_path = NULL;
      g_autoptr(GError) error = NULL;
      GResource *resource;

      resource = g_resource_load (gresources_path, &error);

      if (resource == NULL)
        {
          g_warning ("Failed to load gresources: %s", error->message);
          return;
        }

      g_hash_table_insert (self->plugin_gresources, g_strdup (module_name), resource);
      g_resources_register (resource);

      resource_path = g_strdup_printf ("resource:///plugins/%s", module_name);
      _bimforge_application_add_resources (self, resource_path);
    }
}

static void
load_plugin (BimforgeApplication *self,
             PeasPluginInfo    *info)
{
  PeasEngine *engine = peas_engine_get_default ();
  g_autoptr(GHashTable) circular = NULL;

  g_assert (BIMFORGE_IS_MAIN_THREAD());
  g_assert (BIMFORGE_IS_APPLICATION (self));
  g_assert (info != NULL);

  circular = g_hash_table_new (g_str_hash, g_str_equal);

  if (can_load_plugin (self, info, circular))
    peas_engine_load_plugin (engine, info);
}

static void
load_plugin_after_cb (BimforgeApplication *self,
                      PeasPluginInfo    *plugin_info,
                      PeasEngine        *engine)
{
  const gchar *data_dir;
  const gchar *module_dir;
  const gchar *module_name;

  g_assert (BIMFORGE_IS_MAIN_THREAD());
  g_assert (BIMFORGE_IS_APPLICATION (self));
  g_assert (plugin_info != NULL);
  g_assert (PEAS_IS_ENGINE (engine));

  data_dir = peas_plugin_info_get_data_dir (plugin_info);
  module_dir = peas_plugin_info_get_module_dir (plugin_info);
  module_name = peas_plugin_info_get_module_name (plugin_info);

  g_debug ("Loaded plugin \"%s\" with module-dir \"%s\"",
           module_name, module_dir);

  if (peas_plugin_info_get_external_data (plugin_info, "Has-Resources"))
    load_plugin_resources (self, engine, plugin_info);

  if (g_str_has_prefix (data_dir, "resource://") ||
      !peas_plugin_info_is_builtin (plugin_info))
    _bimforge_application_add_resources (self, data_dir);
}

static void
unload_plugin_after_cb (BimforgeApplication *self,
                        PeasPluginInfo    *plugin_info,
                        PeasEngine        *engine)
{
  const gchar *module_dir;
  const gchar *module_name;

  g_assert (BIMFORGE_IS_MAIN_THREAD());
  g_assert (BIMFORGE_IS_APPLICATION (self));
  g_assert (plugin_info != NULL);
  g_assert (PEAS_IS_ENGINE (engine));

  module_dir = peas_plugin_info_get_module_dir (plugin_info);
  module_name = peas_plugin_info_get_module_name (plugin_info);

  g_debug ("Unloaded plugin \"%s\" with module-dir \"%s\"",
           module_name, module_dir);
}

void
_bimforge_application_load_plugins_for_startup (BimforgeApplication *self)
{
  PeasEngine *engine;
  guint n_items;

  BIMFORGE_ENTRY;

  g_assert (BIMFORGE_IS_MAIN_THREAD());
  g_assert (BIMFORGE_IS_APPLICATION (self));

  engine = peas_engine_get_default ();

  g_signal_connect_object (engine,
                           "load-plugin",
                           G_CALLBACK (load_plugin_after_cb),
                           self,
                           G_CONNECT_SWAPPED | G_CONNECT_AFTER);

  g_signal_connect_object (engine,
                           "unload-plugin",
                           G_CALLBACK (unload_plugin_after_cb),
                           self,
                           G_CONNECT_SWAPPED | G_CONNECT_AFTER);

  peas_engine_add_search_path (engine,
                               "resource:///plugins",
                               "resource:///plugins");

  if (_is_flatpak ())
    peas_engine_add_search_path (engine,
                                 "/app/extensions/lib/bimforge/plugins",
                                 "/app/extensions/lib/bimforge/plugins");

  peas_engine_rescan_plugins (engine);

  n_items = g_list_model_get_n_items (G_LIST_MODEL (engine));
  for (guint i = 0; i < n_items; i++)
    {
      g_autoptr(PeasPluginInfo) plugin_info = g_list_model_get_item (G_LIST_MODEL (engine), i);

      if (!peas_plugin_info_is_loaded (plugin_info) &&
          peas_plugin_info_get_external_data (plugin_info, "At-Startup"))
        load_plugin (self, plugin_info);
    }

  BIMFORGE_EXIT;
}




void
_bimforge_application_load_plugins (BimforgeApplication *self)
{
  g_autofree gchar *user_plugins_dir = NULL;
  PeasEngine *engine;
  guint n_items;

  g_assert (BIMFORGE_IS_APPLICATION (self));

  engine = peas_engine_get_default ();

  peas_engine_add_search_path (engine,
                               PACKAGE_LIBDIR"/bimforge/plugins",
                               PACKAGE_DATADIR"/bimforge/plugins");

  if (_is_flatpak ())
    {
      g_autofree gchar *extensions_plugins_dir = NULL;
      g_autofree gchar *plugins_dir = NULL;

      plugins_dir = g_build_filename (g_get_home_dir (),
                                      ".local",
                                      "share",
                                      "bimforge",
                                      "plugins",
                                      NULL);
      peas_engine_add_search_path (engine, plugins_dir, plugins_dir);

      extensions_plugins_dir = g_build_filename ("/app",
                                                 "extensions",
                                                 "lib",
                                                 "bimforge",
                                                 "plugins",
                                                 NULL);
      peas_engine_add_search_path (engine, extensions_plugins_dir, extensions_plugins_dir);
    }

  user_plugins_dir = g_build_filename (g_get_user_data_dir (),
                                       "bimforge",
                                       "plugins",
                                       NULL);
  peas_engine_add_search_path (engine, user_plugins_dir, NULL);

  if (self->loaded_typelibs)
    peas_engine_enable_loader (engine, "python");

  peas_engine_rescan_plugins (engine);

  n_items = g_list_model_get_n_items (G_LIST_MODEL (engine));

  for (guint i = 0; i < n_items; i++)
    {
      g_autoptr(PeasPluginInfo) plugin_info = g_list_model_get_item (G_LIST_MODEL (engine), i);

      if (!peas_plugin_info_is_loaded (plugin_info))
        load_plugin (self, plugin_info);
    }
}



static void
on_addins_extension_added_cb (PeasExtensionSet *set,
                              PeasPluginInfo   *info,
                              GObject          *extension,
                              gpointer          user_data)
{
  BimforgeApplication *self = user_data;
  BimforgeApplicationAddin *addin = (BimforgeApplicationAddin *)extension;

  g_assert (BIMFORGE_IS_MAIN_THREAD());
  g_assert (BIMFORGE_IS_APPLICATION (self));
  g_assert (BIMFORGE_IS_APPLICATION_ADDIN (addin));
  g_assert (PEAS_IS_EXTENSION_SET (set));
  g_assert (info != NULL);

  bimforge_application_addin_load (addin, self);
}

static void
on_addins_extension_removed_cb (PeasExtensionSet *set,
                                PeasPluginInfo   *info,
                                GObject          *extension,
                                gpointer          user_data)
{
  BimforgeApplication *self = user_data;
  BimforgeApplicationAddin *addin = (BimforgeApplicationAddin*)extension;

  g_assert (BIMFORGE_IS_MAIN_THREAD());
  g_assert (BIMFORGE_IS_APPLICATION (self));
  g_assert (BIMFORGE_IS_APPLICATION_ADDIN (addin));
  g_assert (PEAS_IS_EXTENSION_SET (set));
  g_assert (info != NULL);

  bimforge_application_addin_unload (addin, self);
}

void
_bimforge_application_load_addins (BimforgeApplication *self)
{
  g_assert (BIMFORGE_IS_MAIN_THREAD());
  g_assert (BIMFORGE_IS_APPLICATION (self));
  g_assert (self->addins == NULL);

  self->addins = peas_extension_set_new (peas_engine_get_default (),
                                         BIMFORGE_TYPE_APPLICATION_ADDIN,
                                         NULL);

  g_signal_connect (self->addins,
                    "extension-added",
                    G_CALLBACK (on_addins_extension_added_cb),
                    self);

  g_signal_connect (self->addins,
                    "extension-removed",
                    G_CALLBACK (on_addins_extension_removed_cb),
                    self);

  peas_extension_set_foreach (self->addins,
                              on_addins_extension_added_cb,
                              self);
}

void
_bimforge_application_unload_addins (BimforgeApplication *self)
{
  g_assert (BIMFORGE_IS_MAIN_THREAD());
  g_assert (BIMFORGE_IS_APPLICATION (self));

  g_clear_object (&self->addins);
}
