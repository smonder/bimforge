/*
 * bimforge-workspace.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bimforge-workspace"

#include "config.h"
#include "bimforge-debug.h"

#include <libpeas.h>

#include "bimforge-workspace.h"
#include "bimforge-workspace-addin.h"
#include "bimforge-gui-private.h"
#include "bimforge-application.h"


/**
 * SECTION: BimforgeWorkspace
 * @short_description: The base type of windows that all Bimforge windows will inherit from.
 * @title: BimforgeWorkspace.
 *
 * #BimforgeWorkspace is a #GtkWindow subclass that is configured to fit with other Bimforge
 * classes like Panels, Pages, etc.
 *
 * Since: 0.1
 */


typedef struct
{
  /* Template headerbar widgets */
  AdwHeaderBar * header_bar;
  GtkMenuButton * menu_button;

  /* DOCK */
  PanelDock * dock;

  /* The context with which this window is linked */
  ForgerContext * context;

  /* Our addins for the workspace window, that are limited by the "kind" of
   * workspace that is loaded.
   */
  PeasExtensionSet * addins;

  /* Viewports */
  /* GtkMenuButton * viewport_menu_button; */
  GQueue viewports;
  BimforgeViewport ** focused_viewport;

  /* Pages */
  GQueue pages;

  /* Queued source to save window size/etc */
  guint queued_window_save;
} BimforgeWorkspacePrivate;

G_DEFINE_TYPE_WITH_PRIVATE (BimforgeWorkspace, bimforge_workspace, PANEL_TYPE_DOCUMENT_WORKSPACE)

enum {
  PROP_0,
  PROP_CONTEXT,
  PROP_MAIN_MENU,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];
static GSettings * settings;


/*
 * -----< CONSTRUCT ONLY SETTERS >----- *
 */
static void
bimforge_workspace_set_context (BimforgeWorkspace *self,
                                ForgerContext     *context)
{
  BimforgeWorkspacePrivate *priv = bimforge_workspace_get_instance_private (self);

  BIMFORGE_ENTRY;
  g_return_if_fail (BIMFORGE_IS_WORKSPACE (self));
  g_return_if_fail (FORGER_IS_CONTEXT (context));

  if (g_set_object (&priv->context, context))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CONTEXT]);

  BIMFORGE_EXIT;
}


/*
 * -----< SIGNAL CALLBACKS >----- *
 */


/*
 * -----< GTK_WIDGET CLASS IMPLEMENTATION >-----*
 */
static gboolean
bimforge_workspace_save_settings (gpointer data)
{
  BimforgeWorkspace *self = data;
  BimforgeWorkspacePrivate *priv = bimforge_workspace_get_instance_private (self);
  GdkRectangle geom = {0};
  gboolean maximized;

  g_assert (BIMFORGE_IS_WORKSPACE (self));

  priv->queued_window_save = 0;

  if (!gtk_widget_get_realized (GTK_WIDGET (self)) ||
      !gtk_widget_get_visible (GTK_WIDGET (self)))
    return G_SOURCE_REMOVE;

  if (settings == NULL)
    settings = g_settings_new ("io.sam.bimforge");

  gtk_window_get_default_size (GTK_WINDOW (self), &geom.width, &geom.height);
  maximized = gtk_window_is_maximized (GTK_WINDOW (self));

  g_settings_set (settings, "window-size", "(ii)", geom.width, geom.height);
  g_settings_set_boolean (settings, "window-maximized", maximized);

  return G_SOURCE_REMOVE;
}

static void
bimforge_workspace_size_allocate (GtkWidget *widget,
                                gint       width,
                                gint       height,
                                gint       baseline)
{
  BimforgeWorkspace *self = (BimforgeWorkspace *)widget;
  BimforgeWorkspacePrivate *priv = bimforge_workspace_get_instance_private (self);

  g_assert (BIMFORGE_IS_WORKSPACE (self));

  GTK_WIDGET_CLASS (bimforge_workspace_parent_class)->size_allocate (widget, width, height, baseline);

  if (priv->queued_window_save == 0)
    priv->queued_window_save = g_timeout_add_seconds (1, bimforge_workspace_save_settings, self);
}

static void
bimforge_workspace_restore_size (BimforgeWorkspace *self,
                               gint             width,
                               gint             height)
{
  g_assert (BIMFORGE_IS_WORKSPACE (self));

  gtk_window_set_default_size (GTK_WINDOW (self), width, height);
}

static void
bimforge_workspace_realize (GtkWidget *widget)
{
  BimforgeWorkspace *self = (BimforgeWorkspace *)widget;
  GdkRectangle geom = {0};
  gboolean maximized = FALSE;

  g_assert (BIMFORGE_IS_WORKSPACE (self));

  if (settings == NULL)
    settings = g_settings_new ("io.sam.bimforge");

  g_settings_get (settings, "window-size", "(ii)", &geom.width, &geom.height);
  g_settings_get (settings, "window-maximized", "b", &maximized);

  bimforge_workspace_restore_size (self, geom.width, geom.height);

  GTK_WIDGET_CLASS (bimforge_workspace_parent_class)->realize (widget);

  if (maximized)
    gtk_window_maximize (GTK_WINDOW (self));
}


/*
 * -----< PLUGINS >----- *
 */
static void
on_addins_extension_added_cb (PeasExtensionSet *set,
                              PeasPluginInfo   *info,
                              GObject          *extension,
                              gpointer          user_data)
{
  BimforgeWorkspace *self = user_data;
  BimforgeWorkspaceAddin *addin = (BimforgeWorkspaceAddin *)extension;

  g_assert (BIMFORGE_IS_WORKSPACE (self));
  g_assert (BIMFORGE_IS_WORKSPACE_ADDIN (addin));
  g_assert (PEAS_IS_EXTENSION_SET (set));
  g_assert (info != NULL);

  bimforge_workspace_addin_load (addin, self);
}

static void
on_addins_extension_removed_cb (PeasExtensionSet *set,
                                PeasPluginInfo   *info,
                                GObject          *extension,
                                gpointer          user_data)
{
  BimforgeWorkspace *self = user_data;
  BimforgeWorkspaceAddin *addin = (BimforgeWorkspaceAddin *)extension;

  g_assert (BIMFORGE_IS_WORKSPACE (self));
  g_assert (BIMFORGE_IS_WORKSPACE_ADDIN (addin));
  g_assert (PEAS_IS_EXTENSION_SET (set));
  g_assert (info != NULL);

  bimforge_workspace_addin_unload (addin, self);
}


/*
 * -----< G_OBJECT CLASS IMPLEMENTATION >----- *
 */
static void
bimforge_workspace_constructed (GObject *object)
{
  BimforgeWorkspace *self = (BimforgeWorkspace *)object;
  BimforgeWorkspacePrivate *priv = bimforge_workspace_get_instance_private (self);

  G_OBJECT_CLASS (bimforge_workspace_parent_class)->constructed (object);

  priv->addins = peas_extension_set_new (peas_engine_get_default (),
                                         BIMFORGE_TYPE_WORKSPACE_ADDIN,
                                         NULL);

  g_signal_connect (priv->addins,
                    "extension-added",
                    G_CALLBACK (on_addins_extension_added_cb),
                    self);

  g_signal_connect (priv->addins,
                    "extension-removed",
                    G_CALLBACK (on_addins_extension_removed_cb),
                    self);

  peas_extension_set_foreach (priv->addins, on_addins_extension_added_cb, self);
}

static void
bimforge_workspace_dispose (GObject *object)
{
  BimforgeWorkspace *self = (BimforgeWorkspace *)object;

  gtk_widget_dispose_template (GTK_WIDGET (self), BIMFORGE_TYPE_WORKSPACE);

  G_OBJECT_CLASS (bimforge_workspace_parent_class)->dispose (object);
}

static void
bimforge_workspace_finalize (GObject *object)
{
  BimforgeWorkspace *self = (BimforgeWorkspace *)object;
  BimforgeWorkspacePrivate *priv = bimforge_workspace_get_instance_private (self);

  g_clear_object (&priv->addins);

  g_clear_pointer (&priv->context, g_object_unref);

  G_OBJECT_CLASS (bimforge_workspace_parent_class)->finalize (object);
}

static void
bimforge_workspace_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  BimforgeWorkspace *self = BIMFORGE_WORKSPACE (object);

  switch (prop_id)
    {
    case PROP_CONTEXT:
      g_value_set_object (value, bimforge_workspace_get_context (self));
      break;

    case PROP_MAIN_MENU:
      g_value_set_object (value, bimforge_workspace_get_main_menu (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bimforge_workspace_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  BimforgeWorkspace *self = BIMFORGE_WORKSPACE (object);

  switch (prop_id)
    {
    case PROP_CONTEXT:
      bimforge_workspace_set_context (self, g_value_get_object (value));
      break;

    case PROP_MAIN_MENU:
      bimforge_workspace_set_main_menu (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bimforge_workspace_class_init (BimforgeWorkspaceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = bimforge_workspace_dispose;
  object_class->finalize = bimforge_workspace_finalize;
  object_class->constructed = bimforge_workspace_constructed;
  object_class->get_property = bimforge_workspace_get_property;
  object_class->set_property = bimforge_workspace_set_property;

  widget_class->realize = bimforge_workspace_realize;
  widget_class->size_allocate = bimforge_workspace_size_allocate;

  /**
   * BimforgeWorkspace:context:
   *
   * The "context" property is the #ForgerContext object that is linked to
   * workspace.
   *
   * Since: 0.1
   */
  properties [PROP_CONTEXT] =
    g_param_spec_object ("context", NULL, NULL,
                         FORGER_TYPE_CONTEXT,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  /**
   * BimforgeWorkspace:main-menu:
   * The "main-menu" property is the menu that is going to be shown when
   * the user activate the menu button on the header bar.
   *
   * Since: 0.1
   */
  properties [PROP_MAIN_MENU] =
    g_param_spec_object ("main-menu", NULL, NULL,
                         G_TYPE_MENU_MODEL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

	gtk_widget_class_set_template_from_resource (widget_class, "/io/sam/libbimforge/app/bimforge-workspace.ui");
	gtk_widget_class_bind_template_child_private (widget_class, BimforgeWorkspace, header_bar);
	gtk_widget_class_bind_template_child_private (widget_class, BimforgeWorkspace, menu_button);
	gtk_widget_class_bind_template_child_private (widget_class, BimforgeWorkspace, dock);

  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_F9, 0, "win.reveal-start", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_F9, GDK_CONTROL_MASK, "win.reveal-bottom", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_F9, GDK_SHIFT_MASK, "win.reveal-end", NULL);
}

static void
bimforge_workspace_init (BimforgeWorkspace *self)
{
  BimforgeWorkspacePrivate *priv = bimforge_workspace_get_instance_private (self);
  GPropertyAction *reveal_start = NULL;
  GPropertyAction *reveal_end = NULL;
  GPropertyAction *reveal_bottom = NULL;

  gtk_widget_init_template (GTK_WIDGET (self));

  reveal_start = g_property_action_new ("reveal-start", priv->dock, "reveal-start");
  reveal_bottom = g_property_action_new ("reveal-bottom", priv->dock, "reveal-bottom");
  reveal_end = g_property_action_new ("reveal-end", priv->dock, "reveal-end");

  g_action_map_add_action (G_ACTION_MAP (self), G_ACTION (reveal_start));
  g_action_map_add_action (G_ACTION_MAP (self), G_ACTION (reveal_end));
  g_action_map_add_action (G_ACTION_MAP (self), G_ACTION (reveal_bottom));

  g_clear_object (&reveal_start);
  g_clear_object (&reveal_end);
  g_clear_object (&reveal_bottom);
}


/*
 * -----< CONSTRUCTORS >----- *
 */

/**
 * bimforge_workspace_new:
 * @context: a #ForgerContext.
 *
 * Create a new #BimforgeWorkspace for context @context.
 *
 * Returns: (transfer full) (nullable): a newly created #BimforgeWorkspace
 */
BimforgeWorkspace *
bimforge_workspace_new (ForgerContext * context)
{
  g_return_val_if_fail (FORGER_IS_CONTEXT (context), NULL);

  return g_object_new (BIMFORGE_TYPE_WORKSPACE,
                       "application", BIMFORGE_APPLICATION_DEFAULT,
                       "title", GETTEXT_PACKAGE,
                       "context", context, NULL);
}


/*
 * -----< PROPERTIES >----- *
 */
/**
 * bimforge_workspace_get_context:
 * @self: a #BimforgeWorkspace
 *
 * Gets the "context" property value.
 *
 * Returns: (transfer full): a #ForgerContext.
 */
ForgerContext *
bimforge_workspace_get_context (BimforgeWorkspace *self)
{
  BimforgeWorkspacePrivate *priv = bimforge_workspace_get_instance_private (self);
  g_return_val_if_fail (BIMFORGE_IS_WORKSPACE (self), NULL);
  return priv->context;
}

/**
 * bimforge_workspace_get_main_menu:
 * @self: a #BimforgeWorkspace.
 *
 * Get the "main-menu" property value.
 *
 * Returns: (transfer full): a #GMenuModel object.
 */
GMenuModel *
bimforge_workspace_get_main_menu (BimforgeWorkspace *self)
{
  BimforgeWorkspacePrivate *priv = bimforge_workspace_get_instance_private (self);
  g_return_val_if_fail (BIMFORGE_IS_WORKSPACE (self), NULL);
  return gtk_menu_button_get_menu_model (priv->menu_button);
}

/**
 * bimforge_workspace_set_main_menu:
 * @self: a #BimforgeWorkspace.
 * @menu: a #GMenuModel
 *
 * Set the "main-menu" property of @self.
 * The "main-menu" property is the menu that is going to be shown when
 * the user activate the menu button on the headerbar.
 *
 */
void
bimforge_workspace_set_main_menu (BimforgeWorkspace *self,
                                GMenuModel      *menu)
{
  BimforgeWorkspacePrivate *priv = bimforge_workspace_get_instance_private (self);

  g_return_if_fail (BIMFORGE_IS_WORKSPACE (self));
  g_return_if_fail (!menu || G_IS_MENU_MODEL (menu));

  gtk_menu_button_set_menu_model (priv->menu_button, menu);
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_MAIN_MENU]);
}


/*
 * -----< PAGES API >----- *
 */
/**
 * bimforge_workspace_add_hpanel:
 * @self: a #BimforgeWorkspace.
 * @page: a #BimforgePage
 * @area: a #PanelArea: where the page should be added.
 *
 * Attach @page to @self and set its "context" property.
 *
 */
void
bimforge_workspace_add_page (BimforgeWorkspace *self,
                           BimforgePage      *page,
                           PanelArea        area)
{
  BimforgeWorkspacePrivate *priv = bimforge_workspace_get_instance_private (self);
  PanelWidget *page_widget;
  PanelPosition *pos;

  BIMFORGE_ENTRY;
  g_return_if_fail (BIMFORGE_IS_WORKSPACE (self));
  g_return_if_fail (BIMFORGE_IS_PAGE (page));

  _bimforge_page_set_context (page, priv->context);

  if (bimforge_page_get_orientation (page) == GTK_ORIENTATION_HORIZONTAL)
    area = PANEL_AREA_BOTTOM;

  page_widget = PANEL_WIDGET (panel_widget_new ());
  panel_widget_set_kind (page_widget, PANEL_WIDGET_KIND_UTILITY);
  panel_widget_set_child (page_widget, GTK_WIDGET (page));
  panel_widget_set_can_maximize (page_widget, FALSE);
  pos = panel_position_new ();
  panel_position_set_area (pos, area);

  g_object_bind_property (page, "title", page_widget, "title", G_BINDING_SYNC_CREATE);
  g_object_bind_property (page, "icon-name", page_widget, "icon-name", G_BINDING_SYNC_CREATE);

  g_queue_push_head_link (&priv->pages, _bimforge_page_get_link (page));

  panel_document_workspace_add_widget (PANEL_DOCUMENT_WORKSPACE (self),
                                       page_widget, pos);

  BIMFORGE_EXIT;
}

/**
 * bimforge_workspace_foreach_page:
 * @self: a #BimforgeWorkspace.
 * @func_: a #GFunc
 * @user_data: a pointer to data to pass to @func_.
 *
 * Call @func_ on each #BimforgePage attached to @self.
 *
 */
void
bimforge_workspace_foreach_page (BimforgeWorkspace *self,
                               GFunc            func_,
                               gpointer         user_data)
{
  BimforgeWorkspacePrivate *priv = bimforge_workspace_get_instance_private (self);

  BIMFORGE_ENTRY;
  g_return_if_fail (BIMFORGE_IS_WORKSPACE (self));
  g_queue_foreach (&priv->pages, func_, user_data);
  BIMFORGE_EXIT;
}


/*
 * -----< VIEWPORT API >----- *
 */
/**
 * bimforge_workspace_add_viewport:
 * @self: a #BimforgeWorkspace.
 * @viewport: a #BimforgeViewport
 *
 * Attach @viewport to @self and set the "context" property of viewport
 *
 */
void
bimforge_workspace_add_viewport (BimforgeWorkspace *self,
                               BimforgeViewport  *viewport)
{
  BimforgeWorkspacePrivate *priv = bimforge_workspace_get_instance_private (self);

  BIMFORGE_ENTRY;
  g_return_if_fail (BIMFORGE_IS_WORKSPACE (self));
  g_return_if_fail (BIMFORGE_IS_VIEWPORT (viewport));

  _bimforge_viewport_set_context (viewport, priv->context);
  g_queue_push_head_link (&priv->viewports,
                          _bimforge_viewport_get_link (viewport));

  panel_document_workspace_add_widget (PANEL_DOCUMENT_WORKSPACE (self),
                                       PANEL_WIDGET (viewport),
                                       NULL);
  panel_widget_raise (PANEL_WIDGET (viewport));
  panel_widget_focus_default (PANEL_WIDGET (viewport));
  bimforge_workspace_set_main_menu (self,
                                  bimforge_viewport_get_header_menu (viewport));

  BIMFORGE_EXIT;
}

/**
 * bimforge_workspace_foreach_viewport:
 * @self: a #BimforgeWorkspace.
 * @func_: a #GFunc callback.
 * @user_data: a pointer to data to pass to @func_.
 *
 * Call @func_ on each viewport of @self.
 *
 */
void
bimforge_workspace_foreach_viewport (BimforgeWorkspace *self,
                                   GFunc            func_,
                                   gpointer         user_data)
{
  BimforgeWorkspacePrivate *priv = bimforge_workspace_get_instance_private (self);

  BIMFORGE_ENTRY;
  g_return_if_fail (BIMFORGE_IS_WORKSPACE (self));

  g_queue_foreach (&priv->viewports, func_, user_data);

  BIMFORGE_EXIT;
}

/**
 * bimforge_workspace_focous_viewport:
 * @self: a #BimforgeWorkspace.
 * @viewport: a #BimforgeViewport
 *
 * Make @viewport the active viewport in @self.
 *
 */
void
bimforge_workspace_focus_viewport (BimforgeWorkspace *self,
                                 BimforgeViewport  *viewport)
{
  BimforgeWorkspacePrivate *priv = bimforge_workspace_get_instance_private (self);

  BIMFORGE_ENTRY;
  g_return_if_fail (BIMFORGE_IS_WORKSPACE (self));
  g_return_if_fail (BIMFORGE_IS_VIEWPORT (viewport));

  if (g_set_object (priv->focused_viewport, viewport))
    {
      panel_widget_raise (PANEL_WIDGET (viewport));
      panel_widget_focus_default (PANEL_WIDGET (viewport));
    }

  BIMFORGE_EXIT;
}

/**
 * bimforge_workspace_get_active_viewport:
 * @self: a #BimforgeWorkspace.
 *
 * Get the focused viewport
 *
 * Returns: (transfer full) (nullable): a #BimforgeViewport pr %NULL.
 */
BimforgeViewport *
bimforge_workspace_get_active_viewport (BimforgeWorkspace *self)
{
  BimforgeWorkspacePrivate *priv = bimforge_workspace_get_instance_private (self);
  g_return_val_if_fail (BIMFORGE_IS_WORKSPACE (self), NULL);
  return *priv->focused_viewport;
}

