/* bimforge-page.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(BIMFORGE_INSIDE) && !defined(BIMFORGE_COMPILATION)
#error "Only <libbimforge.h> can be included directly."
#endif

#include <adwaita.h>
#include <libforger.h>

#include "bimforge-version-macros.h"

G_BEGIN_DECLS

#define BIMFORGE_TYPE_PAGE (bimforge_page_get_type())

BIMFORGE_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (BimforgePage, bimforge_page, BIMFORGE, PAGE, GtkWidget)

struct _BimforgePageClass
{
  GtkWidgetClass parent_class;

  /* ---< PRIVATE >--- */
  gpointer padding [12];
};

BIMFORGE_AVAILABLE_IN_ALL
ForgerContext * bimforge_page_get_context       (BimforgePage *    self);

BIMFORGE_AVAILABLE_IN_ALL
gchar *         bimforge_page_get_title         (BimforgePage *    self);
BIMFORGE_AVAILABLE_IN_ALL
void            bimforge_page_set_title         (BimforgePage *    self,
                                               const gchar *   title);

BIMFORGE_AVAILABLE_IN_ALL
gchar *         bimforge_page_get_icon_name     (BimforgePage *    self);
BIMFORGE_AVAILABLE_IN_ALL
void            bimforge_page_set_icon_name     (BimforgePage *    self,
                                               const gchar *   icon_name);

BIMFORGE_AVAILABLE_IN_ALL
GtkOrientation  bimforge_page_get_orientation   (BimforgePage *    self);
BIMFORGE_AVAILABLE_IN_ALL
void            bimforge_page_set_orientation   (BimforgePage *    self,
                                               GtkOrientation  setting);

BIMFORGE_AVAILABLE_IN_ALL
void            bimforge_page_append_child      (BimforgePage *    self,
                                               GtkWidget *     child);

G_END_DECLS


