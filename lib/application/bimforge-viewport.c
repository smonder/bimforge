/*
 * bimforge-viewport.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bimforge-viewport"

#include "config.h"
#include "bimforge-debug.h"

#include "bimforge-viewport.h"
#include "bimforge-gui-private.h"


/**
 * SECTION: BimforgeViewport.
 * @short_description: The base type of editors in Bimforge.
 * @title: BimforgeViewport.
 *
 * This is the base type that all editors in Bimforge should inherit from.
 * #BimforgeViewport represents a working area responsible for holding a viewer
 * widget in the #BimforgeWorkspace that it is attached to.
 *
 * Since: 0.1
 */


typedef struct
{
  GList link;

  /* Properties */
  ForgerContext * context;

  /* Children Widgets */
  GtkOverlay * overlay;
} BimforgeViewportPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (BimforgeViewport, bimforge_viewport, PANEL_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_HEADER_MENU,
  PROP_CONTEXT_MENU,
  PROP_CONTEXT,
  PROP_CONTENT,
  N_PROPS
};

enum {
  SIG_CONTEXT_SET,
  N_SIGNALS
};

static GParamSpec *properties [N_PROPS];
static guint signals [N_SIGNALS];


static gboolean
tick_cb (gpointer user_data)
{
  GTask *task = user_data;
  PanelSaveDelegate *delegate = g_task_get_source_object (task);
  double progress = panel_save_delegate_get_progress (delegate);

  if (g_task_had_error (task) ||
      g_task_return_error_if_cancelled (task))
    return G_SOURCE_REMOVE;

  progress = CLAMP (progress+.005, .0, 1.);
  panel_save_delegate_set_progress (delegate, progress);

  if (progress >= 1.)
    {
      g_task_return_boolean (task, TRUE);
      return G_SOURCE_REMOVE;
    }

  return G_SOURCE_CONTINUE;
}

static gboolean
bimforge_viewport_save (BimforgeViewport    *self,
                      GTask             *task,
                      PanelSaveDelegate *delegate)
{
  g_assert (BIMFORGE_IS_VIEWPORT (self));
  g_assert (G_IS_TASK (task));
  g_assert (PANEL_IS_SAVE_DELEGATE (delegate));

  g_timeout_add_full (G_PRIORITY_DEFAULT,
                      30,
                      tick_cb,
                      g_object_ref (task),
                      g_object_unref);

  return TRUE;
}


/*
 * -----< PANEL_WIDGET CLASS IMPLEMENTATIONS >----- *
 */
static GtkWidget *
bimforge_viewport_get_default_focus (PanelWidget *widget)
{
  BimforgeViewport *self = (BimforgeViewport *)widget;
  BimforgeViewportPrivate *priv = bimforge_viewport_get_instance_private (self);

  return gtk_overlay_get_child (priv->overlay);
}


/*
 * -----< G_OBJECT CLASS IMPLEMENTATIONS >----- *
 */
static void
bimforge_viewport_dispose (GObject *object)
{
  BimforgeViewport *self = (BimforgeViewport *)object;
  BimforgeViewportPrivate *priv = bimforge_viewport_get_instance_private (self);

  g_clear_pointer ((GtkWidget **)&priv->overlay, gtk_widget_unparent);

  G_OBJECT_CLASS (bimforge_viewport_parent_class)->dispose (object);
}

static void
bimforge_viewport_finalize (GObject *object)
{
  BimforgeViewport *self = (BimforgeViewport *)object;
  BimforgeViewportPrivate *priv = bimforge_viewport_get_instance_private (self);

  g_object_unref (priv->context);

  G_OBJECT_CLASS (bimforge_viewport_parent_class)->finalize (object);
}

static void
bimforge_viewport_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  BimforgeViewport *self = BIMFORGE_VIEWPORT (object);

  switch (prop_id)
    {
    case PROP_HEADER_MENU:
      g_value_set_object (value, bimforge_viewport_get_header_menu (self));
      break;

    case PROP_CONTEXT_MENU:
      g_value_set_object (value, bimforge_viewport_get_context_menu (self));
      break;

    case PROP_CONTEXT:
      g_value_set_object (value, bimforge_viewport_get_context (self));
      break;

    case PROP_CONTENT:
      g_value_set_object (value, bimforge_viewport_get_content (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bimforge_viewport_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  BimforgeViewport *self = BIMFORGE_VIEWPORT (object);

  switch (prop_id)
    {
    case PROP_CONTENT:
      bimforge_viewport_set_content (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bimforge_viewport_class_init (BimforgeViewportClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  PanelWidgetClass *p_widget_class = PANEL_WIDGET_CLASS (klass);

  p_widget_class->get_default_focus = bimforge_viewport_get_default_focus;

  object_class->dispose = bimforge_viewport_dispose;
  object_class->finalize = bimforge_viewport_finalize;
  object_class->get_property = bimforge_viewport_get_property;
  object_class->set_property = bimforge_viewport_set_property;

  /**
   * BimforgeViewport:header-menu:
   *
   * The "Header-Menu" property is the menu that is going to be merged
   * with the viewport frame header bar menu in the workspace.
   *
   * Since: 0.1
   */
  properties [PROP_HEADER_MENU] =
    g_param_spec_object ("header-menu", NULL, NULL,
                         G_TYPE_MENU,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  /**
   * BimforgeViewport:context-menu:
   *
   * The "context-menu" property is the menu that is going to be displayed
   * when right clicking the viewport.
   *
   * Since: 0.1
   */
  properties [PROP_CONTEXT_MENU] =
    g_param_spec_object ("context-menu", NULL, NULL,
                         G_TYPE_MENU,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  /**
   * BimforgeViewport:context:
   *
   * The "context" property is the #ForgerContext object that is linked
   * with %self.
   *
   * Since: 0.1
   */
  properties [PROP_CONTEXT] =
    g_param_spec_object ("context", NULL, NULL,
                         FORGER_TYPE_CONTEXT,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  /**
   * BimforgeViewport:content:
   *
   * The "content" property is the child #GtkWidget of %self.
   *
   * Since: 0.1
   */
  properties [PROP_CONTENT] =
    g_param_spec_object ("content", NULL, NULL,
                         GTK_TYPE_WIDGET,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);


  /**
   * BimforgeViewport::context-set:
   *
   * The "context-set" signal is emitted after the context property
   * of @self is being set.
   *
   * Since: 0.1
   */
  signals [SIG_CONTEXT_SET] =
    g_signal_newv ("context-set",
                   G_TYPE_FROM_CLASS (object_class),
                   G_SIGNAL_RUN_LAST | G_SIGNAL_NO_HOOKS | G_SIGNAL_NO_RECURSE,
                   NULL, NULL, NULL, g_cclosure_marshal_generic,
                   G_TYPE_NONE, 0, NULL);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
  gtk_widget_class_set_accessible_role (widget_class, GTK_ACCESSIBLE_ROLE_DOCUMENT);
}

static void
bimforge_viewport_init (BimforgeViewport *self)
{
  BimforgeViewportPrivate *priv = bimforge_viewport_get_instance_private (self);
  PanelSaveDelegate *delegate = NULL;

  priv->overlay = GTK_OVERLAY (gtk_overlay_new ());

  panel_widget_set_child (PANEL_WIDGET (self), GTK_WIDGET (priv->overlay));
  panel_widget_set_kind (PANEL_WIDGET (self), PANEL_WIDGET_KIND_DOCUMENT);

  delegate = panel_save_delegate_new ();
  g_signal_connect_object (delegate,
                           "save",
                           G_CALLBACK (bimforge_viewport_save),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_swapped (delegate, "discard", G_CALLBACK (panel_widget_force_close), self);
  g_signal_connect_swapped (delegate, "close", G_CALLBACK (panel_widget_force_close), self);
  g_object_bind_property (self, "title", delegate, "title", G_BINDING_SYNC_CREATE);
  g_object_bind_property (self, "icon", delegate, "icon", G_BINDING_SYNC_CREATE);
  panel_widget_set_save_delegate (PANEL_WIDGET (self), delegate);
  g_clear_object (&delegate);
}


/*
 * -----< VIRTUAL METHODS >----- *
 */
/**
 * bimforge_viewport_get_context_menu:
 * @self: a #BimforgeViewport.
 *
 * Get the "header-menu" property value.
 * Inheritors should be responsible of creating the menu and freeing it.
 *
 * Returns: (transfer full) (nullable): a #GMenu.
 */
GMenu *
bimforge_viewport_get_header_menu (BimforgeViewport *self)
{
  g_return_val_if_fail (BIMFORGE_IS_VIEWPORT (self), NULL);

  if (BIMFORGE_VIEWPORT_GET_CLASS (self)->get_header_menu)
    return BIMFORGE_VIEWPORT_GET_CLASS (self)->get_header_menu (self);

  return NULL;
}

/**
 * bimforge_viewport_get_context_menu:
 * @self: a #BimforgeViewport.
 *
 * Get the "context-menu" property value.
 * Inheritors should be responsible of creating the menu and freeing it.
 *
 * Returns: (transfer full) (nullable): a #GMenu.
 */
GMenu *
bimforge_viewport_get_context_menu (BimforgeViewport *self)
{
  g_return_val_if_fail (BIMFORGE_IS_VIEWPORT (self), NULL);

  if (BIMFORGE_VIEWPORT_GET_CLASS (self)->get_context_menu)
    return BIMFORGE_VIEWPORT_GET_CLASS (self)->get_context_menu (self);

  return NULL;
}


/*
 * -----< PROPERTIES >----- *
 */
/**
 * bimforge_viewport_get_context:
 * @self: a #BimforgeViewport
 *
 * Get the "context" property.
 *
 * Returns: (transfer full): a #ForgerContext.
 */
ForgerContext *
bimforge_viewport_get_context (BimforgeViewport *self)
{
  BimforgeViewportPrivate *priv = bimforge_viewport_get_instance_private (self);
  g_return_val_if_fail (BIMFORGE_IS_VIEWPORT (self), NULL);
  return priv->context;
}

/**
 * bimforge_viewport_get_title:
 * @self: a #BimforgeViewport
 *
 * Get the "title" property (to be used with languages bindings only).
 *
 * Returns: (transfer full): a #gchar.
 */
gchar *
bimforge_viewport_get_title (BimforgeViewport *self)
{
  return (gchar *)panel_widget_get_title (PANEL_WIDGET (self));
}

/**
 * bimforge_viewport_set_title:
 * @self: a #BimforgeViewport
 * @title: a #gchar.
 *
 * Set the "title" property value of @self
 * (to be used with languages bindings only).
 *
 */
void
bimforge_viewport_set_title (BimforgeViewport *self,
                           const gchar    *title)
{
  panel_widget_set_title (PANEL_WIDGET (self), title);
}

/**
 * bimforge_viewport_get_icon_name:
 * @self: a #BimforgeViewport
 *
 * Get the "icon-name" property
 * (to be used with languages bindings only).
 *
 * Returns: (transfer full): a #gchar.
 */
gchar *
bimforge_viewport_get_icon_name (BimforgeViewport *self)
{
  return (gchar *)panel_widget_get_icon_name (PANEL_WIDGET (self));
}

/**
 * bimforge_viewport_set_icon_name:
 * @self: a #BimforgeViewport
 * @icon_name: a #gchar.
 *
 * Set the "icon-name" property value of @self.
 * (to be used with languages bindings only).
 *
 */
void
bimforge_viewport_set_icon_name (BimforgeViewport *self,
                               const gchar    *icon_name)
{
  panel_widget_set_icon_name (PANEL_WIDGET (self), icon_name);
}

/**
 * bimforge_viewport_get_content:
 * @self: a #BimforgeViewport
 *
 * Get the "content" property value of @self.
 *
 * Returns: (transfer full): a #GtkWidget.
 */
GtkWidget *
bimforge_viewport_get_content (BimforgeViewport *self)
{
  BimforgeViewportPrivate *priv = bimforge_viewport_get_instance_private (self);
  g_return_val_if_fail (BIMFORGE_IS_VIEWPORT (self), NULL);
  return gtk_overlay_get_child (priv->overlay);
}

/**
 * bimforge_viewport_set_content:
 * @self: a #BimforgeViewport
 * @child: a #GtkWidget.
 *
 * Set the "content" property value of @self.
 *
 */
void
bimforge_viewport_set_content (BimforgeViewport *self,
                             GtkWidget      *child)
{
  BimforgeViewportPrivate *priv = bimforge_viewport_get_instance_private (self);

  BIMFORGE_ENTRY;
  g_return_if_fail (BIMFORGE_IS_VIEWPORT (self));
  g_return_if_fail (GTK_IS_WIDGET (child));

  gtk_overlay_set_child (priv->overlay, child);
  gtk_widget_set_focus_child (GTK_WIDGET (self), child);
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_CONTENT]);
  BIMFORGE_EXIT;
}


/*
 * -----< OVERLAYS >----- *
 */
/**
 * bimforge_viewport_add_overlay:
 * @self: a #BimforgeViewport.
 * @child: a #GtkWidget.
 *
 * Add @child to the overlays list.
 * Any #GtkWidget in the overlays list is going to be drawn floating
 * on the screen over the content of @self.
 *
 */
void
bimforge_viewport_add_overlay (BimforgeViewport *self,
                             GtkWidget      *child)
{
  BimforgeViewportPrivate *priv = bimforge_viewport_get_instance_private (self);

  BIMFORGE_ENTRY;
  g_return_if_fail (BIMFORGE_IS_VIEWPORT (self));
  g_return_if_fail (GTK_IS_WIDGET (child));

  gtk_overlay_add_overlay (priv->overlay, child);

  BIMFORGE_EXIT;
}

/**
 * bimforge_viewport_remove_overlay:
 * @self: a #BimforgeViewport.
 * @child: a #GtkWidget.
 *
 * Remove @child from the overlays list.
 * Any #GtkWidget in the overlays list is going to be drawn floating
 * on the screen over the content of @self.
 *
 */
void
bimforge_viewport_remove_overlay (BimforgeViewport *self,
                                GtkWidget      *child)
{
  BimforgeViewportPrivate *priv = bimforge_viewport_get_instance_private (self);

  BIMFORGE_ENTRY;
  g_return_if_fail (BIMFORGE_IS_VIEWPORT (self));
  g_return_if_fail (GTK_IS_WIDGET (child));

  gtk_overlay_remove_overlay (priv->overlay, child);

  BIMFORGE_EXIT;
}


/*
 * -----< PRIVATE API >----- *
 */
GList *
_bimforge_viewport_get_link (BimforgeViewport *self)
{
  BimforgeViewportPrivate *priv = bimforge_viewport_get_instance_private (self);
  g_return_val_if_fail (BIMFORGE_IS_VIEWPORT (self), NULL);
  return &priv->link;
}

void
_bimforge_viewport_set_context (BimforgeViewport *self,
                                ForgerContext    *context)
{
  BimforgeViewportPrivate *priv = bimforge_viewport_get_instance_private (self);

  BIMFORGE_ENTRY;
  g_return_if_fail (BIMFORGE_IS_VIEWPORT (self));
  g_return_if_fail (FORGER_IS_CONTEXT (context));

  if (priv->context == context)
    BIMFORGE_EXIT;

  priv->context = g_object_ref (context);
  g_signal_emit (self, signals [SIG_CONTEXT_SET], 0);
  BIMFORGE_EXIT;
}

