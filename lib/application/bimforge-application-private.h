/*
 * bimforge-application-private.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <libpeas.h>

#include "bimforge-application.h"
#include "bimforge-app-global.h"

G_BEGIN_DECLS

struct _BimforgeApplication
{
  PanelApplication parent_instance;

  /* We keep a hashtable of GSettings for each of the loaded plugins
   * so that we can keep track if they are manually disabled using
   * the org.gnome.builder.plugin gschema.
   */
  GHashTable * plugin_settings;

  /* We need to track the GResource files that were manually loaded for
   * plugins on disk (generally Python plugins that need resources). That
   * way we can remove them when the plugin is unloaded.
   */
  GHashTable * plugin_gresources;

  /* CSS providers for each plugin that is loaded, indexed by the resource
   * path for the plugin/internal library.
   */
  GHashTable * css_providers;

  PeasExtensionSet * addins;

  gboolean loaded_typelibs;
};

void    _bimforge_application_add_resources            (BimforgeApplication * self,
                                                      const char *        resource_path);

void    _bimforge_application_init_actions             (BimforgeApplication * self);

void    _bimforge_application_load_plugins             (BimforgeApplication * self);
void    _bimforge_application_load_plugins_for_startup (BimforgeApplication * self);

void    _bimforge_application_load_addins              (BimforgeApplication * self);
void    _bimforge_application_unload_addins            (BimforgeApplication * self);


G_END_DECLS
