/*
 * bimforge_viewport.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(BIMFORGE_INSIDE) && !defined(BIMFORGE_COMPILATION)
#error "Only <libbimforge.h> can be included directly."
#endif

#include <adwaita.h>
#include <libpanel.h>
#include <libforger.h>

#include "bimforge-version-macros.h"

G_BEGIN_DECLS

#define BIMFORGE_TYPE_VIEWPORT (bimforge_viewport_get_type())

BIMFORGE_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (BimforgeViewport, bimforge_viewport, BIMFORGE, VIEWPORT, PanelWidget)

struct _BimforgeViewportClass
{
  PanelWidgetClass parent_class;

  GMenu *   (* get_header_menu)  (BimforgeViewport * viewport);
  GMenu *   (* get_context_menu) (BimforgeViewport * viewport);

  /* ---< PRIVATE >--- */
  gpointer padding [12];
};


BIMFORGE_AVAILABLE_IN_ALL
GMenu *           bimforge_viewport_get_header_menu     (BimforgeViewport *    self);
BIMFORGE_AVAILABLE_IN_ALL
GMenu *           bimforge_viewport_get_context_menu    (BimforgeViewport *    self);

BIMFORGE_AVAILABLE_IN_ALL
ForgerContext *   bimforge_viewport_get_context         (BimforgeViewport *    self);

BIMFORGE_AVAILABLE_IN_ALL
gchar *           bimforge_viewport_get_title           (BimforgeViewport *    self);
BIMFORGE_AVAILABLE_IN_ALL
void              bimforge_viewport_set_title           (BimforgeViewport *    self,
                                                       const gchar *       title);

BIMFORGE_AVAILABLE_IN_ALL
gchar *           bimforge_viewport_get_icon_name       (BimforgeViewport *    self);
BIMFORGE_AVAILABLE_IN_ALL
void              bimforge_viewport_set_icon_name       (BimforgeViewport *    self,
                                                       const gchar *       icon_name);

BIMFORGE_AVAILABLE_IN_ALL
GtkWidget *       bimforge_viewport_get_content         (BimforgeViewport *    self);
BIMFORGE_AVAILABLE_IN_ALL
void              bimforge_viewport_set_content         (BimforgeViewport *    self,
                                                       GtkWidget *         child);

BIMFORGE_AVAILABLE_IN_ALL
void              bimforge_viewport_add_overlay         (BimforgeViewport *    self,
                                                       GtkWidget *         child);
BIMFORGE_AVAILABLE_IN_ALL
void              bimforge_viewport_remove_overlay      (BimforgeViewport *    self,
                                                       GtkWidget *         child);


G_END_DECLS

