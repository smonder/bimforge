/*
 * bimforge-application.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#if !defined(BIMFORGE_INSIDE) && !defined(BIMFORGE_COMPILATION)
#error "Only <libbimforge.h> can be included directly."
#endif

#include <adwaita.h>
#include <libpanel.h>
#include "bimforge-version-macros.h"

G_BEGIN_DECLS

#define BIMFORGE_TYPE_APPLICATION (bimforge_application_get_type())
#define BIMFORGE_APPLICATION_DEFAULT (BIMFORGE_APPLICATION (g_application_get_default()))

BIMFORGE_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (BimforgeApplication, bimforge_application, BIMFORGE, APPLICATION, PanelApplication)

BIMFORGE_AVAILABLE_IN_ALL
BimforgeApplication * bimforge_application_new          (void) G_GNUC_WARN_UNUSED_RESULT;

G_END_DECLS
