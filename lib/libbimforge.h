/* libbimforge.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#define BIMFORGE_INSIDE

#include "bimforge-debug.h"
#include "bimforge-enums.h"
#include "bimforge-init.h"
#include "bimforge-types.h"
#include "bimforge-version.h"
#include "bimforge-version-macros.h"

#include "application/bimforge-app-global.h"
#include "application/bimforge-application.h"
#include "application/bimforge-application-addin.h"
#include "application/bimforge-page.h"
#include "application/bimforge-viewport.h"
#include "application/bimforge-workspace.h"
#include "application/bimforge-workspace-addin.h"

// #include "pages/bimforge-cameras-page.h"
// #include "pages/bimforge-curves-page.h"
// #include "pages/bimforge-entities-page.h"
// #include "pages/bimforge-tools-page.h"

#include "tree/bimforge-tree.h"
#include "tree/bimforge-tree-addin.h"
#include "tree/bimforge-tree-node.h"

#include "widgets/bimforge-gizmo-manipulator.h"
#include "widgets/bimforge-wvec3.h"

#undef BIMFORGE_INSIDE
