# LIBBimforge Design

The goal of libbimforge is to provide a graphic framework for simulation and testing
purposes ...

## BimforgeShader
Right now our shader system is working fine and efficiently (according to the
development of the project).
The only take on I think of right now is that we should delay the compilation
and linking process and separate it from shader construction so that we can
create the shader object and give it its source code before there is an active
GL Context and therefore the shader creation can happen before the realization
of **BimforgeCanvas**. We can then create shaders when the project is loading and
compile them when the GUI of BIMFORGE is realized.

## BimforgeGadget
**BimforgeGadget** is going to hold not only the VertexArray but also the vertex
and the index buffers and also the VertexAttribLayout.
The creation and init of these components is going to be abstracted away from
the inheritors of **BimforgeGadget** and we should offer calls to add buffers
or to set indices.

**BIMFORGE_GADGET_DRAW_ALL (gadget)** is a temporary call for testing purposes only.
Later, the draw calls should be held by the engine because it is responsible for
binding materials (shaders, textures, etc.) before each draw call for these are
outside the scope of **BimforgeGadget**, and **BimforgeGadget** is only going to hold an
index, or simply a string containing the name of the material, to search for in
the material box of the active BimforgeContext.

## BimforgeEventHandler
We need to add functions that are going to be called in the events' signal callbacks
of **BimforgeCanvas**.
Right now I'm only thinking about:
  1. on_keypressed(): Is going to be called in the
     "BimforgeCanvas.GtkEventControllerKey::key_pressed" signal callback and in order
     to correctly implement it, we need to have a shortcut manager first that is
     responsible for Keybindings.

## BimforgeCamera
**BimforgeCamera** is not only a controller that changes the view in BimforgeCanvas.
it has also a geometry to represent it in the canvas and a name to be treated with
in cameras list.

## BimforgeEngine
When I think about the relationship between **BimforgeCanvas**, **BimforgeEngine** and
**BimforgeContext**, I always imagine it like this:
  1. The context should be hidden from the canvas and there should be no direct
     relationship between the two.
  2. The engine can access both the canvas and the context.
  3. This is obviously an **MVC** pattern in action, where BimforgeCanvas is the VIEWER,
     BimforgeContext is the MODEL and BimforgeEngine is the CONTROLLER.

