/* bimforge-version-macros.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(BIMFORGE_INSIDE) && !defined(BIMFORGE_COMPILATION)
#error "Only <libbimforge.h> can be included directly."
#endif

#include <glib.h>

#include "bimforge-version.h"

#ifndef _BIMFORGE_EXTERN
#define _BIMFORGE_EXTERN extern
#endif

#ifdef BIMFORGE_DISABLE_DEPRECATION_WARNINGS
# define BIMFORGE_DEPRECATED _BIMFORGE_EXTERN
# define BIMFORGE_DEPRECATED_FOR(f) _BIMFORGE_EXTERN
# define BIMFORGE_UNAVAILABLE(maj,min) _BIMFORGE_EXTERN
#else
# define BIMFORGE_DEPRECATED G_DEPRECATED _BIMFORGE_EXTERN
# define BIMFORGE_DEPRECATED_FOR(f) G_DEPRECATED_FOR(f) _BIMFORGE_EXTERN
# define BIMFORGE_UNAVAILABLE(maj,min) G_UNAVAILABLE(maj,min) _BIMFORGE_EXTERN
#endif

#define BIMFORGE_VERSION_1_0 (G_ENCODE_VERSION (1, 0))

#if (BIMFORGE_MINOR_VERSION == 99)
# define BIMFORGE_VERSION_CUR_STABLE (G_ENCODE_VERSION (BIMFORGE_MAJOR_VERSION + 1, 0))
#elif (BIMFORGE_MINOR_VERSION % 2)
# define BIMFORGE_VERSION_CUR_STABLE (G_ENCODE_VERSION (BIMFORGE_MAJOR_VERSION, BIMFORGE_MINOR_VERSION + 1))
#else
# define BIMFORGE_VERSION_CUR_STABLE (G_ENCODE_VERSION (BIMFORGE_MAJOR_VERSION, BIMFORGE_MINOR_VERSION))
#endif

#if (BIMFORGE_MINOR_VERSION == 99)
# define BIMFORGE_VERSION_PREV_STABLE (G_ENCODE_VERSION (BIMFORGE_MAJOR_VERSION + 1, 0))
#elif (BIMFORGE_MINOR_VERSION % 2)
# define BIMFORGE_VERSION_PREV_STABLE (G_ENCODE_VERSION (BIMFORGE_MAJOR_VERSION, BIMFORGE_MINOR_VERSION - 1))
#else
# define BIMFORGE_VERSION_PREV_STABLE (G_ENCODE_VERSION (BIMFORGE_MAJOR_VERSION, BIMFORGE_MINOR_VERSION - 2))
#endif

/**
 * BIMFORGE_VERSION_MIN_REQUIRED:
 *
 * A macro that should be defined by the user prior to including
 * the libbimforge.h header.
 *
 * The definition should be one of the predefined BIMFORGE version
 * macros: %BIMFORGE_VERSION_1_0, ...
 *
 * This macro defines the lower bound for the BIMFORGE API to use.
 *
 * If a function has been deprecated in a newer version of BIMFORGE,
 * it is possible to use this symbol to avoid the compiler warnings
 * without disabling warning for every deprecated function.
 */
#ifndef BIMFORGE_VERSION_MIN_REQUIRED
# define BIMFORGE_VERSION_MIN_REQUIRED (BIMFORGE_VERSION_CUR_STABLE)
#endif

/**
 * BIMFORGE_VERSION_MAX_ALLOWED:
 *
 * A macro that should be defined by the user prior to including
 * the libbimforge.h header.

 * The definition should be one of the predefined BIMFORGE version
 * macros: %BIMFORGE_VERSION_1_0, %BIMFORGE_VERSION_1_2,...
 *
 * This macro defines the upper bound for the BIMFORGE API to use.
 *
 * If a function has been introduced in a newer version of BIMFORGE,
 * it is possible to use this symbol to get compiler warnings when
 * trying to use that function.
 */
#ifndef BIMFORGE_VERSION_MAX_ALLOWED
# if BIMFORGE_VERSION_MIN_REQUIRED > BIMFORGE_VERSION_PREV_STABLE
#  define BIMFORGE_VERSION_MAX_ALLOWED (BIMFORGE_VERSION_MIN_REQUIRED)
# else
#  define BIMFORGE_VERSION_MAX_ALLOWED (BIMFORGE_VERSION_CUR_STABLE)
# endif
#endif

#if BIMFORGE_VERSION_MAX_ALLOWED < BIMFORGE_VERSION_MIN_REQUIRED
#error "BIMFORGE_VERSION_MAX_ALLOWED must be >= BIMFORGE_VERSION_MIN_REQUIRED"
#endif
#if BIMFORGE_VERSION_MIN_REQUIRED < BIMFORGE_VERSION_1_0
#error "BIMFORGE_VERSION_MIN_REQUIRED must be >= BIMFORGE_VERSION_1_0"
#endif

#define BIMFORGE_AVAILABLE_IN_ALL                  _BIMFORGE_EXTERN

#if BIMFORGE_VERSION_MIN_REQUIRED >= BIMFORGE_VERSION_1_0
# define BIMFORGE_DEPRECATED_IN_1_0                BIMFORGE_DEPRECATED
# define BIMFORGE_DEPRECATED_IN_1_0_FOR(f)         BIMFORGE_DEPRECATED_FOR(f)
#else
# define BIMFORGE_DEPRECATED_IN_1_0                _BIMFORGE_EXTERN
# define BIMFORGE_DEPRECATED_IN_1_0_FOR(f)         _BIMFORGE_EXTERN
#endif

#if BIMFORGE_VERSION_MAX_ALLOWED < BIMFORGE_VERSION_1_0
# define BIMFORGE_AVAILABLE_IN_1_0                 BIMFORGE_UNAVAILABLE(1, 0)
#else
# define BIMFORGE_AVAILABLE_IN_1_0                 _BIMFORGE_EXTERN
#endif
