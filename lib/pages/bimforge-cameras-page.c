/* bimforge-cameras-page.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bimforge-cameras-page"

#include "config.h"
#include "bimforge-debug.h"

#include "bimforge-cameras-page.h"

struct _BimforgeCamerasPage
{
  BimforgePage parent_instance;

  GtkListBox * box;
  GtkButton * header_button;
};

G_DEFINE_FINAL_TYPE (BimforgeCamerasPage, bimforge_cameras_page, BIMFORGE_TYPE_PAGE)

static guint counter;


static void
on_header_button_clicked_cb (BimforgeCamerasPage *self,
                             GtkButton       *button)
{
  BimforgeContext * context = bimforge_page_get_context (BIMFORGE_PAGE (self));
  BimforgeCamera *new_camera;

  new_camera = bimforge_camera_new_default (g_strdup_printf ("new_camera_%d", counter), NULL);
  bimforge_context_add_camera (context, new_camera);
  bimforge_context_bind_camera (context, new_camera);
  bimforge_context_bind_tool (context, BIMFORGE_EVENT_HANDLER (bimforge_context_get_active_camera (context)));
  counter ++;
}

static void
on_camera_row_activated_cb (BimforgeContext *context,
                            AdwActionRow  *row)
{
  BimforgeCamera *camera;

  g_return_if_fail (BIMFORGE_IS_CONTEXT (context));
  g_return_if_fail (ADW_IS_ACTION_ROW (row));

  camera = g_object_get_data (G_OBJECT (row), "camera");
  g_assert (BIMFORGE_IS_CAMERA (camera));

  bimforge_context_bind_camera (context, camera);
}

static GtkWidget *
create_row (gpointer item,
            gpointer user_data)
{
  BimforgeCamerasPage *self = (BimforgeCamerasPage *)user_data;
  BimforgeCamera *camera = (BimforgeCamera *)item;
  AdwActionRow * camera_row;

  g_assert (BIMFORGE_IS_CAMERAS_PAGE (self));
  g_assert (BIMFORGE_IS_CAMERA (camera));

  camera_row = g_object_new (ADW_TYPE_ACTION_ROW,
                             "title", bimforge_entity_dup_name (BIMFORGE_ENTITY (camera)),
                             "icon-name", "camera-web-symbolic",
                             "tooltip-text", bimforge_camera_get_description (camera),
                             "activatable", TRUE,
                             NULL);

  g_object_set_data (G_OBJECT (camera_row), "camera", camera);
  g_signal_connect_swapped (camera_row, "activated",
                            G_CALLBACK (on_camera_row_activated_cb),
                            bimforge_page_get_context (BIMFORGE_PAGE (self)));

  return (GtkWidget *)camera_row;
}

static void
on_context_set_cb (BimforgePage *page,
                   gpointer user_data)
{
  BimforgeCamerasPage *self = (BimforgeCamerasPage *)page;
  BimforgeContext *context;

  g_return_if_fail (BIMFORGE_IS_CAMERAS_PAGE (self));

  context = bimforge_page_get_context (page);

  gtk_list_box_bind_model (self->box,
                           bimforge_context_get_cameras (context),
                           create_row, self, NULL);
}

static void
bimforge_cameras_page_dispose (GObject *object)
{
  BimforgeCamerasPage *self = (BimforgeCamerasPage *)object;
  GtkWidget *child = NULL;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (self))) != NULL)
    gtk_widget_unparent (child);

  G_OBJECT_CLASS (bimforge_cameras_page_parent_class)->dispose (object);
}

static void
bimforge_cameras_page_class_init (BimforgeCamerasPageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = bimforge_cameras_page_dispose;

  counter = 0;
}

static void
bimforge_cameras_page_init (BimforgeCamerasPage *self)
{
  AdwButtonContent *btn_con;

  bimforge_page_set_title (BIMFORGE_PAGE (self), "Cameras");
  bimforge_page_set_icon_name (BIMFORGE_PAGE (self), "camera-web-symbolic");

  self->box = g_object_new (GTK_TYPE_LIST_BOX,
                            "activate-on-single-click", TRUE,
                            "selection-mode", GTK_SELECTION_NONE,
                            "show-separators", FALSE,
                            NULL);
  gtk_widget_add_css_class (GTK_WIDGET (self->box), "boxed-list");
  bimforge_page_append_child (BIMFORGE_PAGE (self), GTK_WIDGET (self->box));

  btn_con = g_object_new (ADW_TYPE_BUTTON_CONTENT,
                          "icon-name", "list-add-symbolic",
                          "label", "Add",
                          NULL);
  self->header_button = g_object_new (GTK_TYPE_BUTTON, NULL);
  gtk_button_set_child (self->header_button, GTK_WIDGET (btn_con));
  gtk_widget_add_css_class (GTK_WIDGET (self->header_button), "flat");
  bimforge_page_set_header_suffix (BIMFORGE_PAGE (self), GTK_WIDGET (self->header_button));
  g_signal_connect_swapped (self->header_button, "clicked", G_CALLBACK (on_header_button_clicked_cb), self);

  g_signal_connect (self, "context-set", G_CALLBACK (on_context_set_cb), NULL);
}


BimforgePage *
bimforge_cameras_page_new (void)
{
  return g_object_new (BIMFORGE_TYPE_CAMERAS_PAGE, NULL);
}

