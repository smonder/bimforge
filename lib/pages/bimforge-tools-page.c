/* bimforge-tools-page.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bimforge-tools-page"

#include "config.h"
#include "bimforge-debug.h"

#include "bimforge-tools-page.h"

struct _BimforgeToolsPage
{
  BimforgePage parent_instance;

  GListModel * tools_list;

  GtkListBox * box;
  GtkButton * header_button;
};

G_DEFINE_FINAL_TYPE (BimforgeToolsPage, bimforge_tools_page, BIMFORGE_TYPE_PAGE)

enum {
  PROP_0,
  PROP_TOOLS_LIST,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
on_context_set_cb (BimforgePage *page)
{
  BimforgeToolsPage *self = (BimforgeToolsPage *)page;
  BimforgeContext *context;

  g_assert (BIMFORGE_IS_TOOLS_PAGE (self));

  context = bimforge_page_get_context (page);
}

static void
bimforge_tools_page_dispose (GObject *object)
{
  BimforgeToolsPage *self = (BimforgeToolsPage *)object;
  GtkWidget *child = NULL;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (self))) != NULL)
    gtk_widget_unparent (child);

  G_OBJECT_CLASS (bimforge_tools_page_parent_class)->dispose (object);
}

static void
bimforge_tools_page_finalize (GObject *object)
{
  BimforgeToolsPage *self = (BimforgeToolsPage *)object;

  g_clear_object (&self->tools_list);

  G_OBJECT_CLASS (bimforge_tools_page_parent_class)->finalize (object);
}

static void
bimforge_tools_page_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  BimforgeToolsPage *self = BIMFORGE_TOOLS_PAGE (object);

  switch (prop_id)
    {
    case PROP_TOOLS_LIST:
      g_value_set_object (value, bimforge_tools_page_get_list (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bimforge_tools_page_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  BimforgeToolsPage *self = BIMFORGE_TOOLS_PAGE (object);

  switch (prop_id)
    {
    case PROP_TOOLS_LIST:
      bimforge_tools_page_bind_list (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bimforge_tools_page_class_init (BimforgeToolsPageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = bimforge_tools_page_dispose;
  object_class->finalize = bimforge_tools_page_finalize;
  object_class->get_property = bimforge_tools_page_get_property;
  object_class->set_property = bimforge_tools_page_set_property;

  /**
   * BimforgeToolsPage:tools-list:
   *
   * The "tools-list" property is the #GListModel of tools that
   * BIMFORGE application is offering to handle the viewport.
   * This is obtained by calling bimforge_application_get_tools ().
   *
   * Since: 0.1
   */
  properties [PROP_TOOLS_LIST] =
    g_param_spec_object ("tools-list",
                         "Tools List",
                         "The list of tools that BIMFORGE is offering.",
                         G_TYPE_LIST_MODEL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
bimforge_tools_page_init (BimforgeToolsPage *self)
{
  AdwButtonContent *btn_con;

  bimforge_page_set_title (BIMFORGE_PAGE (self), "Tools and Modifiers");
  bimforge_page_set_icon_name (BIMFORGE_PAGE (self), "preferences-other-symbolic");

  self->box = g_object_new (GTK_TYPE_LIST_BOX,
                            "activate-on-single-click", TRUE,
                            "selection-mode", GTK_SELECTION_NONE,
                            "show-separators", FALSE,
                            NULL);
  gtk_widget_add_css_class (GTK_WIDGET (self->box), "boxed-list");
  bimforge_page_append_child (BIMFORGE_PAGE (self), GTK_WIDGET (self->box));

  btn_con = g_object_new (ADW_TYPE_BUTTON_CONTENT,
                          "icon-name", "view-refresh-symbolic",
                          "label", "Refresh",
                          NULL);
  self->header_button = g_object_new (GTK_TYPE_BUTTON, NULL);
  gtk_button_set_child (self->header_button, GTK_WIDGET (btn_con));
  gtk_widget_add_css_class (GTK_WIDGET (self->header_button), "flat");
  bimforge_page_set_header_suffix (BIMFORGE_PAGE (self), GTK_WIDGET (self->header_button));
  /* g_signal_connect_swapped (self->header_button, "clicked", G_CALLBACK (on_header_button_clicked_cb), self); */

  g_signal_connect (self, "context-set", G_CALLBACK (on_context_set_cb), NULL);
}


/**
 * bimforge_tools_page_new:
 *
 * Creates a new #BimforgeToolsPage.
 *
 * Returns: a newly created #BimforgePage.
 *
 * Since: 0.1
 */
BimforgePage *
bimforge_tools_page_new (void)
{
  return g_object_new (BIMFORGE_TYPE_TOOLS_PAGE, NULL);
}


/**
 * bimforge_tools_page_get_list:
 * @self: a #BimforgeToolsPage.
 *
 * Gets the #GListModel that is currently bound to @self.
 *
 * Returns: (transfer full) a #GListModel or %NULL.
 *
 * Since: 0.1
 */
GListModel *
bimforge_tools_page_get_list (BimforgeToolsPage *self)
{
  g_return_val_if_fail (BIMFORGE_IS_TOOLS_PAGE (self), NULL);

  return self->tools_list;
}


static void
on_tool_row_activated_cb (BimforgeContext *context,
                          AdwActionRow  *row)
{
  BimforgeEventHandler *tool;

  g_return_if_fail (BIMFORGE_IS_CONTEXT (context));
  g_return_if_fail (ADW_IS_ACTION_ROW (row));

  tool = g_object_get_data (G_OBJECT (row), "tool");
  g_assert (BIMFORGE_IS_EVENT_HANDLER (tool));

  bimforge_context_bind_tool (context, tool);
}

static GtkWidget *
create_row (gpointer item,
            gpointer user_data)
{
  BimforgeToolsPage *self = (BimforgeToolsPage *)user_data;
  BimforgeEventHandler *tool = (BimforgeEventHandler *)item;
  AdwActionRow * tool_row;

  g_assert (BIMFORGE_IS_TOOLS_PAGE (self));
  g_assert (BIMFORGE_IS_EVENT_HANDLER (tool));

  tool_row = g_object_new (ADW_TYPE_ACTION_ROW,
                           "title", bimforge_event_handler_get_name (tool),
                           "icon-name", bimforge_event_handler_get_icon_name (tool),
                           "tooltip-text", bimforge_event_handler_get_tooltip (tool),
                           "activatable", TRUE,
                           NULL);

  g_object_set_data (G_OBJECT (tool_row), "tool", tool);
  g_signal_connect_swapped (tool_row, "activated",
                            G_CALLBACK (on_tool_row_activated_cb),
                            bimforge_page_get_context (BIMFORGE_PAGE (self)));

  return (GtkWidget *)tool_row;
}

/**
 * bimforge_tools_page_bind_list:
 * @self: a #BimforgeToolsPage.
 * @list: a #GListModel.
 *
 * Binds @list to @self.
 *
 * Since: 0.1
 */
void
bimforge_tools_page_bind_list (BimforgeToolsPage *self,
                           GListModel    *list)
{
  g_return_if_fail (BIMFORGE_IS_TOOLS_PAGE (self));
  g_return_if_fail (G_IS_LIST_MODEL (list));

  if (g_set_object (&self->tools_list, list))
    {
      gtk_list_box_bind_model (self->box,
                               self->tools_list,
                               create_row, self, NULL);

      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_TOOLS_LIST]);
    }
}

