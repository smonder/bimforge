/* bimforge-entities-page.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bimforge-entities-page"

#include "config.h"
#include "bimforge-debug.h"

#include <glib/gi18n.h>

#include "bimforge-entities-page.h"
#include "entity/bimforge-entity.h"
#include "widgets/bimforge-gizmo-manipulator.h"
#include "widgets/bimforge-wvec3.h"

struct _BimforgeEntitiesPage
{
  BimforgePage parent_instance;

  GtkListBox * box;
  GtkButton *  header_button;
};

G_DEFINE_FINAL_TYPE (BimforgeEntitiesPage, bimforge_entities_page, BIMFORGE_TYPE_PAGE)


static void
on_open_file_finished_cb (GObject      *source_object,
                          GAsyncResult *res,
                          gpointer      data)
{
  GtkFileDialog * dlg = (GtkFileDialog *)source_object;
  BimforgeEntity * entity = (BimforgeEntity *)data;
  BimforgeTexture *new_texture;
  g_autoptr (GFile) file = NULL;

  g_return_if_fail (GTK_IS_FILE_DIALOG (dlg));
  g_return_if_fail (BIMFORGE_IS_ENTITY (entity));

  file = gtk_file_dialog_open_finish (dlg, res, NULL);
  if (!file)
    {
      g_critical ("Unable to open image file");
      return;
    }

  new_texture = bimforge_texture_new_from_file (g_file_get_path (file));
  bimforge_entity_set_texture (entity, new_texture);
}

static void
on_change_texture_row_activated_cb (BimforgeEntity *entity,
                                    AdwActionRow *row)
{
  GtkFileDialog * dlg;
  GtkFileFilter * filter;

  g_return_if_fail (BIMFORGE_IS_ENTITY (entity));
  g_return_if_fail (ADW_IS_ACTION_ROW (row));

  dlg = gtk_file_dialog_new ();
  filter = gtk_file_filter_new ();
  gtk_file_filter_set_name (filter, "Images");
  gtk_file_filter_add_pixbuf_formats (filter);
  gtk_file_dialog_set_default_filter (dlg, filter);

  gtk_file_dialog_open (dlg, NULL, NULL,
                        on_open_file_finished_cb, entity);
}

static void
on_modify_texture_row_activated_cb (BimforgeEntity *entity,
                                    AdwActionRow *row)
{
  BimforgeTexture *texture;
  guchar buffer[100];

  g_return_if_fail (BIMFORGE_IS_ENTITY (entity));
  g_return_if_fail (ADW_IS_ACTION_ROW (row));

  texture = bimforge_entity_get_texture (entity);

  for (guint i = 0; i < 100; i++)
    buffer [i] = (guchar)0xff000000;

  for (gint y = 0; y < bimforge_texture_get_height (texture); y += 10)
    for (gint x = 0; x < bimforge_texture_get_width (texture); x += 10)
      {
        bimforge_texture_modify_subbuffer (texture, x, y,
                                       10,
                                       10,
                                       buffer);
      }
}

static void
on_destroy_entity_row_activated_cb (BimforgeEntity *entity,
                                    AdwActionRow *row)
{
  GtkWidget *self;

  g_return_if_fail (BIMFORGE_IS_ENTITY (entity));
  g_return_if_fail (ADW_IS_ACTION_ROW (row));

  self = gtk_widget_get_parent (GTK_WIDGET (row));

  while (!BIMFORGE_IS_ENTITIES_PAGE (self))
    self = gtk_widget_get_parent (GTK_WIDGET (self));

  if (BIMFORGE_IS_ENTITIES_PAGE (self))
    {
      /* bimforge_context_select_entity (bimforge_page_get_context (BIMFORGE_PAGE (self)), NULL); */
      bimforge_context_remove_entity (bimforge_page_get_context (BIMFORGE_PAGE (self)),
                                    entity);
    }
}


static GtkWidget *
create_row (gpointer item,
            gpointer user_data)
{
  BimforgeEntitiesPage * self = (BimforgeEntitiesPage *)user_data;
  BimforgeEntity * entity = (BimforgeEntity *)item;
  GtkBuilder *builder;
  AdwExpanderRow * entity_row;
  AdwEntryRow * name_row;
  AdwSwitchRow * sensitive_row;
  BimforgeGizmoManipulator * gizmo_manipulator;
  AdwActionRow * change_texture_row;
  AdwActionRow * modify_texture_row;
  AdwActionRow * destroy_entity_row;

  g_assert (BIMFORGE_IS_ENTITIES_PAGE (self));
  g_assert (BIMFORGE_IS_ENTITY (entity));

  builder = gtk_builder_new_from_resource ("/io/sam/libbimforge/pages/bimforge-entities-page.ui");

  entity_row = ADW_EXPANDER_ROW (gtk_builder_get_object (builder, "entity_row"));
  g_object_bind_property (entity, "name", entity_row, "title", G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
  g_object_bind_property (entity, "visible", entity_row, "enable-expansion", G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);

  name_row = ADW_ENTRY_ROW (gtk_builder_get_object (builder, "name_row"));
  g_object_bind_property (entity, "name", name_row, "text", G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);

  sensitive_row = ADW_SWITCH_ROW (gtk_builder_get_object (builder, "sensitive_row"));
  g_object_bind_property (entity, "sensitive", sensitive_row, "active", G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);

  gizmo_manipulator = BIMFORGE_GIZMO_MANIPULATOR (gtk_builder_get_object (builder, "gizmo_manipulator"));
  g_object_bind_property (entity, "sensitive", gizmo_manipulator, "sensitive", G_BINDING_SYNC_CREATE);
  bimforge_gizmo_manipulator_set_gizmo (gizmo_manipulator, bimforge_entity_get_gizmo (entity));

  change_texture_row = ADW_ACTION_ROW (gtk_builder_get_object (builder, "change_texture_row"));
  g_object_bind_property (entity, "sensitive", change_texture_row, "sensitive", G_BINDING_SYNC_CREATE);
  g_signal_connect_swapped (change_texture_row, "activated", G_CALLBACK (on_change_texture_row_activated_cb), entity);

  modify_texture_row = ADW_ACTION_ROW (gtk_builder_get_object (builder, "modify_texture_row"));
  g_object_bind_property (entity, "sensitive", modify_texture_row, "sensitive", G_BINDING_SYNC_CREATE);
  g_signal_connect_swapped (modify_texture_row, "activated", G_CALLBACK (on_modify_texture_row_activated_cb), entity);

  destroy_entity_row = ADW_ACTION_ROW (gtk_builder_get_object (builder, "destroy_entity_row"));
  g_object_bind_property (entity, "sensitive", destroy_entity_row, "sensitive", G_BINDING_SYNC_CREATE);
  g_signal_connect_swapped (destroy_entity_row, "activated", G_CALLBACK (on_destroy_entity_row_activated_cb), entity);

  return (GtkWidget *)entity_row;
}

static void
on_context_set_cb (BimforgePage *page,
                   gpointer user_data)
{
  BimforgeEntitiesPage *self = (BimforgeEntitiesPage *)page;
  BimforgeContext *context;

  g_return_if_fail (BIMFORGE_IS_ENTITIES_PAGE (self));

  context = bimforge_page_get_context (page);

  gtk_list_box_bind_model (self->box,
                           bimforge_context_get_entities (context),
                           create_row, self, NULL);
}

static void
bimforge_entities_page_dispose (GObject *object)
{
  BimforgeEntitiesPage *self = (BimforgeEntitiesPage *)object;

  gtk_widget_dispose_template (GTK_WIDGET (self), BIMFORGE_TYPE_ENTITIES_PAGE);

  G_OBJECT_CLASS (bimforge_entities_page_parent_class)->dispose (object);
}

static void
bimforge_entities_page_class_init (BimforgeEntitiesPageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = bimforge_entities_page_dispose;

  g_type_ensure (BIMFORGE_TYPE_WVEC3);
}

static void
bimforge_entities_page_init (BimforgeEntitiesPage *self)
{
  bimforge_page_set_title (BIMFORGE_PAGE (self), "Entities");
  bimforge_page_set_icon_name (BIMFORGE_PAGE (self), "package-x-generic-symbolic");

  self->box = g_object_new (GTK_TYPE_LIST_BOX,
                            "activate-on-single-click", TRUE,
                            "selection-mode", GTK_SELECTION_NONE,
                            "show-separators", FALSE,
                            NULL);
  gtk_widget_add_css_class (GTK_WIDGET (self->box), "boxed-list");
  bimforge_page_append_child (BIMFORGE_PAGE (self), GTK_WIDGET (self->box));
  g_signal_connect (self, "context-set", G_CALLBACK (on_context_set_cb), NULL);
}


/**
 * bimforge_entities_page_new:
 *
 * Creates a new #BimforgeEntitiesPage.
 *
 * Returns: a newly created #GtkWidget.
 *
 * Since: 0.1
 */
BimforgePage *
bimforge_entities_page_new (void)
{
  return g_object_new (BIMFORGE_TYPE_ENTITIES_PAGE, NULL);
}

