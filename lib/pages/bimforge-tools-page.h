/* bimforge-tools-page.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(BIMFORGE_INSIDE) && !defined(BIMFORGE_COMPILATION)
#error "Only <libbimforge.h> can be included directly."
#endif

#include "application/bimforge-page.h"
#include "bimforge-version-macros.h"

G_BEGIN_DECLS

#define BIMFORGE_TYPE_TOOLS_PAGE (bimforge_tools_page_get_type())

BIMFORGE_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (BimforgeToolsPage, bimforge_tools_page, BIMFORGE, TOOLS_PAGE, BimforgePage)


BIMFORGE_AVAILABLE_IN_ALL
BimforgePage *    bimforge_tools_page_new        (void) G_GNUC_WARN_UNUSED_RESULT;

BIMFORGE_AVAILABLE_IN_ALL
GListModel *    bimforge_tools_page_get_list   (BimforgeToolsPage * self);
BIMFORGE_AVAILABLE_IN_ALL
void            bimforge_tools_page_bind_list  (BimforgeToolsPage * self,
                                              GListModel *      list);

G_END_DECLS
