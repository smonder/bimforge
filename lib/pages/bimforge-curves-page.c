/*
 * bimforge-curves-page.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bimforge-curves-page"

#include "config.h"
#include "bimforge-debug.h"
#include "bimforge-curves-page.h"
#include "entity/bimforge-curve.h"
#include "renderer/bimforge-gadget-private.h"
#include "widgets/bimforge-gizmo-manipulator.h"

/**
 * SECTION: BimforgeCurvesPage
 * @short_description: A utility page to handle creation of curves.
 * @title: BimforgeCurvesPage.
 *
 * This is the page responsible for dealing with curves from the main
 * window of BIMFORGE.
 *
 * Since: 0.1
 */

struct _BimforgeCurvesPage
{
  BimforgePage parent_instance;

  BimforgeCurve *            active_curve;

  /* -----< TEMPLATE WIDGETS >----- */
  AdwEntryRow *            curve_name;
  AdwSwitchRow *           curve_visible;
  AdwSwitchRow *           curve_sensitive;
  AdwSwitchRow *           curve_cyclic;
  GtkColorDialogButton *   curve_color;
  AdwSpinRow *             curve_thickness;
  BimforgeGizmoManipulator * curve_transforms;
  GtkTreeView *            points_list;
  GtkEntry *               new_point_x;
  GtkEntry *               new_point_y;
  GtkEntry *               new_point_z;

  /* -----< PROPERTY BINDINGS >----- */
  GBinding * curve_name_bind;
  GBinding * curve_visible_bind;
  GBinding * curve_sensitive_bind;
  GBinding * curve_cyclic_bind;
  GBinding * curve_color_bind;
  GBinding * curve_thickness_bind;
};

enum
{
  COLUMN_INDEX,
  COLUMN_X,
  COLUMN_Y,
  COLUMN_Z,
  NUM_COLUMNS
};

G_DEFINE_FINAL_TYPE (BimforgeCurvesPage, bimforge_curves_page, BIMFORGE_TYPE_PAGE)


static void
bimforge_curves_page_reset_state (BimforgeCurvesPage *self)
{
  const GdkRGBA color_black = { 0.0f, 0.0f, 0.0f, 1.0f };
  g_assert (BIMFORGE_IS_CURVES_PAGE (self));

  gtk_editable_set_text (GTK_EDITABLE (self->curve_name), "");
  adw_switch_row_set_active (self->curve_visible, FALSE);
  adw_switch_row_set_active (self->curve_sensitive, FALSE);
  adw_switch_row_set_active (self->curve_cyclic, FALSE);
  gtk_color_dialog_button_set_rgba (self->curve_color, &color_black);
  adw_spin_row_set_value (self->curve_thickness, 1.0f);
}

static void
bimforge_curves_page_set_active_curve (BimforgeCurvesPage *self,
                                     BimforgeCurve      *curve)
{
  g_return_if_fail (BIMFORGE_IS_CURVES_PAGE (self));
  g_return_if_fail (curve == NULL || BIMFORGE_IS_CURVE (curve));

  BIMFORGE_ENTRY;

  if (curve && self->active_curve == curve)
    BIMFORGE_EXIT;

  if (self->active_curve != NULL)
    {
      g_binding_unbind (self->curve_name_bind);
      g_binding_unbind (self->curve_visible_bind);
      g_binding_unbind (self->curve_sensitive_bind);
      g_binding_unbind (self->curve_cyclic_bind);
      g_binding_unbind (self->curve_color_bind);
      g_binding_unbind (self->curve_thickness_bind);
      bimforge_gizmo_manipulator_set_gizmo (self->curve_transforms, NULL);

      bimforge_curves_page_reset_state (self);
      gtk_widget_set_visible (GTK_WIDGET (self), FALSE);

      g_clear_pointer (&self->active_curve, g_object_unref);
    }

  if (curve)
    {
      self->active_curve = g_object_ref (curve);
      self->curve_name_bind = g_object_bind_property (self->active_curve, "name",
                                                      self->curve_name, "text",
                                                      G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
      self->curve_visible_bind = g_object_bind_property (self->active_curve, "visible",
                                                         self->curve_visible, "active",
                                                         G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
      self->curve_sensitive_bind = g_object_bind_property (self->active_curve, "sensitive",
                                                           self->curve_sensitive, "active",
                                                           G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
      self->curve_cyclic_bind = g_object_bind_property (self->active_curve, "cyclic",
                                                        self->curve_cyclic, "active",
                                                        G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
      self->curve_color_bind = g_object_bind_property (self->active_curve, "color",
                                                       self->curve_color, "rgba",
                                                       G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
      self->curve_thickness_bind = g_object_bind_property (self->active_curve, "thickness",
                                                           self->curve_thickness, "value",
                                                           G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
      bimforge_gizmo_manipulator_set_gizmo (self->curve_transforms,
                                           _bimforge_gadget_get_gizmo (BIMFORGE_GADGET (self->active_curve)));

      gtk_widget_set_visible (GTK_WIDGET (self), TRUE);
    }

  BIMFORGE_EXIT;
}


static void
on_context_active_gadget_notify_cb (BimforgeContext *context,
                                    GParamSpec    *pspec,
                                    gpointer       user_data)
{
  BimforgeCurvesPage *self = (BimforgeCurvesPage *)user_data;
  BimforgeGadget * active_gadget;

  g_assert (BIMFORGE_IS_CURVES_PAGE (self));
  g_assert (BIMFORGE_IS_CONTEXT (context));

  active_gadget = bimforge_context_get_active_gadget (context);

  if (BIMFORGE_IS_CURVE (active_gadget))
    bimforge_curves_page_set_active_curve (self, BIMFORGE_CURVE (active_gadget));
  else
    bimforge_curves_page_set_active_curve (self, NULL);
}

static void
on_context_set_cb (BimforgePage *page)
{
  BimforgeCurvesPage *self = (BimforgeCurvesPage *)page;
  BimforgeContext *context = bimforge_page_get_context (page);

  g_assert (BIMFORGE_IS_CURVES_PAGE (self));
  g_assert (BIMFORGE_IS_CONTEXT (context));

  g_signal_connect (context, "notify::active-gadget", G_CALLBACK (on_context_active_gadget_notify_cb), self);
}


static void
on_append_point_clicked_cb (BimforgeCurvesPage *self,
                            GtkButton        *btn)
{
  BimforgeVec3 point;

  g_assert (BIMFORGE_IS_CURVES_PAGE (self));
  g_assert (GTK_IS_BUTTON (btn));

  BIMFORGE_ENTRY;

  if (!self->active_curve)
    BIMFORGE_EXIT;

  point.x = strtof (gtk_editable_get_text (GTK_EDITABLE (self->new_point_x)), NULL);
  point.y = strtof (gtk_editable_get_text (GTK_EDITABLE (self->new_point_y)), NULL);
  point.z = strtof (gtk_editable_get_text (GTK_EDITABLE (self->new_point_z)), NULL);

  bimforge_curve_append_point (self->active_curve, &point);

  BIMFORGE_EXIT;
}

static void
on_prepend_point_clicked_cb (BimforgeCurvesPage *self,
                             GtkButton        *btn)
{
  BimforgeVec3 point;

  g_assert (BIMFORGE_IS_CURVES_PAGE (self));
  g_assert (GTK_IS_BUTTON (btn));

  BIMFORGE_ENTRY;

  if (!self->active_curve)
    BIMFORGE_EXIT;

  point.x = strtof (gtk_editable_get_text (GTK_EDITABLE (self->new_point_x)), NULL);
  point.y = strtof (gtk_editable_get_text (GTK_EDITABLE (self->new_point_y)), NULL);
  point.z = strtof (gtk_editable_get_text (GTK_EDITABLE (self->new_point_z)), NULL);

  bimforge_curve_append_point (self->active_curve, &point);

  BIMFORGE_EXIT;
}


static void
bimforge_curves_page_dispose (GObject *object)
{
  BimforgeCurvesPage *self = (BimforgeCurvesPage *)object;
  GtkWidget *child = NULL;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (self))) != NULL)
    gtk_widget_unparent (child);

  G_OBJECT_CLASS (bimforge_curves_page_parent_class)->dispose (object);
}

static void
bimforge_curves_page_finalize (GObject *object)
{
  BimforgeCurvesPage *self = (BimforgeCurvesPage *)object;

  g_clear_pointer (&self->active_curve, g_object_unref);

  G_OBJECT_CLASS (bimforge_curves_page_parent_class)->finalize (object);
}

static void
bimforge_curves_page_class_init (BimforgeCurvesPageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = bimforge_curves_page_dispose;
  object_class->finalize = bimforge_curves_page_finalize;

  gtk_widget_class_set_template_from_resource (widget_class, "/io/sam/libbimforge/pages/bimforge-curves-page.ui");
  gtk_widget_class_bind_template_child (widget_class, BimforgeCurvesPage, curve_name);
  gtk_widget_class_bind_template_child (widget_class, BimforgeCurvesPage, curve_visible);
  gtk_widget_class_bind_template_child (widget_class, BimforgeCurvesPage, curve_sensitive);
  gtk_widget_class_bind_template_child (widget_class, BimforgeCurvesPage, curve_cyclic);
  gtk_widget_class_bind_template_child (widget_class, BimforgeCurvesPage, curve_color);
  gtk_widget_class_bind_template_child (widget_class, BimforgeCurvesPage, curve_thickness);
  gtk_widget_class_bind_template_child (widget_class, BimforgeCurvesPage, curve_transforms);
  gtk_widget_class_bind_template_child (widget_class, BimforgeCurvesPage, points_list);
  gtk_widget_class_bind_template_child (widget_class, BimforgeCurvesPage, new_point_x);
  gtk_widget_class_bind_template_child (widget_class, BimforgeCurvesPage, new_point_y);
  gtk_widget_class_bind_template_child (widget_class, BimforgeCurvesPage, new_point_z);

  gtk_widget_class_bind_template_callback (widget_class, on_context_set_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_append_point_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_prepend_point_clicked_cb);
}

static void
bimforge_curves_page_init (BimforgeCurvesPage *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}


/**
 * bimforge_curves_page_new:
 *
 * Create a new #BimforgeCurvesPage.
 *
 * Returns: a newly created #BimforgePage.
 */
BimforgePage *
bimforge_curves_page_new (void)
{
  return g_object_new (BIMFORGE_TYPE_CURVES_PAGE, NULL);
}


