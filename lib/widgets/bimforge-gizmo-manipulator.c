/* bimforge-gizmo-manipulator.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bimforge-gizmo-manipulator"

#include "config.h"
#include "bimforge-debug.h"

#include "bimforge-wvec3.h"
#include "bimforge-gizmo-manipulator.h"

struct _BimforgeGizmoManipulator
{
  AdwPreferencesGroup parent_instance;

  ForgerGizmo * gizmo;

  /* -----< TEMPLATE WIDGETS >----- */
  BimforgeWVec3 * translation;
  BimforgeWVec3 * rotation;
  BimforgeWVec3 * scale;
};

G_DEFINE_FINAL_TYPE (BimforgeGizmoManipulator, bimforge_gizmo_manipulator, ADW_TYPE_PREFERENCES_GROUP)

enum {
  PROP_0,
  PROP_GIZMO,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
bimforge_gizmo_manipulator_dispose (GObject *object)
{
  BimforgeGizmoManipulator *self = (BimforgeGizmoManipulator *)object;

  gtk_widget_dispose_template (GTK_WIDGET (self), BIMFORGE_TYPE_GIZMO_MANIPULATOR);

  G_OBJECT_CLASS (bimforge_gizmo_manipulator_parent_class)->dispose (object);
}

static void
bimforge_gizmo_manipulator_get_property (GObject    *object,
                                         guint       prop_id,
                                         GValue     *value,
                                         GParamSpec *pspec)
{
  BimforgeGizmoManipulator *self = BIMFORGE_GIZMO_MANIPULATOR (object);

  switch (prop_id)
    {
    case PROP_GIZMO:
      g_value_set_object (value, bimforge_gizmo_manipulator_get_gizmo (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bimforge_gizmo_manipulator_set_property (GObject      *object,
                                         guint         prop_id,
                                         const GValue *value,
                                         GParamSpec   *pspec)
{
  BimforgeGizmoManipulator *self = BIMFORGE_GIZMO_MANIPULATOR (object);

  switch (prop_id)
    {
    case PROP_GIZMO:
      bimforge_gizmo_manipulator_set_gizmo (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bimforge_gizmo_manipulator_class_init (BimforgeGizmoManipulatorClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = bimforge_gizmo_manipulator_dispose;
  object_class->get_property = bimforge_gizmo_manipulator_get_property;
  object_class->set_property = bimforge_gizmo_manipulator_set_property;

  g_type_ensure (BIMFORGE_TYPE_WVEC3);

  /**
   * BimforgeGizmoManipulator:gizmo:
   *
   * The "gizmo" property is the #ForgerGizmo to linked with.
   *
   * Since: 0.1
   */
  properties [PROP_GIZMO] =
    g_param_spec_object ("gizmo", NULL, NULL,
                         FORGER_TYPE_GIZMO,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/io/sam/libbimforge/ui/bimforge-gizmo-manipulator.ui");
  gtk_widget_class_bind_template_child (widget_class, BimforgeGizmoManipulator, translation);
  gtk_widget_class_bind_template_child (widget_class, BimforgeGizmoManipulator, rotation);
  gtk_widget_class_bind_template_child (widget_class, BimforgeGizmoManipulator, scale);
}

static void
bimforge_gizmo_manipulator_init (BimforgeGizmoManipulator *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}


/**
 * bimforge_gizmo_manipulator_new:
 *
 * Creates a new #BimforgeGizmoManipulator.
 *
 * Returns: a newly created #GtkWidget.
 *
 * Since: 0.1
 */
GtkWidget *
bimforge_gizmo_manipulator_new (void)
{
  return g_object_new (BIMFORGE_TYPE_GIZMO_MANIPULATOR, NULL);
}

/**
 * bimforge_gizmo_manipulator_get_gizmo:
 * @self: a #BimforgeGizmoManipulator.
 *
 * Gets the "gizmo" property of @self.
 *
 * Returns: (transfer full) (nullable): a #ForgerGizmo
 *
 * Since: 0.1
 */
ForgerGizmo *
bimforge_gizmo_manipulator_get_gizmo (BimforgeGizmoManipulator *self)
{
  g_return_val_if_fail (BIMFORGE_IS_GIZMO_MANIPULATOR (self), NULL);

  return self->gizmo;
}

/**
 * bimforge_gizmo_manipulator_set_gizmo:
 * @self: a #BimforgeGizmoManipulator.
 * @gizmo: a #ForgerGizmo.
 *
 * Sets "gizmo" property of @self.
 *
 * Since: 0.1
 */
void
bimforge_gizmo_manipulator_set_gizmo (BimforgeGizmoManipulator *self,
                                      ForgerGizmo              *gizmo)
{
  BIMFORGE_ENTRY;
  g_return_if_fail (BIMFORGE_IS_GIZMO_MANIPULATOR (self));
  g_return_if_fail (FORGER_IS_GIZMO (gizmo));

  if (self->gizmo == gizmo)
    BIMFORGE_EXIT;

  if (g_set_object (&self->gizmo, gizmo))
    {
      bimforge_wvec3_bind_vector (self->translation, forger_gizmo_get_location (self->gizmo));
      bimforge_wvec3_bind_vector (self->rotation, forger_gizmo_get_rotation (self->gizmo));
      bimforge_wvec3_bind_vector (self->scale, forger_gizmo_get_scale (self->gizmo));

      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_GIZMO]);
    }
}
