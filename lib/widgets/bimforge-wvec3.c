/*
 * bimforge-wvec3.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bimforge-wvec3"

#include "config.h"
#include "bimforge-debug.h"

#include "bimforge-wvec3.h"

struct _BimforgeWVec3
{
  AdwExpanderRow parent_instance;

  graphene_point3d_t * vector;

  /* -----< TEMPLATE WIDGETS >----- */
  AdwSpinRow *          u_spin_row;
  AdwSpinRow *          v_spin_row;
  AdwSpinRow *          w_spin_row;
};

G_DEFINE_FINAL_TYPE (BimforgeWVec3, bimforge_wvec3, ADW_TYPE_EXPANDER_ROW)

enum {
  PROP_0,
  PROP_VECTOR,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
on_u_value_changed_cb (BimforgeWVec3 *self,
                       GtkAdjustment *adj)
{
  g_assert (BIMFORGE_IS_WVEC3 (self));
  g_assert (GTK_IS_ADJUSTMENT (adj));

  if (!self->vector)
    return;

  self->vector->x = gtk_adjustment_get_value (adj);
}

static void
on_v_value_changed_cb (BimforgeWVec3 *self,
                       GtkAdjustment *adj)
{
  g_assert (BIMFORGE_IS_WVEC3 (self));
  g_assert (GTK_IS_ADJUSTMENT (adj));

  if (!self->vector)
    return;

  self->vector->y = gtk_adjustment_get_value (adj);
}

static void
on_w_value_changed_cb (BimforgeWVec3 *self,
                       GtkAdjustment *adj)
{
  g_assert (BIMFORGE_IS_WVEC3 (self));
  g_assert (GTK_IS_ADJUSTMENT (adj));

  if (!self->vector)
    return;

  self->vector->z = gtk_adjustment_get_value (adj);
}

static void
bimforge_wvec3_dispose (GObject *object)
{
  BimforgeWVec3 *self = (BimforgeWVec3 *)object;

  gtk_widget_dispose_template (GTK_WIDGET (self), BIMFORGE_TYPE_WVEC3);

  G_OBJECT_CLASS (bimforge_wvec3_parent_class)->dispose (object);
}

static gboolean
bimforge_wvec3_grab_focus (GtkWidget *widget)
{
  BimforgeWVec3 *self = (BimforgeWVec3 *)widget;

  return gtk_widget_grab_focus (GTK_WIDGET (self->u_spin_row));
}

static void
bimforge_wvec3_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  BimforgeWVec3 *self = BIMFORGE_WVEC3 (object);

  switch (prop_id)
    {
    case PROP_VECTOR:
      g_value_set_boxed (value, bimforge_wvec3_get_vector (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bimforge_wvec3_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  BimforgeWVec3 *self = BIMFORGE_WVEC3 (object);

  switch (prop_id)
    {
    case PROP_VECTOR:
      bimforge_wvec3_bind_vector (self, g_value_get_boxed (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bimforge_wvec3_class_init (BimforgeWVec3Class *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = bimforge_wvec3_dispose;
  object_class->get_property = bimforge_wvec3_get_property;
  object_class->set_property = bimforge_wvec3_set_property;

  widget_class->grab_focus = bimforge_wvec3_grab_focus;

  properties [PROP_VECTOR] =
    g_param_spec_boxed ("vector", NULL, NULL,
                        GRAPHENE_TYPE_POINT3D,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY |
                         G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/io/sam/libbimforge/ui/bimforge-wvec3.ui");
  gtk_widget_class_bind_template_child (widget_class, BimforgeWVec3, u_spin_row);
  gtk_widget_class_bind_template_child (widget_class, BimforgeWVec3, v_spin_row);
  gtk_widget_class_bind_template_child (widget_class, BimforgeWVec3, w_spin_row);

  gtk_widget_class_bind_template_callback (widget_class, on_u_value_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_v_value_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_w_value_changed_cb);
}

static void
bimforge_wvec3_init (BimforgeWVec3 *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  adw_spin_row_set_value (self->u_spin_row, 0.0f);
  adw_spin_row_set_value (self->v_spin_row, 0.0f);
  adw_spin_row_set_value (self->w_spin_row, 0.0f);
}


GtkWidget *
bimforge_wvec3_new (void)
{
  return g_object_new (BIMFORGE_TYPE_WVEC3, NULL);
}

graphene_point3d_t *
bimforge_wvec3_get_vector (BimforgeWVec3 * self)
{
  g_return_val_if_fail (BIMFORGE_IS_WVEC3 (self), NULL);

  return self->vector;
}

void
bimforge_wvec3_bind_vector (BimforgeWVec3      *self,
                            graphene_point3d_t *setting)
{
  g_return_if_fail (BIMFORGE_IS_WVEC3 (self));

  if (self->vector == setting)
    return;

  self->vector = setting;

  if (self->vector)
    {
      adw_spin_row_set_value (self->u_spin_row, self->vector->x);
      adw_spin_row_set_value (self->v_spin_row, self->vector->y);
      adw_spin_row_set_value (self->w_spin_row, self->vector->z);
    }

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_VECTOR]);
}


