/*
 * bimforge-wvec3.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(BIMFORGE_INSIDE) && !defined(BIMFORGE_COMPILATION)
#error "Only <libbimforge.h> can be included directly."
#endif

#include <adwaita.h>
#include <graphene-gobject.h>

#include "bimforge-version-macros.h"

G_BEGIN_DECLS

#define BIMFORGE_TYPE_WVEC3 (bimforge_wvec3_get_type())

BIMFORGE_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (BimforgeWVec3, bimforge_wvec3, BIMFORGE, WVEC3, AdwExpanderRow)

BIMFORGE_AVAILABLE_IN_ALL
GtkWidget *          bimforge_wvec3_new         (void) G_GNUC_WARN_UNUSED_RESULT;

BIMFORGE_AVAILABLE_IN_ALL
graphene_point3d_t * bimforge_wvec3_get_vector  (BimforgeWVec3 *        self);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_wvec3_bind_vector (BimforgeWVec3 *        self,
                                                 graphene_point3d_t *   setting);

G_END_DECLS
