/* bimforge-init.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bimforge-init"

#include "config.h"

#include <gtk/gtk.h>

#include "bimforge-init.h"

#include "application/bimforge-page.h"
#include "widgets/bimforge-gizmo-manipulator.h"
#include "widgets/bimforge-wvec3.h"

void
bimforge_init (void)
{
  /* We need to make sure that all the GType in library are registered
   * here in the init process */
  g_type_ensure (BIMFORGE_TYPE_GIZMO_MANIPULATOR);
  g_type_ensure (BIMFORGE_TYPE_PAGE);
  g_type_ensure (BIMFORGE_TYPE_WVEC3);
}

void
bimforge_init_styling (void)
{
  g_autoptr (GtkCssProvider) css = NULL;
  GdkDisplay *display = gdk_display_get_default ();

  if (display)
    {
      css = gtk_css_provider_new ();
      gtk_css_provider_load_from_resource (css, "/io/sam/libbimforge/shared.css");
      gtk_style_context_add_provider_for_display (display,
                                                  GTK_STYLE_PROVIDER (css),
                                                  GTK_STYLE_PROVIDER_PRIORITY_APPLICATION-1);
    }
}
