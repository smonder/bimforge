/* bimforge-tree.c
 *
 * Copyright 2018-2023 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bimforge-tree"

#include "config.h"
#include "bimforge-debug.h"

#include <libpeas.h>
#include <libforger.h>

#include "bimforge-tree.h"
#include "bimforge-tree-addin.h"
#include "bimforge-tree-private.h"
#include "bimforge-tree-empty.h"
#include "bimforge-tree-expander.h"
#include "application/bimforge-app-global.h"

typedef struct
{
  PeasExtensionSet *      addins;
  GtkTreeListModel *      tree_model;
  BimforgeTreeNode *        root;
  char *                  kind;
  GMenuModel *            menu_model;

  GtkScrolledWindow *     scroller;
  GtkListView *           list_view;
  GtkSingleSelection *    selection;

  GdkDragAction           drop_action;
} BimforgeTreePrivate;

enum {
  PROP_0,
  PROP_KIND,
  PROP_MENU_MODEL,
  PROP_ROOT,
  PROP_SELECTED_NODE,
  N_PROPS
};

G_DEFINE_TYPE_WITH_PRIVATE (BimforgeTree, bimforge_tree, GTK_TYPE_WIDGET)

static GParamSpec *properties [N_PROPS];

typedef struct
{
  BimforgeTree     *tree;
  BimforgeTreeNode *node;
  GPtrArray   *providers;
} DragPrepare;

typedef struct
{
  BimforgeTree       *tree;
  GtkDropTarget *drop_target;
  BimforgeTreeNode   *node;
  GArray        *gtypes;
  GdkDragAction  action;
} DropAccept;

typedef struct
{
  BimforgeTree       *tree;
  GtkDropTarget *drop_target;
  BimforgeTreeNode   *node;
  GPtrArray     *active;
} Drop;

typedef struct
{
  BimforgeTree     *tree;
  BimforgeTreeNode *node;
  guint        handled : 1;
} NodeActivated;

static void
bimforge_tree_node_activated_cb (PeasExtensionSet *addins,
                               PeasPluginInfo   *plugin_info,
                               GObject          *extension,
                               gpointer          user_data)
{
  BimforgeTreeAddin *addin = (BimforgeTreeAddin *)extension;
  NodeActivated *state = user_data;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (PEAS_IS_EXTENSION_SET (addins));
  g_assert (plugin_info != NULL);
  g_assert (BIMFORGE_IS_TREE_ADDIN (addin));
  g_assert (state != NULL);

  if (!state->handled)
    state->handled = bimforge_tree_addin_node_activated (addin, state->tree, state->node);
}

static void
bimforge_tree_activate_cb (BimforgeTree  *self,
                         guint        position,
                         GtkListView *list_view)
{
  BimforgeTreePrivate *priv = bimforge_tree_get_instance_private (self);
  g_autoptr(GtkTreeListRow) row = NULL;
  g_autoptr(BimforgeTreeNode) node = NULL;
  NodeActivated state;

  BIMFORGE_ENTRY;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (BIMFORGE_IS_TREE (self));
  g_assert (GTK_IS_LIST_VIEW (list_view));

  if (!(row = g_list_model_get_item (G_LIST_MODEL (priv->selection), position)) ||
      !(node = gtk_tree_list_row_get_item (row)))
    BIMFORGE_EXIT;

  state.tree = self;
  state.node = node;
  state.handled = FALSE;

  peas_extension_set_foreach (priv->addins,
                              bimforge_tree_node_activated_cb,
                              &state);

  BIMFORGE_EXIT;
}

static void
bimforge_tree_notify_selected_cb (BimforgeTree         *self,
                                GParamSpec         *pspec,
                                GtkSingleSelection *selection)
{
  g_assert (BIMFORGE_IS_TREE (self));
  g_assert (GTK_IS_SINGLE_SELECTION (selection));

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_SELECTED_NODE]);
}

static void
bimforge_tree_extension_added_cb (PeasExtensionSet *addins,
                                PeasPluginInfo   *plugin_info,
                                GObject          *extension,
                                gpointer          user_data)
{
  BimforgeTreeAddin *addin = (BimforgeTreeAddin *)extension;
  BimforgeTree *self = user_data;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (PEAS_IS_EXTENSION_SET (addins));
  g_assert (plugin_info != NULL);
  g_assert (BIMFORGE_IS_TREE_ADDIN (addin));

  bimforge_tree_addin_load (addin, self);
}

static void
bimforge_tree_extension_removed_cb (PeasExtensionSet *addins,
                                  PeasPluginInfo   *plugin_info,
                                  GObject          *extension,
                                  gpointer          user_data)
{
  BimforgeTreeAddin *addin = (BimforgeTreeAddin *)extension;
  BimforgeTree *self = user_data;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (PEAS_IS_EXTENSION_SET (addins));
  g_assert (plugin_info != NULL);
  g_assert (BIMFORGE_IS_TREE_ADDIN (addin));

  bimforge_tree_addin_unload (addin, self);
}

static void
bimforge_tree_root (GtkWidget *widget)
{
  BimforgeTree *self = (BimforgeTree *)widget;
  BimforgeTreePrivate *priv = bimforge_tree_get_instance_private (self);

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (BIMFORGE_IS_TREE (self));

  GTK_WIDGET_CLASS (bimforge_tree_parent_class)->root (widget);

  if (priv->addins != NULL)
    return;

  priv->addins = peas_extension_set_new (peas_engine_get_default (),
                                         BIMFORGE_TYPE_TREE_ADDIN, NULL);
                                         /* "Tree-Kind", priv->kind); */
  g_signal_connect (priv->addins,
                    "extension-added",
                    G_CALLBACK (bimforge_tree_extension_added_cb),
                    self);
  g_signal_connect (priv->addins,
                    "extension-removed",
                    G_CALLBACK (bimforge_tree_extension_removed_cb),
                    self);
  peas_extension_set_foreach (priv->addins,
                              bimforge_tree_extension_added_cb,
                              self);

  if (priv->root != NULL)
    _bimforge_tree_node_expand_async (priv->root, priv->addins, NULL, NULL, NULL);
}

static void
bimforge_tree_click_pressed_cb (GtkGestureClick *click,
                              int              n_press,
                              double           x,
                              double           y,
                              gpointer         user_data)
{
  g_autoptr(BimforgeTreeNode) node = NULL;
  GdkEventSequence *sequence;
  BimforgeTreeExpander *expander;
  GtkTreeListRow *row;
  BimforgeTreePrivate *priv;
  GdkEvent *event;
  BimforgeTree *tree;

  g_assert (GTK_IS_GESTURE_CLICK (click));

  if (n_press != 1)
    return;

  sequence = gtk_gesture_single_get_current_sequence (GTK_GESTURE_SINGLE (click));
  event = gtk_gesture_get_last_event (GTK_GESTURE (click), sequence);
  expander = BIMFORGE_TREE_EXPANDER (gtk_event_controller_get_widget (GTK_EVENT_CONTROLLER (click)));
  tree = BIMFORGE_TREE (gtk_widget_get_ancestor (GTK_WIDGET (expander), BIMFORGE_TYPE_TREE));
  priv = bimforge_tree_get_instance_private (tree);
  row = bimforge_tree_expander_get_list_row (expander);
  node = BIMFORGE_TREE_NODE (gtk_tree_list_row_get_item (row));

  gtk_widget_activate_action (GTK_WIDGET (expander), "listitem.select", "(bb)", FALSE, FALSE);

  if (gdk_event_triggers_context_menu (event))
    {
      GtkPopover *popover;

      if (priv->menu_model == NULL)
        return;

      popover = g_object_new (GTK_TYPE_POPOVER_MENU,
                              "menu-model", priv->menu_model,
                              "has-arrow", TRUE,
                              "position", GTK_POS_RIGHT,
                              NULL);

      bimforge_tree_set_selected_node (tree, node);
      bimforge_tree_expander_show_popover (expander, popover);

      gtk_gesture_set_sequence_state (GTK_GESTURE (click),
                                      sequence,
                                      GTK_EVENT_SEQUENCE_CLAIMED);
    }
}

static void
bimforge_tree_click_released_cb (GtkGestureClick *click,
                               int              n_press,
                               double           x,
                               double           y,
                               gpointer         user_data)
{
  g_autoptr(BimforgeTreeNode) node = NULL;
  GdkEventSequence *sequence;
  BimforgeTreeExpander *expander;
  GtkTreeListRow *row;
  BimforgeTreePrivate *priv;
  BimforgeTree *tree;

  g_assert (GTK_IS_GESTURE_CLICK (click));

  if (n_press != 1)
    return;

  sequence = gtk_gesture_single_get_current_sequence (GTK_GESTURE_SINGLE (click));
  expander = BIMFORGE_TREE_EXPANDER (gtk_event_controller_get_widget (GTK_EVENT_CONTROLLER (click)));
  tree = BIMFORGE_TREE (gtk_widget_get_ancestor (GTK_WIDGET (expander), BIMFORGE_TYPE_TREE));
  priv = bimforge_tree_get_instance_private (tree);
  row = bimforge_tree_expander_get_list_row (expander);
  node = BIMFORGE_TREE_NODE (gtk_tree_list_row_get_item (row));

  if (gtk_gesture_get_sequence_state (GTK_GESTURE (click), sequence) == GTK_EVENT_SEQUENCE_NONE)
    {
      NodeActivated state = {0};

      state.tree = tree;
      state.node = node;
      state.handled = FALSE;

      peas_extension_set_foreach (priv->addins,
                                  bimforge_tree_node_activated_cb,
                                  &state);

      if (state.handled)
        gtk_gesture_set_sequence_state (GTK_GESTURE (click),
                                        sequence,
                                        GTK_EVENT_SEQUENCE_CLAIMED);
    }
}

static void
bimforge_tree_drag_source_prepare_addin_cb (PeasExtensionSet *adapter,
                                          PeasPluginInfo   *plugin_info,
                                          GObject          *exten,
                                          gpointer          user_data)
{
  BimforgeTreeAddin *addin = (BimforgeTreeAddin *)exten;
  DragPrepare *state = user_data;
  GdkContentProvider *provider;

  g_assert (PEAS_IS_EXTENSION_SET (adapter));
  g_assert (plugin_info != NULL);
  g_assert (BIMFORGE_IS_TREE_ADDIN (addin));

  if ((provider = bimforge_tree_addin_node_draggable (addin, state->node)))
    g_ptr_array_add (state->providers, g_steal_pointer (&provider));
}

static GdkContentProvider *
bimforge_tree_drag_source_prepare_cb (BimforgeTree    *self,
                                    double         x,
                                    double         y,
                                    GtkDragSource *source)
{
  BimforgeTreePrivate *priv = bimforge_tree_get_instance_private (self);
  g_autoptr(BimforgeTreeNode) node = NULL;
  g_autoptr(GPtrArray) providers = NULL;
  GdkContentProvider *provider = NULL;
  BimforgeTreeExpander *expander;
  GtkTreeListRow *row;
  DragPrepare state = {0};

  BIMFORGE_ENTRY;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (BIMFORGE_IS_TREE (self));
  g_assert (GTK_IS_DRAG_SOURCE (source));

  expander = BIMFORGE_TREE_EXPANDER (gtk_event_controller_get_widget (GTK_EVENT_CONTROLLER (source)));
  row = bimforge_tree_expander_get_list_row (expander);
  node = BIMFORGE_TREE_NODE (gtk_tree_list_row_get_item (row));
  providers = g_ptr_array_new ();

  state.tree = self;
  state.node = node;
  state.providers = providers;

  peas_extension_set_foreach (priv->addins,
                              bimforge_tree_drag_source_prepare_addin_cb,
                              &state);

  if (providers->len == 0)
    BIMFORGE_RETURN (NULL);
  else if (providers->len == 1)
    provider = g_ptr_array_steal_index (providers, 0);
  else
    provider = gdk_content_provider_new_union ((GdkContentProvider **)providers->pdata, providers->len);

  gtk_gesture_set_state (GTK_GESTURE (source), GTK_EVENT_SEQUENCE_CLAIMED);
  gtk_drag_source_set_actions (source, GDK_ACTION_ALL);

  BIMFORGE_RETURN (provider);
}

static void
bimforge_tree_drag_source_drag_begin_cb (BimforgeTree    *self,
                                       GdkDrag       *drag,
                                       GtkDragSource *source)
{
  g_autoptr(GdkPaintable) paintable = NULL;
  GtkWidget *widget;

  BIMFORGE_ENTRY;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (BIMFORGE_IS_TREE (self));
  g_assert (GDK_IS_DRAG (drag));
  g_assert (GTK_IS_DRAG_SOURCE (source));

  /* Get our BimforgeTreeExpander */
  widget = gtk_event_controller_get_widget (GTK_EVENT_CONTROLLER (source));

  /* But snapshot the parent to get row content */
  widget = gtk_widget_get_parent (widget);

  if ((paintable = gtk_widget_paintable_new (widget)))
    {
      GtkSnapshot *snapshot = gtk_snapshot_new ();
      double width = gdk_paintable_get_intrinsic_width (paintable);
      double height = gdk_paintable_get_intrinsic_height (paintable);
      g_autoptr(GdkPaintable) with_parent = NULL;

      gdk_paintable_snapshot (paintable, snapshot, width, height);

      with_parent = gtk_snapshot_free_to_paintable (snapshot, &GRAPHENE_SIZE_INIT (width, height));
      gtk_drag_source_set_icon (source, paintable, 0, 0);
    }

  BIMFORGE_EXIT;
}

static void
bimforge_tree_drag_source_drag_end_cb (BimforgeTree    *self,
                                     GdkDrag       *drag,
                                     gboolean       delete_data,
                                     GtkDragSource *source)
{
  BIMFORGE_ENTRY;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (BIMFORGE_IS_TREE (self));
  g_assert (GTK_IS_DRAG_SOURCE (source));
  g_assert (GDK_IS_DRAG (drag));

  gtk_drag_source_set_content (source, NULL);
  gtk_drag_source_set_icon (source, NULL, 0, 0);

  BIMFORGE_EXIT;
}

static void
bimforge_tree_drop_target_accept_foreach_cb (PeasExtensionSet *adapter,
                                           PeasPluginInfo   *plugin_info,
                                           GObject          *exten,
                                           gpointer          user_data)
{
  BimforgeTreeAddin *addin = (BimforgeTreeAddin *)exten;
  DropAccept *state = user_data;

  g_assert (PEAS_IS_EXTENSION_SET (adapter));
  g_assert (plugin_info != NULL);
  g_assert (BIMFORGE_IS_TREE_ADDIN (addin));

  state->action |= bimforge_tree_addin_node_droppable (addin, state->drop_target, state->node, state->gtypes);
}

static gboolean
bimforge_tree_drop_target_accept_cb (BimforgeTree    *self,
                                   GdkDrop       *drop,
                                   GtkDropTarget *drop_target)
{
  BimforgeTreePrivate *priv = bimforge_tree_get_instance_private (self);
  g_autoptr(BimforgeTreeNode) node = NULL;
  g_autoptr(GArray) gtypes = NULL;
  BimforgeTreeExpander *expander;
  GtkTreeListRow *row;
  DropAccept state = {0};

  BIMFORGE_ENTRY;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (BIMFORGE_IS_TREE (self));
  g_assert (GDK_IS_DROP (drop));
  g_assert (GTK_IS_DROP_TARGET (drop_target));

  expander = BIMFORGE_TREE_EXPANDER (gtk_event_controller_get_widget (GTK_EVENT_CONTROLLER (drop_target)));
  row = bimforge_tree_expander_get_list_row (expander);
  node = BIMFORGE_TREE_NODE (gtk_tree_list_row_get_item (row));
  gtypes = g_array_new (FALSE, FALSE, sizeof (GType));

  state.tree = self;
  state.drop_target = drop_target;
  state.node = node;
  state.action = 0;
  state.gtypes = gtypes;

  peas_extension_set_foreach (priv->addins,
                              bimforge_tree_drop_target_accept_foreach_cb,
                              &state);

  gtk_drop_target_set_actions (drop_target, state.action);
  gtk_drop_target_set_gtypes (drop_target, (GType *)(gpointer)gtypes->data, gtypes->len);

  BIMFORGE_RETURN (state.action != 0);
}

static GdkDragAction
get_preferred_action (BimforgeTree   *self,
                      const GValue *value)
{
  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (BIMFORGE_IS_TREE (self));
  g_assert (!value || G_IS_VALUE (value));

  if (value == NULL)
    return GDK_ACTION_COPY;

  if (G_VALUE_HOLDS_STRING (value))
    return GDK_ACTION_COPY;

  if (G_VALUE_HOLDS (value, FORGER_TYPE_ENTITY))
    return GDK_ACTION_MOVE;

  return GDK_ACTION_COPY;
}

static void
bimforge_tree_drop_target_notify_value_cb (BimforgeTree    *self,
                                         GParamSpec    *pspec,
                                         GtkDropTarget *drop_target)
{
  BimforgeTreePrivate *priv = bimforge_tree_get_instance_private (self);
  g_autoptr(BimforgeTreeNode) node = NULL;
  GtkTreeListRow *row;
  const GValue *value;
  GtkWidget *widget;

  BIMFORGE_ENTRY;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (BIMFORGE_IS_TREE (self));
  g_assert (GTK_IS_DROP_TARGET (drop_target));

  if (!(value = gtk_drop_target_get_value (drop_target)) || !G_IS_VALUE (value))
    BIMFORGE_EXIT;

  widget = gtk_event_controller_get_widget (GTK_EVENT_CONTROLLER (drop_target));
  row = bimforge_tree_expander_get_list_row (BIMFORGE_TREE_EXPANDER (widget));
  node = gtk_tree_list_row_get_item (row);

  g_assert (!node || BIMFORGE_IS_TREE_NODE (node));

  if (node == NULL)
    BIMFORGE_EXIT;

  priv->drop_action = get_preferred_action (self, value);

  BIMFORGE_EXIT;
}

static GdkDragAction
bimforge_tree_drop_target_enter_cb (BimforgeTree    *self,
                                  double         x,
                                  double         y,
                                  GtkDropTarget *drop_target)
{
  BimforgeTreePrivate *priv = bimforge_tree_get_instance_private (self);
  g_autoptr(BimforgeTreeNode) node = NULL;
  GtkTreeListRow *row;
  const GValue *value;
  GtkWidget *widget;

  BIMFORGE_ENTRY;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (BIMFORGE_IS_TREE (self));
  g_assert (GTK_IS_DROP_TARGET (drop_target));

  priv->drop_action = 0;

  widget = gtk_event_controller_get_widget (GTK_EVENT_CONTROLLER (drop_target));
  row = bimforge_tree_expander_get_list_row (BIMFORGE_TREE_EXPANDER (widget));
  node = gtk_tree_list_row_get_item (row);

  g_assert (!node || BIMFORGE_IS_TREE_NODE (node));

  if (node == NULL)
    BIMFORGE_GOTO (reject);

  value = gtk_drop_target_get_value (drop_target);

  priv->drop_action = get_preferred_action (self, value);

  BIMFORGE_RETURN (priv->drop_action);

reject:
  gtk_drop_target_reject (drop_target);

  BIMFORGE_RETURN (0);
}

static GdkDragAction
bimforge_tree_drop_target_motion_cb (BimforgeTree    *self,
                                   double         x,
                                   double         y,
                                   GtkDropTarget *drop_target)
{
  BimforgeTreePrivate *priv = bimforge_tree_get_instance_private (self);

  BIMFORGE_ENTRY;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (BIMFORGE_IS_TREE (self));
  g_assert (GTK_IS_DROP_TARGET (drop_target));

  BIMFORGE_RETURN (priv->drop_action);
}

static void
bimforge_tree_drop_target_leave_cb (BimforgeTree    *self,
                                  GtkDropTarget *drop_target)
{
  BimforgeTreePrivate *priv = bimforge_tree_get_instance_private (self);

  BIMFORGE_ENTRY;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (BIMFORGE_IS_TREE (self));
  g_assert (GTK_IS_DROP_TARGET (drop_target));

  priv->drop_action = 0;

  BIMFORGE_EXIT;
}

static void
drop_finalize (gpointer data)
{
  Drop *drop = data;

  g_clear_object (&drop->tree);
  g_clear_object (&drop->node);
  g_clear_object (&drop->drop_target);
  g_clear_pointer (&drop->active, g_ptr_array_unref);
}

static void
drop_unref (Drop *drop)
{
  g_atomic_rc_box_release_full (drop, drop_finalize);
}

static Drop *
drop_ref (Drop *drop)
{
  return g_atomic_rc_box_acquire (drop);
}

static void
bimforge_tree_drop_target_drop_addin_cb (GObject      *object,
                                       GAsyncResult *result,
                                       gpointer      user_data)
{
  BimforgeTreeAddin *addin = (BimforgeTreeAddin *)object;
  g_autoptr(GError) error = NULL;
  Drop *drop = user_data;

  BIMFORGE_ENTRY;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (BIMFORGE_IS_TREE_ADDIN (addin));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (drop != NULL);

  if (!bimforge_tree_addin_node_dropped_finish (addin, result, &error))
    {
      if (!bimforge_error_ignore (error))
        g_warning ("%s failed to handle drop onto node: %s",
                   G_OBJECT_TYPE_NAME (addin), error->message);
    }

  drop_unref (drop);

  BIMFORGE_EXIT;
}

static void
bimforge_tree_drop_target_drop_foreach_cb (PeasExtensionSet *set,
                                         PeasPluginInfo   *plugin_info,
                                         GObject          *exten,
                                         gpointer          user_data)
{
  BimforgeTreeAddin *addin = (BimforgeTreeAddin *)exten;
  g_autoptr(GArray) gtypes = NULL;
  Drop *drop = user_data;

  g_assert (PEAS_IS_EXTENSION_SET (set));
  g_assert (plugin_info != NULL);
  g_assert (BIMFORGE_IS_TREE_ADDIN (addin));

  gtypes = g_array_new (FALSE, FALSE, sizeof (GType));

  if (bimforge_tree_addin_node_droppable (addin, drop->drop_target, drop->node, gtypes))
    bimforge_tree_addin_node_dropped_async (addin,
                                          drop->drop_target,
                                          drop->node,
                                          NULL,
                                          bimforge_tree_drop_target_drop_addin_cb,
                                          drop_ref (drop));
}

static void
bimforge_tree_drop_target_drop_cb (BimforgeTree    *self,
                                 const GValue  *value,
                                 double         x,
                                 double         y,
                                 GtkDropTarget *drop_target)
{
  BimforgeTreePrivate *priv = bimforge_tree_get_instance_private (self);
  g_autoptr(BimforgeTreeNode) node = NULL;
  GtkTreeListRow *row;
  GtkWidget *widget;
  Drop *drop;

  BIMFORGE_ENTRY;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (BIMFORGE_IS_TREE (self));
  g_assert (GTK_IS_DROP_TARGET (drop_target));

  widget = gtk_event_controller_get_widget (GTK_EVENT_CONTROLLER (drop_target));
  row = bimforge_tree_expander_get_list_row (BIMFORGE_TREE_EXPANDER (widget));
  node = gtk_tree_list_row_get_item (row);

  drop = g_atomic_rc_box_new0 (Drop);
  drop->tree = g_object_ref (self);
  drop->drop_target = g_object_ref (drop_target);
  drop->node = g_object_ref (node);
  drop->active = g_ptr_array_new_with_free_func (g_object_unref);

  peas_extension_set_foreach (priv->addins,
                              bimforge_tree_drop_target_drop_foreach_cb,
                              drop);

  drop_unref (drop);

  BIMFORGE_EXIT;
}

static void
bimforge_tree_list_item_setup_cb (BimforgeTree               *self,
                                GtkListItem              *item,
                                GtkSignalListItemFactory *factory)
{
  BimforgeTreeExpander *expander;
  GtkDragSource *drag;
  GtkDropTarget *drop;
  GtkGesture *gesture;
  GtkImage *image;

  g_assert (BIMFORGE_IS_TREE (self));
  g_assert (GTK_IS_LIST_ITEM (item));
  g_assert (GTK_IS_SIGNAL_LIST_ITEM_FACTORY (factory));

  image = g_object_new (GTK_TYPE_IMAGE, NULL);
  expander = g_object_new (BIMFORGE_TYPE_TREE_EXPANDER,
                           "suffix", image,
                           "has-tooltip", TRUE,
                           NULL);
  gtk_list_item_set_child (item, GTK_WIDGET (expander));

  /* Handle click events before BimforgeTreeExpander handles expansion of
   * rows which have children. We need to deal with right-click context
   * menus and BimforgeTreeAddin handling activation before.
   */
  gesture = gtk_gesture_click_new ();
  gtk_event_controller_set_name (GTK_EVENT_CONTROLLER (gesture), "bimforge-tree-click");
  gtk_gesture_single_set_button (GTK_GESTURE_SINGLE (gesture), 0);
  gtk_event_controller_set_propagation_phase (GTK_EVENT_CONTROLLER (gesture),
                                              GTK_PHASE_CAPTURE);
  g_signal_connect (gesture,
                    "pressed",
                    G_CALLBACK (bimforge_tree_click_pressed_cb),
                    NULL);
  g_signal_connect (gesture,
                    "released",
                    G_CALLBACK (bimforge_tree_click_released_cb),
                    NULL);
  gtk_widget_add_controller (GTK_WIDGET (expander), GTK_EVENT_CONTROLLER (gesture));

  /* Setup Drag gesture for this row */
  drag = gtk_drag_source_new ();
  gtk_event_controller_set_name (GTK_EVENT_CONTROLLER (drag), "bimforge-tree-drag");
  gtk_drag_source_set_actions (drag, GDK_ACTION_ALL);
  gtk_event_controller_set_propagation_phase (GTK_EVENT_CONTROLLER (drag),
                                              GTK_PHASE_CAPTURE);
  g_signal_connect_object (drag,
                           "prepare",
                           G_CALLBACK (bimforge_tree_drag_source_prepare_cb),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (drag,
                           "drag-begin",
                           G_CALLBACK (bimforge_tree_drag_source_drag_begin_cb),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (drag,
                           "drag-end",
                           G_CALLBACK (bimforge_tree_drag_source_drag_end_cb),
                           self,
                           G_CONNECT_SWAPPED);
  gtk_widget_add_controller (GTK_WIDGET (expander), GTK_EVENT_CONTROLLER (drag));

  /* Setup drop site for this row */
  drop = gtk_drop_target_new (G_TYPE_INVALID, GDK_ACTION_ALL);
  gtk_drop_target_set_preload (drop, TRUE);
  gtk_event_controller_set_name (GTK_EVENT_CONTROLLER (drop), "bimforge-tree-drop");
  gtk_event_controller_set_propagation_phase (GTK_EVENT_CONTROLLER (drop),
                                              GTK_PHASE_CAPTURE);
  g_signal_connect_object (drop,
                           "accept",
                           G_CALLBACK (bimforge_tree_drop_target_accept_cb),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (drop,
                           "enter",
                           G_CALLBACK (bimforge_tree_drop_target_enter_cb),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (drop,
                           "leave",
                           G_CALLBACK (bimforge_tree_drop_target_leave_cb),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (drop,
                           "motion",
                           G_CALLBACK (bimforge_tree_drop_target_motion_cb),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (drop,
                           "notify::value",
                           G_CALLBACK (bimforge_tree_drop_target_notify_value_cb),
                           self,
                           G_CONNECT_SWAPPED);
  g_signal_connect_object (drop,
                           "drop",
                           G_CALLBACK (bimforge_tree_drop_target_drop_cb),
                           self,
                           G_CONNECT_SWAPPED);
  gtk_widget_add_controller (GTK_WIDGET (expander), GTK_EVENT_CONTROLLER (drop));
}

static void
bimforge_tree_list_item_teardown_cb (BimforgeTree               *self,
                                   GtkListItem              *item,
                                   GtkSignalListItemFactory *factory)
{
  g_assert (BIMFORGE_IS_TREE (self));
  g_assert (GTK_IS_LIST_ITEM (item));
  g_assert (GTK_IS_SIGNAL_LIST_ITEM_FACTORY (factory));

  gtk_list_item_set_child (item, NULL);
}

static void
bimforge_tree_row_notify_expanded_cb (BimforgeTree     *self,
                                    GParamSpec     *pspec,
                                    GtkTreeListRow *row)
{
  BimforgeTreePrivate *priv = bimforge_tree_get_instance_private (self);
  g_autoptr(BimforgeTreeNode) node = NULL;

  g_assert (BIMFORGE_IS_TREE (self));
  g_assert (GTK_IS_TREE_LIST_ROW (row));

  node = gtk_tree_list_row_get_item (row);

  if (gtk_tree_list_row_get_expanded (row))
    {
      if (!_bimforge_tree_node_children_built (node))
        _bimforge_tree_node_expand_async (node, priv->addins, NULL, NULL, NULL);
    }
  else
    {
      if (node != NULL)
        _bimforge_tree_node_collapsed (node);
    }
}

static inline GIcon *
get_icon (GIcon      **icon,
          const char  *name)
{
  if G_UNLIKELY (*icon == NULL)
    *icon = g_themed_icon_new (name);
  return *icon;
}

static gboolean
flags_to_icon (GBinding     *binding,
               const GValue *from_value,
               GValue       *to_value,
               gpointer      user_data)
{
  static GIcon *changed_icon;
  static GIcon *added_icon;

  BimforgeTreeNodeFlags flags = g_value_get_flags (from_value);
  g_autoptr(GObject) suffix = g_binding_dup_target (binding);
  GIcon *icon;

  if (flags & BIMFORGE_TREE_NODE_FLAGS_ADDED)
    icon = get_icon (&added_icon, "builder-vcs-added-symbolic");
  else if (flags & BIMFORGE_TREE_NODE_FLAGS_CHANGED)
    icon = get_icon (&changed_icon, "builder-vcs-changed-symbolic");
  else
    icon = NULL;

  g_value_set_object (to_value, icon);
  gtk_widget_set_visible (GTK_WIDGET (suffix), icon != NULL);

  return TRUE;
}

static gboolean
bimforge_tree_attach_popover_to_row (BimforgeTreeNode     *node,
                                   GtkPopover         *popover,
                                   BimforgeTreeExpander *expander)
{
  BIMFORGE_ENTRY;

  g_assert (BIMFORGE_IS_TREE_NODE (node));
  g_assert (GTK_IS_POPOVER (popover));
  g_assert (BIMFORGE_IS_TREE_EXPANDER (expander));

  bimforge_tree_expander_show_popover (expander, popover);

  BIMFORGE_RETURN (TRUE);
}

static void
bimforge_tree_list_item_bind_cb (BimforgeTree               *self,
                               GtkListItem              *item,
                               GtkSignalListItemFactory *factory)
{
  g_autoptr(BimforgeTreeNode) node = NULL;
  BimforgeTreeExpander *expander;
  GtkTreeListRow *row;
  GtkWidget *suffix;

  g_assert (BIMFORGE_IS_TREE (self));
  g_assert (GTK_IS_LIST_ITEM (item));
  g_assert (GTK_IS_SIGNAL_LIST_ITEM_FACTORY (factory));

  row = GTK_TREE_LIST_ROW (gtk_list_item_get_item (item));
  expander = BIMFORGE_TREE_EXPANDER (gtk_list_item_get_child (item));
  node = gtk_tree_list_row_get_item (row);
  suffix = bimforge_tree_expander_get_suffix (expander);

  g_assert (GTK_IS_TREE_LIST_ROW (row));
  g_assert (BIMFORGE_IS_TREE_EXPANDER (expander));
  g_assert (BIMFORGE_IS_TREE_NODE (node));

  bimforge_tree_expander_set_list_row (expander, row);

#define BIND_PROPERTY(name, to) \
  G_STMT_START { \
    GBinding *binding = g_object_bind_property (node, name, expander, to, G_BINDING_SYNC_CREATE); \
    g_object_set_data_full (G_OBJECT (expander), "BINDING_" name to, g_object_ref (binding), g_object_unref); \
  } G_STMT_END

  BIND_PROPERTY ("expanded-icon", "expanded-icon");
  BIND_PROPERTY ("icon", "icon");
  BIND_PROPERTY ("title", "title");
  BIND_PROPERTY ("title", "tooltip-text");
  BIND_PROPERTY ("use-markup", "use-markup");

  g_object_set_data_full (G_OBJECT (expander),
                          "BINDING_flagsflags",
                          g_object_ref (g_object_bind_property_full (node, "flags",
                                                                     suffix, "gicon",
                                                                     G_BINDING_SYNC_CREATE,
                                                                     flags_to_icon, NULL, NULL, NULL)),
                          g_object_unref);

#undef BIND_PROPERTY

  g_signal_connect_object (row,
                           "notify::expanded",
                           G_CALLBACK (bimforge_tree_row_notify_expanded_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_signal_connect_object (node,
                           "show-popover",
                           G_CALLBACK (bimforge_tree_attach_popover_to_row),
                           expander,
                           0);
}

static void
bimforge_tree_list_item_unbind_cb (BimforgeTree               *self,
                                 GtkListItem              *item,
                                 GtkSignalListItemFactory *factory)
{
  g_autoptr(BimforgeTreeNode) node = NULL;
  BimforgeTreeExpander *expander;
  GtkTreeListRow *row;

  g_assert (BIMFORGE_IS_TREE (self));
  g_assert (GTK_IS_LIST_ITEM (item));
  g_assert (GTK_IS_SIGNAL_LIST_ITEM_FACTORY (factory));

  row = GTK_TREE_LIST_ROW (gtk_list_item_get_item (item));
  expander = BIMFORGE_TREE_EXPANDER (gtk_list_item_get_child (item));
  node = gtk_tree_list_row_get_item (row);

  if (node != NULL)
    g_signal_handlers_disconnect_by_func (node,
                                          G_CALLBACK (bimforge_tree_attach_popover_to_row),
                                          expander);

  g_signal_handlers_disconnect_by_func (row,
                                        G_CALLBACK (bimforge_tree_row_notify_expanded_cb),
                                        self);

#define UNBIND_PROPERTY(name, to) \
  G_STMT_START { \
    GBinding *binding = g_object_steal_data (G_OBJECT (expander), "BINDING_" name); \
    if (binding != NULL) \
      { \
        g_binding_unbind (binding); \
        g_object_unref (binding); \
      } \
  } G_STMT_END

  UNBIND_PROPERTY ("expanded-icon", "expanded-icon");
  UNBIND_PROPERTY ("icon", "icon");
  UNBIND_PROPERTY ("title", "title");
  UNBIND_PROPERTY ("title", "tooltip-text");
  UNBIND_PROPERTY ("use-markup", "use-markup");
  UNBIND_PROPERTY ("flags", "flags");

#undef UNBIND_PROPERTY

  g_object_set (expander,
                "expanded-icon", NULL,
                "icon", NULL,
                "title", NULL,
                "use-markup", FALSE,
                NULL);

  bimforge_tree_expander_set_list_row (expander, NULL);
}

static void
invalidate_all_action (GtkWidget  *widget,
                       const char *action_name,
                       GVariant   *param)
{
  bimforge_tree_invalidate_all (BIMFORGE_TREE (widget));
}

static void
bimforge_tree_dispose (GObject *object)
{
  BimforgeTree *self = (BimforgeTree *)object;
  BimforgeTreePrivate *priv = bimforge_tree_get_instance_private (self);

  g_clear_object (&priv->addins);

  g_clear_pointer ((GtkWidget **)&priv->scroller, gtk_widget_unparent);

  bimforge_tree_set_root (self, NULL);

  if (priv->selection != NULL)
    gtk_single_selection_set_model (priv->selection, NULL);

  g_clear_object (&priv->tree_model);
  g_clear_object (&priv->menu_model);

  g_clear_pointer (&priv->kind, g_free);

  G_OBJECT_CLASS (bimforge_tree_parent_class)->dispose (object);
}

static void
bimforge_tree_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  BimforgeTree *self = BIMFORGE_TREE (object);
  BimforgeTreePrivate *priv = bimforge_tree_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_KIND:
      g_value_set_string (value, priv->kind);
      break;

    case PROP_MENU_MODEL:
      g_value_set_object (value, bimforge_tree_get_menu_model (self));
      break;

    case PROP_ROOT:
      g_value_set_object (value, bimforge_tree_get_root (self));
      break;

    case PROP_SELECTED_NODE:
      g_value_set_object (value, bimforge_tree_get_selected_node (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bimforge_tree_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  BimforgeTree *self = BIMFORGE_TREE (object);
  BimforgeTreePrivate *priv = bimforge_tree_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_KIND:
      priv->kind = g_value_dup_string (value);
      break;

    case PROP_MENU_MODEL:
      bimforge_tree_set_menu_model (self, g_value_get_object (value));
      break;

    case PROP_ROOT:
      bimforge_tree_set_root (self, g_value_get_object (value));
      break;

    case PROP_SELECTED_NODE:
      bimforge_tree_set_selected_node (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
bimforge_tree_class_init (BimforgeTreeClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = bimforge_tree_dispose;
  object_class->get_property = bimforge_tree_get_property;
  object_class->set_property = bimforge_tree_set_property;

  widget_class->root = bimforge_tree_root;

  properties[PROP_KIND] =
    g_param_spec_string ("kind", NULL, NULL,
                         NULL,
                         (G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT_ONLY |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_MENU_MODEL] =
    g_param_spec_object ("menu-model", NULL, NULL,
                         G_TYPE_MENU_MODEL,
                         (G_PARAM_READWRITE |
                          G_PARAM_EXPLICIT_NOTIFY |
                          G_PARAM_STATIC_STRINGS));

  properties[PROP_ROOT] =
    g_param_spec_object ("root", NULL, NULL,
                         BIMFORGE_TYPE_TREE_NODE,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_SELECTED_NODE] =
    g_param_spec_object ("selected-node", NULL, NULL,
                         BIMFORGE_TYPE_TREE_NODE,
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/io/sam/libbimforge/tree/bimforge-tree.ui");
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
  gtk_widget_class_bind_template_child_private (widget_class, BimforgeTree, list_view);
  gtk_widget_class_bind_template_child_private (widget_class, BimforgeTree, selection);
  gtk_widget_class_bind_template_child_private (widget_class, BimforgeTree, scroller);

  gtk_widget_class_bind_template_callback (widget_class, bimforge_tree_activate_cb);
  gtk_widget_class_bind_template_callback (widget_class, bimforge_tree_notify_selected_cb);
  gtk_widget_class_bind_template_callback (widget_class, bimforge_tree_list_item_bind_cb);
  gtk_widget_class_bind_template_callback (widget_class, bimforge_tree_list_item_unbind_cb);
  gtk_widget_class_bind_template_callback (widget_class, bimforge_tree_list_item_setup_cb);
  gtk_widget_class_bind_template_callback (widget_class, bimforge_tree_list_item_teardown_cb);

  gtk_widget_class_install_action (widget_class, "tree.invalidate-all", NULL, invalidate_all_action);
}

static void
bimforge_tree_init (BimforgeTree *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

static GListModel *
bimforge_tree_create_child_model_cb (gpointer item,
                                   gpointer user_data)
{
  BimforgeTreeNode *node = item;

  g_assert (BIMFORGE_IS_TREE_NODE (node));
  g_assert (user_data == NULL);

  if (bimforge_tree_node_get_children_possible (node))
    return bimforge_tree_empty_new (node);

  return NULL;
}

/**
 * bimforge_tree_get_root:
 * @self: a #BimforgeTree
 *
 * Gets the root node.
 *
 * Returns: (transfer none) (nullable): an BimforgeTreeNode or %NULL
 */
BimforgeTreeNode *
bimforge_tree_get_root (BimforgeTree *self)
{
  BimforgeTreePrivate *priv = bimforge_tree_get_instance_private (self);

  g_return_val_if_fail (BIMFORGE_IS_TREE (self), NULL);

  return priv->root;
}

void
bimforge_tree_set_root (BimforgeTree     *self,
                      BimforgeTreeNode *root)
{
  BimforgeTreePrivate *priv = bimforge_tree_get_instance_private (self);

  g_return_if_fail (BIMFORGE_IS_TREE (self));
  g_return_if_fail (!root || BIMFORGE_IS_TREE_NODE (root));

  if (priv->root == root)
    return;

  gtk_single_selection_set_model (priv->selection, NULL);
  g_clear_object (&priv->tree_model);

  if (priv->root != NULL)
    {
      g_object_set_data (G_OBJECT (priv->root), "BIMFORGE_TREE", NULL);
      g_clear_object (&priv->root);
    }

  g_set_object (&priv->root, root);

  if (priv->root != NULL)
    {
      GListModel *base_model = G_LIST_MODEL (priv->root);

      g_object_set_data (G_OBJECT (priv->root), "BIMFORGE_TREE", self);

      priv->tree_model = gtk_tree_list_model_new (g_object_ref (base_model),
                                                  FALSE, /* Passthrough */
                                                  FALSE,  /* Autoexpand */
                                                  bimforge_tree_create_child_model_cb,
                                                  NULL, NULL);
      gtk_single_selection_set_model (priv->selection, G_LIST_MODEL (priv->tree_model));

      if (priv->addins != NULL)
        _bimforge_tree_node_expand_async (priv->root, priv->addins, NULL, NULL, NULL);
    }

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ROOT]);
}

void
bimforge_tree_show_popover_at_node (BimforgeTree     *self,
                                  BimforgeTreeNode *node,
                                  GtkPopover     *popover)
{
  BimforgeTreePrivate *priv = bimforge_tree_get_instance_private (self);
  GtkTreeListRow *row;

  g_return_if_fail (BIMFORGE_IS_TREE (self));
  g_return_if_fail (BIMFORGE_IS_TREE_NODE (node));
  g_return_if_fail (GTK_IS_POPOVER (popover));

  if ((row = _bimforge_tree_get_row_at_node (self, node, TRUE)))
    {
      guint position = gtk_tree_list_row_get_position (row);

      gtk_widget_activate_action (GTK_WIDGET (priv->list_view), "list.scroll-to-item", "u", position);

      if (!_bimforge_tree_node_show_popover (node, popover))
        {
          g_warning ("Failed to show popover, no signal handler consumed popover!");
          g_object_ref_sink (popover);
          g_object_unref (popover);
        }
    }
}

static GtkTreeListRow *
_bimforge_tree_get_row_at_node_recurse (BimforgeTree     *self,
                                      BimforgeTreeNode *node,
                                      gboolean        expand_to_node)
{
  BimforgeTreePrivate *priv = bimforge_tree_get_instance_private (self);
  g_autoptr(GtkTreeListRow) row = NULL;
  BimforgeTreeNode *parent;
  guint index;

  g_assert (BIMFORGE_IS_TREE (self));
  g_assert (BIMFORGE_IS_TREE_NODE (node));

  /* The root node cannot have a GtkTreeListRow */
  if (!(parent = bimforge_tree_node_get_parent (node)))
    return NULL;

  /* Get our index for offset use within models */
  index = _bimforge_tree_node_get_child_index (parent, node);

  /* Handle children of the root specially by getting their
   * row from the GtkTreeListModel.
   */
  if (parent == priv->root)
    return gtk_tree_list_model_get_child_row (priv->tree_model, index);

  /* Expand the parent row and use the resulting row to locate
   * the child within that.
   */
  if ((row = _bimforge_tree_get_row_at_node_recurse (self, parent, expand_to_node)))
    {
      if (expand_to_node)
        gtk_tree_list_row_set_expanded (row, TRUE);
      return gtk_tree_list_row_get_child_row (row, index);
    }

  /* Failed to get row, probably due to something not expanded */
  return NULL;
}

GtkTreeListRow *
_bimforge_tree_get_row_at_node (BimforgeTree     *self,
                              BimforgeTreeNode *node,
                              gboolean        expand_to_node)
{
  g_return_val_if_fail (BIMFORGE_IS_TREE (self), NULL);
  g_return_val_if_fail (!node || BIMFORGE_IS_TREE_NODE (node), NULL);

  if (node == NULL)
    return NULL;

  return _bimforge_tree_get_row_at_node_recurse (self, node, expand_to_node);
}

gboolean
bimforge_tree_is_node_expanded (BimforgeTree     *self,
                              BimforgeTreeNode *node)
{
  g_autoptr(GtkTreeListRow) row = NULL;

  g_return_val_if_fail (BIMFORGE_IS_TREE (self), FALSE);
  g_return_val_if_fail (BIMFORGE_IS_TREE_NODE (node), FALSE);

  if ((row = _bimforge_tree_get_row_at_node (self, node, FALSE)))
    return gtk_tree_list_row_get_expanded (row);

  return FALSE;
}

/**
 * bimforge_tree_get_selected_node:
 * @self: a #BimforgeTree
 *
 * Gets the selected item.
 *
 * Returns: (transfer none) (nullable): an #BimforgeTreeNode or %NULL
 */
BimforgeTreeNode *
bimforge_tree_get_selected_node (BimforgeTree *self)
{
  BimforgeTreePrivate *priv = bimforge_tree_get_instance_private (self);
  GtkTreeListRow *row;

  g_return_val_if_fail (BIMFORGE_IS_TREE (self), NULL);

  if ((row = gtk_single_selection_get_selected_item (priv->selection)))
    {
      g_autoptr(GObject) item = gtk_tree_list_row_get_item (row);

      g_assert (BIMFORGE_IS_TREE_NODE (item));

      /* Return borrowed instance, which we are sure will stick
       * around after the unref as it's part of node tree.
       */
      return BIMFORGE_TREE_NODE (item);
    }

  return NULL;
}

/**
 * bimforge_tree_set_selected_node:
 * @self: a #BimforgeTree
 * @node: (nullable): an #BimforgeTreeNode or %NULL
 *
 * Sets the selected item in the tree.
 *
 * If @node is %NULL, the current selection is cleared.
 */
void
bimforge_tree_set_selected_node (BimforgeTree     *self,
                               BimforgeTreeNode *node)
{
  BimforgeTreePrivate *priv = bimforge_tree_get_instance_private (self);
  g_autoptr(GtkTreeListRow) row = NULL;
  guint position = GTK_INVALID_LIST_POSITION;

  g_return_if_fail (BIMFORGE_IS_MAIN_THREAD ());
  g_return_if_fail (BIMFORGE_IS_TREE (self));
  g_return_if_fail (!node || BIMFORGE_IS_TREE_NODE (node));

  if ((row = _bimforge_tree_get_row_at_node (self, node, TRUE)))
    position = gtk_tree_list_row_get_position (row);

  gtk_single_selection_set_selected (priv->selection, position);

  gtk_widget_activate_action (GTK_WIDGET (priv->list_view), "list.scroll-to-item", "u", position);
}

void
bimforge_tree_invalidate_all (BimforgeTree *self)
{
  BimforgeTreeNode *root;

  BIMFORGE_ENTRY;

  g_return_if_fail (BIMFORGE_IS_MAIN_THREAD ());
  g_return_if_fail (BIMFORGE_IS_TREE (self));

  if (!(root = bimforge_tree_get_root (self)))
    BIMFORGE_EXIT;

  g_object_ref (root);
  bimforge_tree_set_root (self, NULL);
  bimforge_tree_set_root (self, root);
  g_object_unref (root);

  BIMFORGE_EXIT;
}

void
bimforge_tree_expand_to_node (BimforgeTree     *self,
                            BimforgeTreeNode *node)
{
  g_autoptr(GtkTreeListRow) row = NULL;

  g_return_if_fail (BIMFORGE_IS_TREE (self));
  g_return_if_fail (BIMFORGE_IS_TREE_NODE (node));

  row = _bimforge_tree_get_row_at_node (self, node, TRUE);
}

static void
bimforge_tree_expand_node_cb (GObject      *object,
                            GAsyncResult *result,
                            gpointer      user_data)
{
  BimforgeTreeNode *node = (BimforgeTreeNode *)object;
  g_autoptr(GtkTreeListRow) row = NULL;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  BimforgeTree *self;

  BIMFORGE_ENTRY;

  g_assert (BIMFORGE_IS_TREE_NODE (node));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!_bimforge_tree_node_expand_finish (node, result, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      BIMFORGE_EXIT;
    }

  self = g_task_get_source_object (task);

  if ((row = _bimforge_tree_get_row_at_node (self, node, TRUE)))
    gtk_tree_list_row_set_expanded (row, TRUE);

  g_task_return_boolean (task, TRUE);

  BIMFORGE_EXIT;
}

void
bimforge_tree_expand_node_async (BimforgeTree          *self,
                               BimforgeTreeNode      *node,
                               GCancellable        *cancellable,
                               GAsyncReadyCallback  callback,
                               gpointer             user_data)
{
  BimforgeTreePrivate *priv = bimforge_tree_get_instance_private (self);
  g_autoptr(GTask) task = NULL;

  g_return_if_fail (BIMFORGE_IS_TREE (self));
  g_return_if_fail (BIMFORGE_IS_TREE_NODE (node));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, bimforge_tree_expand_node_async);

  _bimforge_tree_node_expand_async (node,
                               priv->addins,
                               cancellable,
                               bimforge_tree_expand_node_cb,
                               g_steal_pointer (&task));
}

gboolean
bimforge_tree_expand_node_finish (BimforgeTree    *self,
                                GAsyncResult  *result,
                                GError       **error)
{
  g_return_val_if_fail (BIMFORGE_IS_TREE (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

void
bimforge_tree_expand_node (BimforgeTree     *self,
                         BimforgeTreeNode *node)
{
  g_return_if_fail (BIMFORGE_IS_TREE (self));
  g_return_if_fail (BIMFORGE_IS_TREE_NODE (node));

  bimforge_tree_expand_node_async (self, node, NULL, NULL, NULL);
}

void
bimforge_tree_collapse_node (BimforgeTree     *self,
                           BimforgeTreeNode *node)
{
  g_autoptr(GtkTreeListRow) row = NULL;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (BIMFORGE_IS_TREE (self));
  g_assert (BIMFORGE_IS_TREE_NODE (node));
  g_assert (node != bimforge_tree_get_root (self));

  if ((row = _bimforge_tree_get_row_at_node (self, node, FALSE)))
    gtk_tree_list_row_set_expanded (row, FALSE);
}

/**
 * bimforge_tree_get_menu_model:
 * @self: a #BimforgeTree
 *
 * Gets the menu model for the tree.
 *
 * Returns: (transfer none) (nullable): a #GMenuModel or %NULL
 */
GMenuModel *
bimforge_tree_get_menu_model (BimforgeTree *self)
{
  BimforgeTreePrivate *priv = bimforge_tree_get_instance_private (self);

  g_return_val_if_fail (BIMFORGE_IS_TREE (self), NULL);

  return priv->menu_model;
}

void
bimforge_tree_set_menu_model (BimforgeTree *self,
                            GMenuModel *menu_model)
{
  BimforgeTreePrivate *priv = bimforge_tree_get_instance_private (self);

  g_return_if_fail (BIMFORGE_IS_TREE (self));
  g_return_if_fail (!menu_model || G_IS_MENU_MODEL (menu_model));

  if (g_set_object (&priv->menu_model, menu_model))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_MENU_MODEL]);
}

