/* bimforge-tree.h
 *
 * Copyright 2018-2022 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(BIMFORGE_INSIDE) && !defined(BIMFORGE_COMPILATION)
#error "Only <libbimforge.h> can be included directly."
#endif

#include <gtk/gtk.h>

#include "bimforge-tree-node.h"
#include "bimforge-version-macros.h"

G_BEGIN_DECLS

#define BIMFORGE_TYPE_TREE (bimforge_tree_get_type())

BIMFORGE_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (BimforgeTree, bimforge_tree, BIMFORGE, TREE, GtkWidget)

struct _BimforgeTreeClass
{
  GtkWidgetClass parent_class;
};

BIMFORGE_AVAILABLE_IN_ALL
BimforgeTree *     bimforge_tree_new                  (void) G_GNUC_WARN_UNUSED_RESULT;
BIMFORGE_AVAILABLE_IN_ALL
BimforgeTreeNode * bimforge_tree_get_root             (BimforgeTree *        self);
BIMFORGE_AVAILABLE_IN_ALL
void             bimforge_tree_set_root             (BimforgeTree *        self,
                                                   BimforgeTreeNode *    root);
BIMFORGE_AVAILABLE_IN_ALL
GMenuModel  *    bimforge_tree_get_menu_model       (BimforgeTree *        self);
BIMFORGE_AVAILABLE_IN_ALL
void             bimforge_tree_set_menu_model       (BimforgeTree *        self,
                                                   GMenuModel *        menu_model);
BIMFORGE_AVAILABLE_IN_ALL
void             bimforge_tree_show_popover_at_node (BimforgeTree *        self,
                                                   BimforgeTreeNode *    node,
                                                   GtkPopover *        popover);
BIMFORGE_AVAILABLE_IN_ALL
gboolean         bimforge_tree_is_node_expanded     (BimforgeTree *        self,
                                                   BimforgeTreeNode *    node);
BIMFORGE_AVAILABLE_IN_ALL
void             bimforge_tree_collapse_node        (BimforgeTree *        self,
                                                   BimforgeTreeNode *    node);
BIMFORGE_AVAILABLE_IN_ALL
void             bimforge_tree_expand_to_node       (BimforgeTree *        self,
                                                   BimforgeTreeNode *    node);
BIMFORGE_AVAILABLE_IN_ALL
void             bimforge_tree_expand_node          (BimforgeTree *        self,
                                                   BimforgeTreeNode *    node);
BIMFORGE_AVAILABLE_IN_ALL
void             bimforge_tree_expand_node_async    (BimforgeTree *        self,
                                                   BimforgeTreeNode *    node,
                                                   GCancellable *      cancellable,
                                                   GAsyncReadyCallback callback,
                                                   gpointer            user_data);
BIMFORGE_AVAILABLE_IN_ALL
gboolean         bimforge_tree_expand_node_finish   (BimforgeTree *        self,
                                                   GAsyncResult *      result,
                                                   GError **           error);
BIMFORGE_AVAILABLE_IN_ALL
BimforgeTreeNode * bimforge_tree_get_selected_node    (BimforgeTree *        self);
BIMFORGE_AVAILABLE_IN_ALL
void             bimforge_tree_set_selected_node    (BimforgeTree *        self,
                                                   BimforgeTreeNode *    node);
BIMFORGE_AVAILABLE_IN_ALL
void             bimforge_tree_invalidate_all       (BimforgeTree *        self);

G_END_DECLS
