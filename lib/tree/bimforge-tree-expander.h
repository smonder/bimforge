/* bimforge-tree-expander.h
 *
 * Copyright 2022 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

#include "bimforge-version-macros.h"

G_BEGIN_DECLS

#define BIMFORGE_TYPE_TREE_EXPANDER (bimforge_tree_expander_get_type())

BIMFORGE_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (BimforgeTreeExpander, bimforge_tree_expander, BIMFORGE, TREE_EXPANDER, GtkWidget)

BIMFORGE_AVAILABLE_IN_ALL
GtkWidget *      bimforge_tree_expander_new                    (void) G_GNUC_WARN_UNUSED_RESULT;
BIMFORGE_AVAILABLE_IN_ALL
GMenuModel *     bimforge_tree_expander_get_menu_model         (BimforgeTreeExpander * self);
BIMFORGE_AVAILABLE_IN_ALL
void             bimforge_tree_expander_set_menu_model         (BimforgeTreeExpander * self,
                                                              GMenuModel *         menu_model);
BIMFORGE_AVAILABLE_IN_ALL
GIcon *          bimforge_tree_expander_get_icon               (BimforgeTreeExpander * self);
BIMFORGE_AVAILABLE_IN_ALL
void             bimforge_tree_expander_set_icon               (BimforgeTreeExpander * self,
                                                              GIcon *              icon);
BIMFORGE_AVAILABLE_IN_ALL
void             bimforge_tree_expander_set_icon_name          (BimforgeTreeExpander * self,
                                                              const char *         icon_name);
BIMFORGE_AVAILABLE_IN_ALL
GIcon *          bimforge_tree_expander_get_expanded_icon      (BimforgeTreeExpander * self);
BIMFORGE_AVAILABLE_IN_ALL
void             bimforge_tree_expander_set_expanded_icon      (BimforgeTreeExpander * self,
                                                              GIcon *              icon);
BIMFORGE_AVAILABLE_IN_ALL
void             bimforge_tree_expander_set_expanded_icon_name (BimforgeTreeExpander * self,
                                                              const char *         expanded_icon_name);
BIMFORGE_AVAILABLE_IN_ALL
const char *     bimforge_tree_expander_get_title              (BimforgeTreeExpander * self);
BIMFORGE_AVAILABLE_IN_ALL
void             bimforge_tree_expander_set_title              (BimforgeTreeExpander * self,
                                                              const char *         title);
BIMFORGE_AVAILABLE_IN_ALL
GtkWidget *      bimforge_tree_expander_get_suffix             (BimforgeTreeExpander * self);
BIMFORGE_AVAILABLE_IN_ALL
void             bimforge_tree_expander_set_suffix             (BimforgeTreeExpander * self,
                                                              GtkWidget *          suffix);
BIMFORGE_AVAILABLE_IN_ALL
GtkTreeListRow * bimforge_tree_expander_get_list_row           (BimforgeTreeExpander * self);
BIMFORGE_AVAILABLE_IN_ALL
void             bimforge_tree_expander_set_list_row           (BimforgeTreeExpander * self,
                                                              GtkTreeListRow *     list_row);
BIMFORGE_AVAILABLE_IN_ALL
gpointer         bimforge_tree_expander_get_item               (BimforgeTreeExpander * self);
BIMFORGE_AVAILABLE_IN_ALL
gboolean         bimforge_tree_expander_get_use_markup         (BimforgeTreeExpander * self);
BIMFORGE_AVAILABLE_IN_ALL
void             bimforge_tree_expander_set_use_markup         (BimforgeTreeExpander * self,
                                                              gboolean             use_markup);
BIMFORGE_AVAILABLE_IN_ALL
void             bimforge_tree_expander_show_popover           (BimforgeTreeExpander * self,
                                                              GtkPopover *         popover);

G_END_DECLS
