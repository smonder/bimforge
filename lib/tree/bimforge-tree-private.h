/* bimforge-tree-private.h
 *
 * Copyright 2018-2023 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <libpeas.h>

#include "bimforge-tree.h"
#include "bimforge-tree-node.h"

G_BEGIN_DECLS

GtkTreeListRow * _bimforge_tree_get_row_at_node      (BimforgeTree *         self,
                                                    BimforgeTreeNode *     node,
                                                    gboolean             expand_to_row);
gboolean         _bimforge_tree_node_children_built  (BimforgeTreeNode *     self);
guint            _bimforge_tree_node_get_child_index (BimforgeTreeNode *     parent,
                                                    BimforgeTreeNode *     child);
BimforgeTree *    _bimforge_tree_node_get_tree         (BimforgeTreeNode *     self);
void            _bimforge_tree_node_collapsed        (BimforgeTreeNode *     self);
void            _bimforge_tree_node_expand_async     (BimforgeTreeNode *     self,
                                                    PeasExtensionSet *   addins,
                                                    GCancellable *       cancellable,
                                                    GAsyncReadyCallback  callback,
                                                    gpointer             user_data);
gboolean        _bimforge_tree_node_expand_finish    (BimforgeTreeNode *     self,
                                                    GAsyncResult *       result,
                                                    GError **            error);
gboolean        _bimforge_tree_node_show_popover     (BimforgeTreeNode *     self,
                                                    GtkPopover *         popover);

G_END_DECLS
