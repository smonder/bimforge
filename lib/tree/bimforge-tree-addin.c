/* bimforge-tree-addin.c
 *
 * Copyright 2018-2023 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "bimforge-tree-addin"

#include "config.h"
#include "bimforge-debug.h"

#include "bimforge-tree-addin.h"
#include "application/bimforge-app-global.h"

G_DEFINE_INTERFACE (BimforgeTreeAddin, bimforge_tree_addin, G_TYPE_OBJECT)

static void
bimforge_tree_addin_real_build_children_async (BimforgeTreeAddin     *self,
                                             BimforgeTreeNode      *node,
                                             GCancellable        *cancellable,
                                             GAsyncReadyCallback  callback,
                                             gpointer             user_data)
{
  g_autoptr (GTask) task = NULL;

  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (BIMFORGE_IS_TREE_ADDIN (self));
  g_assert (BIMFORGE_IS_TREE_NODE (node));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, bimforge_tree_addin_real_build_children_async);

  if (BIMFORGE_TREE_ADDIN_GET_IFACE (self)->build_children)
    BIMFORGE_TREE_ADDIN_GET_IFACE (self)->build_children (self, node);

  g_task_return_boolean (task, TRUE);
}

static gboolean
bimforge_tree_addin_real_build_children_finish (BimforgeTreeAddin  *self,
                                              GAsyncResult     *result,
                                              GError          **error)
{
  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (BIMFORGE_IS_TREE_ADDIN (self));
  g_assert (G_IS_TASK (result));

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
bimforge_tree_addin_real_node_dropped_async (BimforgeTreeAddin     *self,
                                           GtkDropTarget       *drop_target,
                                           BimforgeTreeNode      *drop_node,
                                           GCancellable        *cancellable,
                                           GAsyncReadyCallback  callback,
                                           gpointer             user_data)
{
  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (BIMFORGE_IS_TREE_ADDIN (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  g_task_report_new_error (self, callback, user_data,
                           bimforge_tree_addin_real_node_dropped_async,
                           G_IO_ERROR,
                           G_IO_ERROR_NOT_SUPPORTED,
                           "Addin does not support dropping nodes");
}

static gboolean
bimforge_tree_addin_real_node_dropped_finish (BimforgeTreeAddin  *self,
                                            GAsyncResult     *result,
                                            GError          **error)
{
  g_assert (BIMFORGE_IS_MAIN_THREAD ());
  g_assert (BIMFORGE_IS_TREE_ADDIN (self));
  g_assert (G_IS_TASK (result));

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
bimforge_tree_addin_default_init (BimforgeTreeAddinInterface *iface)
{
  iface->build_children_async = bimforge_tree_addin_real_build_children_async;
  iface->build_children_finish = bimforge_tree_addin_real_build_children_finish;
  iface->node_dropped_async = bimforge_tree_addin_real_node_dropped_async;
  iface->node_dropped_finish = bimforge_tree_addin_real_node_dropped_finish;
}

/**
 * bimforge_tree_addin_build_children_async:
 * @self: a #BimforgeTreeAddin
 * @node: a #BimforgeTreeNode
 * @cancellable: (nullable): a #GCancellable or %NULL
 * @callback: a #GAsyncReadyCallback or %NULL
 * @user_data: user data for @callback
 *
 * This function is called when building the children of a node. This
 * happens when expanding an node that might have children, or building the
 * root node.
 *
 * You may want to use bimforge_tree_node_holds() to determine if the node
 * contains an item that you are interested in.
 *
 * This function will call the synchronous form of
 * BimforgeTreeAddin.build_children() if no asynchronous form is available.
 */
void
bimforge_tree_addin_build_children_async (BimforgeTreeAddin     *self,
                                        BimforgeTreeNode      *node,
                                        GCancellable        *cancellable,
                                        GAsyncReadyCallback  callback,
                                        gpointer             user_data)
{
  g_return_if_fail (BIMFORGE_IS_MAIN_THREAD ());
  g_return_if_fail (BIMFORGE_IS_TREE_ADDIN (self));
  g_return_if_fail (BIMFORGE_IS_TREE_NODE (node));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  BIMFORGE_TREE_ADDIN_GET_IFACE (self)->build_children_async (self, node, cancellable, callback, user_data);
}

/**
 * bimforge_tree_addin_build_children_finish:
 * @self: a #BimforgeTreeAddin
 * @result: result given to callback in bimforge_tree_addin_build_children_async()
 * @error: a location for a #GError, or %NULL
 *
 * Completes an asynchronous request to bimforge_tree_addin_build_children_async().
 *
 * Returns: %TRUE if successful; otherwise %FALSE and @error is set.
 */
gboolean
bimforge_tree_addin_build_children_finish (BimforgeTreeAddin  *self,
                                         GAsyncResult     *result,
                                         GError          **error)
{
  g_return_val_if_fail (BIMFORGE_IS_MAIN_THREAD (), FALSE);
  g_return_val_if_fail (BIMFORGE_IS_TREE_ADDIN (self), FALSE);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), FALSE);

  return BIMFORGE_TREE_ADDIN_GET_IFACE (self)->build_children_finish (self, result, error);
}

/**
 * bimforge_tree_addin_build_node:
 * @self: a #BimforgeTreeAddin
 * @node: a #BimforgeTreeNode
 *
 * This function is called when preparing a node for display in the tree.
 *
 * Addins should adjust any state on the node that makes sense based on the
 * addin.
 *
 * You may want to use bimforge_tree_node_holds() to determine if the node
 * contains an item that you are interested in.
 */
void
bimforge_tree_addin_build_node (BimforgeTreeAddin *self,
                              BimforgeTreeNode  *node)
{
  g_return_if_fail (BIMFORGE_IS_MAIN_THREAD ());
  g_return_if_fail (BIMFORGE_IS_TREE_ADDIN (self));
  g_return_if_fail (BIMFORGE_IS_TREE_NODE (node));

  if (BIMFORGE_TREE_ADDIN_GET_IFACE (self)->build_node)
    BIMFORGE_TREE_ADDIN_GET_IFACE (self)->build_node (self, node);
}

/**
 * bimforge_tree_addin_activated:
 * @self: an #BimforgeTreeAddin
 * @tree: an #BimforgeTree
 * @node: an #BimforgeTreeNode
 *
 * This function is called when a node has been activated in the tree
 * and allows for the addin to perform any necessary operations in response
 * to that.
 *
 * If the addin performs an action based on the activation request, then it
 * should return %TRUE from this function so that no further addins may
 * respond to the action.
 *
 * Returns: %TRUE if the activation was handled, otherwise %FALSE
 */
gboolean
bimforge_tree_addin_node_activated (BimforgeTreeAddin *self,
                                  BimforgeTree      *tree,
                                  BimforgeTreeNode  *node)
{
  g_return_val_if_fail (BIMFORGE_IS_MAIN_THREAD (), FALSE);
  g_return_val_if_fail (BIMFORGE_IS_TREE_ADDIN (self), FALSE);
  g_return_val_if_fail (BIMFORGE_IS_TREE (tree), FALSE);
  g_return_val_if_fail (BIMFORGE_IS_TREE_NODE (node), FALSE);

  if (BIMFORGE_TREE_ADDIN_GET_IFACE (self)->node_activated)
    return BIMFORGE_TREE_ADDIN_GET_IFACE (self)->node_activated (self, tree, node);

  return FALSE;
}

void
bimforge_tree_addin_load (BimforgeTreeAddin *self,
                        BimforgeTree      *tree)
{
  g_return_if_fail (BIMFORGE_IS_TREE_ADDIN (self));
  g_return_if_fail (BIMFORGE_IS_TREE (tree));

  if (BIMFORGE_TREE_ADDIN_GET_IFACE (self)->load)
    BIMFORGE_TREE_ADDIN_GET_IFACE (self)->load (self, tree);
}

void
bimforge_tree_addin_unload (BimforgeTreeAddin *self,
                          BimforgeTree      *tree)
{
  g_return_if_fail (BIMFORGE_IS_TREE_ADDIN (self));
  g_return_if_fail (BIMFORGE_IS_TREE (tree));

  if (BIMFORGE_TREE_ADDIN_GET_IFACE (self)->unload)
    BIMFORGE_TREE_ADDIN_GET_IFACE (self)->unload (self, tree);
}

void
bimforge_tree_addin_selection_changed (BimforgeTreeAddin *self,
                                     BimforgeTreeNode  *selection)
{
  g_return_if_fail (BIMFORGE_IS_TREE_ADDIN (self));
  g_return_if_fail (!selection || BIMFORGE_IS_TREE_NODE (selection));

  if (BIMFORGE_TREE_ADDIN_GET_IFACE (self)->selection_changed)
    BIMFORGE_TREE_ADDIN_GET_IFACE (self)->selection_changed (self, selection);
}

void
bimforge_tree_addin_node_expanded (BimforgeTreeAddin *self,
                                 BimforgeTreeNode  *node)
{
  g_return_if_fail (BIMFORGE_IS_TREE_ADDIN (self));
  g_return_if_fail (BIMFORGE_IS_TREE_NODE (node));

  if (BIMFORGE_TREE_ADDIN_GET_IFACE (self)->node_expanded)
    BIMFORGE_TREE_ADDIN_GET_IFACE (self)->node_expanded (self, node);
}

void
bimforge_tree_addin_node_collapsed (BimforgeTreeAddin *self,
                                  BimforgeTreeNode  *node)
{
  g_return_if_fail (BIMFORGE_IS_TREE_ADDIN (self));
  g_return_if_fail (BIMFORGE_IS_TREE_NODE (node));

  if (BIMFORGE_TREE_ADDIN_GET_IFACE (self)->node_collapsed)
    BIMFORGE_TREE_ADDIN_GET_IFACE (self)->node_collapsed (self, node);
}

/**
 * bimforge_tree_addin_node_draggable:
 * @self: a #BimforgeTreeAddin
 * @node: an #BimforgeTreeNode
 *
 * Checks if a node is draggable.
 *
 * Returns: (transfer full) (nullable): %NULL or a #GdkContentProvider if
 *   the node is draggable.
 *
 * Since: 44
 */
GdkContentProvider *
bimforge_tree_addin_node_draggable (BimforgeTreeAddin *self,
                                  BimforgeTreeNode  *node)
{
  g_return_val_if_fail (BIMFORGE_IS_TREE_ADDIN (self), NULL);
  g_return_val_if_fail (BIMFORGE_IS_TREE_NODE (node), NULL);

  if (BIMFORGE_TREE_ADDIN_GET_IFACE (self)->node_draggable)
    return BIMFORGE_TREE_ADDIN_GET_IFACE (self)->node_draggable (self, node);

  return NULL;
}

/**
 * bimforge_tree_addin_node_droppable:
 * @self: an #BimforgeTreeAddin
 * @drop_target: a #GtkDropTarget
 * @drop_node: an #BimforgeTreeNode
 * @gtypes: (element-type GType): an array of #GType
 *
 * Determines if @drop_node is a droppable for @drop_target.
 *
 * If so, this function should add the allowed #GType to @gtypes.
 *
 * Returns: 0 if not droppable, otherwise a #GdkDragAction
 *
 * Since: 44
 */
GdkDragAction
bimforge_tree_addin_node_droppable (BimforgeTreeAddin *self,
                                  GtkDropTarget   *drop_target,
                                  BimforgeTreeNode  *drop_node,
                                  GArray          *gtypes)
{
  g_return_val_if_fail (BIMFORGE_IS_TREE_ADDIN (self), 0);
  g_return_val_if_fail (GTK_IS_DROP_TARGET (drop_target), 0);
  g_return_val_if_fail (BIMFORGE_IS_TREE_NODE (drop_node), 0);
  g_return_val_if_fail (gtypes != NULL, 0);

  if (BIMFORGE_TREE_ADDIN_GET_IFACE (self)->node_droppable)
    return BIMFORGE_TREE_ADDIN_GET_IFACE (self)->node_droppable (self, drop_target, drop_node, gtypes);

  return 0;
}

void
bimforge_tree_addin_node_dropped_async (BimforgeTreeAddin     *self,
                                      GtkDropTarget       *drop_target,
                                      BimforgeTreeNode      *drop_node,
                                      GCancellable        *cancellable,
                                      GAsyncReadyCallback  callback,
                                      gpointer             user_data)
{
  g_return_if_fail (BIMFORGE_IS_MAIN_THREAD ());
  g_return_if_fail (BIMFORGE_IS_TREE_ADDIN (self));
  g_return_if_fail (!drop_node || BIMFORGE_IS_TREE_NODE (drop_node));
  g_return_if_fail (GTK_IS_DROP_TARGET (drop_target));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  BIMFORGE_TREE_ADDIN_GET_IFACE (self)->node_dropped_async (self, drop_target, drop_node, cancellable, callback, user_data);
}

gboolean
bimforge_tree_addin_node_dropped_finish (BimforgeTreeAddin  *self,
                                       GAsyncResult     *result,
                                       GError          **error)
{
  g_return_val_if_fail (BIMFORGE_IS_MAIN_THREAD (), FALSE);
  g_return_val_if_fail (BIMFORGE_IS_TREE_ADDIN (self), FALSE);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), FALSE);

  return BIMFORGE_TREE_ADDIN_GET_IFACE (self)->node_dropped_finish (self, result, error);
}
