/* bimforge-tree-addin.h
 *
 * Copyright 2018-2022 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(BIMFORGE_INSIDE) && !defined(BIMFORGE_COMPILATION)
#error "Only <libbimforge.h> can be included directly."
#endif

#include "bimforge-tree.h"
#include "bimforge-tree-node.h"
#include "bimforge-version-macros.h"

G_BEGIN_DECLS

#define BIMFORGE_TYPE_TREE_ADDIN (bimforge_tree_addin_get_type())

BIMFORGE_AVAILABLE_IN_ALL
G_DECLARE_INTERFACE (BimforgeTreeAddin, bimforge_tree_addin, BIMFORGE, TREE_ADDIN, GObject)

struct _BimforgeTreeAddinInterface
{
  GTypeInterface parent;

  void                (*load)                  (BimforgeTreeAddin *   self,
                                                BimforgeTree *        tree);
  void                (*unload)                (BimforgeTreeAddin *   self,
                                                BimforgeTree *        tree);
  void                (*build_node)            (BimforgeTreeAddin *   self,
                                                BimforgeTreeNode *    node);
  void                (*build_children)        (BimforgeTreeAddin *   self,
                                                BimforgeTreeNode *    node);
  void                (*build_children_async)  (BimforgeTreeAddin *   self,
                                                BimforgeTreeNode *    node,
                                                GCancellable *      cancellable,
                                                GAsyncReadyCallback callback,
                                                gpointer            user_data);
  gboolean            (*build_children_finish) (BimforgeTreeAddin *   self,
                                                GAsyncResult *      result,
                                                GError **           error);
  gboolean            (*node_activated)        (BimforgeTreeAddin *   self,
                                                BimforgeTree *        tree,
                                                BimforgeTreeNode *    node);
  void                (*selection_changed)     (BimforgeTreeAddin *   self,
                                                BimforgeTreeNode *    selection);
  void                (*node_expanded)         (BimforgeTreeAddin *   self,
                                                BimforgeTreeNode *    node);
  void                (*node_collapsed)        (BimforgeTreeAddin *   self,
                                                BimforgeTreeNode *    node);
  GdkContentProvider *(*node_draggable)        (BimforgeTreeAddin *   self,
                                                BimforgeTreeNode *    node);
  GdkDragAction       (*node_droppable)        (BimforgeTreeAddin *   self,
                                                GtkDropTarget *     drop_target,
                                                BimforgeTreeNode *    drop_node,
                                                GArray *            gtypes);
  void                (*node_dropped_async)    (BimforgeTreeAddin *   self,
                                                GtkDropTarget *     drop_target,
                                                BimforgeTreeNode *    drop_node,
                                                GCancellable *      cancellable,
                                                GAsyncReadyCallback callback,
                                                gpointer            user_data);
  gboolean            (*node_dropped_finish)   (BimforgeTreeAddin *   self,
                                                GAsyncResult *      result,
                                                GError **           error);
};

BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_addin_load                  (BimforgeTreeAddin *   self,
                                                              BimforgeTree *        tree);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_addin_unload                (BimforgeTreeAddin *   self,
                                                              BimforgeTree *        tree);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_addin_build_node            (BimforgeTreeAddin *   self,
                                                              BimforgeTreeNode *    node);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_addin_build_children_async  (BimforgeTreeAddin *   self,
                                                              BimforgeTreeNode *    node,
                                                              GCancellable *      cancellable,
                                                              GAsyncReadyCallback callback,
                                                              gpointer            user_data);
BIMFORGE_AVAILABLE_IN_ALL
gboolean             bimforge_tree_addin_build_children_finish (BimforgeTreeAddin *   self,
                                                              GAsyncResult *      result,
                                                              GError **           error);
BIMFORGE_AVAILABLE_IN_ALL
gboolean             bimforge_tree_addin_node_activated        (BimforgeTreeAddin *   self,
                                                              BimforgeTree *        tree,
                                                              BimforgeTreeNode *    node);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_addin_selection_changed     (BimforgeTreeAddin *   self,
                                                              BimforgeTreeNode *    selection);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_addin_node_expanded         (BimforgeTreeAddin *   self,
                                                              BimforgeTreeNode *    node);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_addin_node_collapsed        (BimforgeTreeAddin *   self,
                                                              BimforgeTreeNode *    node);
BIMFORGE_AVAILABLE_IN_ALL
GdkContentProvider * bimforge_tree_addin_node_draggable        (BimforgeTreeAddin *   self,
                                                              BimforgeTreeNode *    node);
BIMFORGE_AVAILABLE_IN_ALL
GdkDragAction        bimforge_tree_addin_node_droppable        (BimforgeTreeAddin *   self,
                                                              GtkDropTarget *     drop_target,
                                                              BimforgeTreeNode *    drop_node,
                                                              GArray *            gtypes);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_addin_node_dropped_async    (BimforgeTreeAddin *   self,
                                                              GtkDropTarget *     drop_target,
                                                              BimforgeTreeNode *    drop_node,
                                                              GCancellable *      cancellable,
                                                              GAsyncReadyCallback callback,
                                                              gpointer            user_data);
BIMFORGE_AVAILABLE_IN_ALL
gboolean             bimforge_tree_addin_node_dropped_finish   (BimforgeTreeAddin *   self,
                                                              GAsyncResult *      result,
                                                              GError **           error);

G_END_DECLS
