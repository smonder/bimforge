/* bimforge-tree-node.h
 *
 * Copyright 2018-2022 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(BIMFORGE_INSIDE) && !defined(BIMFORGE_COMPILATION)
#error "Only <libbimforge.h> can be included directly."
#endif

#include <gio/gio.h>

#include "bimforge-types.h"
#include "bimforge-version-macros.h"

G_BEGIN_DECLS

#define BIMFORGE_TYPE_TREE_NODE (bimforge_tree_node_get_type())

BIMFORGE_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (BimforgeTreeNode, bimforge_tree_node, BIMFORGE, TREE_NODE, GObject)

/**
 * BimforgeTreeTraverseFunc:
 * @node: a #BimforgeTreeNode
 * @user_data: closure data provided to bimforge_tree_node_traverse()
 *
 * This function prototype is used to traverse a tree of #BimforgeTreeNode.
 *
 * Returns: #BimforgeTreeNodeVisit, %BIMFORGE_TREE_NODE_VISIT_BREAK to stop traversal.
 */
typedef BimforgeTreeNodeVisit (* BimforgeTreeTraverseFunc) (BimforgeTreeNode * node,
                                                        gpointer         user_data);

/**
 * BimforgeTreeNodeCompare:
 * @node: a #BimforgeTreeNode that iterate over children
 * @child: a #BimforgeTreeNode to be inserted
 *
 * This callback function is a convenience wrapper around GCompareFunc
 *
 * Returns: int
 */
typedef int (* BimforgeTreeNodeCompare) (BimforgeTreeNode * node,
                                       BimforgeTreeNode * child);

BIMFORGE_AVAILABLE_IN_ALL
BimforgeTreeNode *     bimforge_tree_node_new                    (void) G_GNUC_WARN_UNUSED_RESULT;
BIMFORGE_AVAILABLE_IN_ALL
guint                bimforge_tree_node_get_n_children         (BimforgeTreeNode *        self);
BIMFORGE_AVAILABLE_IN_ALL
const char *         bimforge_tree_node_get_title              (BimforgeTreeNode *        self);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_node_set_title              (BimforgeTreeNode *        self,
                                                              const char *            title);
BIMFORGE_AVAILABLE_IN_ALL
GIcon *              bimforge_tree_node_get_icon               (BimforgeTreeNode *        self);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_node_set_icon               (BimforgeTreeNode *        self,
                                                              GIcon *                 icon);
BIMFORGE_AVAILABLE_IN_ALL
GIcon *              bimforge_tree_node_get_expanded_icon      (BimforgeTreeNode *        self);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_node_set_expanded_icon      (BimforgeTreeNode *        self,
                                                              GIcon *                 expanded_icon);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_node_set_icon_name          (BimforgeTreeNode *        self,
                                                              const char *            icon_name);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_node_set_expanded_icon_name (BimforgeTreeNode *        self,
                                                              const char *            expanded_icon_name);
BIMFORGE_AVAILABLE_IN_ALL
BimforgeTreeNodeFlags  bimforge_tree_node_get_flags              (BimforgeTreeNode *        self);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_node_set_flags              (BimforgeTreeNode *        self,
                                                              BimforgeTreeNodeFlags     flags);
BIMFORGE_AVAILABLE_IN_ALL
gboolean             bimforge_tree_node_get_has_error          (BimforgeTreeNode *        self);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_node_set_has_error          (BimforgeTreeNode *        self,
                                                              gboolean                has_error);
BIMFORGE_AVAILABLE_IN_ALL
gboolean             bimforge_tree_node_get_destroy_item       (BimforgeTreeNode *        self);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_node_set_destroy_item       (BimforgeTreeNode *        self,
                                                              gboolean                destroy_item);
BIMFORGE_AVAILABLE_IN_ALL
gpointer             bimforge_tree_node_get_item               (BimforgeTreeNode *        self);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_node_set_item               (BimforgeTreeNode *        self,
                                                              gpointer                item);
BIMFORGE_AVAILABLE_IN_ALL
gboolean             bimforge_tree_node_get_children_possible  (BimforgeTreeNode *        self);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_node_set_children_possible  (BimforgeTreeNode *        self,
                                                              gboolean                children_possible);
BIMFORGE_AVAILABLE_IN_ALL
gboolean             bimforge_tree_node_get_reset_on_collapse  (BimforgeTreeNode *        self);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_node_set_reset_on_collapse  (BimforgeTreeNode *        self,
                                                              gboolean                reset_on_collapse);
BIMFORGE_AVAILABLE_IN_ALL
gboolean             bimforge_tree_node_get_use_markup         (BimforgeTreeNode *        self);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_node_set_use_markup         (BimforgeTreeNode *        self,
                                                              gboolean                use_markup);
BIMFORGE_AVAILABLE_IN_ALL
gboolean             bimforge_tree_node_get_is_header          (BimforgeTreeNode *        self);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_node_set_is_header          (BimforgeTreeNode *        self,
                                                              gboolean                is_header);
BIMFORGE_AVAILABLE_IN_ALL
BimforgeTreeNode *     bimforge_tree_node_get_first_child        (BimforgeTreeNode *        self);
BIMFORGE_AVAILABLE_IN_ALL
BimforgeTreeNode *     bimforge_tree_node_get_last_child         (BimforgeTreeNode *        self);
BIMFORGE_AVAILABLE_IN_ALL
BimforgeTreeNode *     bimforge_tree_node_get_prev_sibling       (BimforgeTreeNode *        self);
BIMFORGE_AVAILABLE_IN_ALL
BimforgeTreeNode *     bimforge_tree_node_get_next_sibling       (BimforgeTreeNode *        self);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_node_set_parent             (BimforgeTreeNode *        self,
                                                              BimforgeTreeNode *        node);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_node_remove                 (BimforgeTreeNode *        self,
                                                              BimforgeTreeNode *        child);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_node_unparent               (BimforgeTreeNode *        self);
BIMFORGE_AVAILABLE_IN_ALL
BimforgeTreeNode *     bimforge_tree_node_get_parent             (BimforgeTreeNode *        self);
BIMFORGE_AVAILABLE_IN_ALL
BimforgeTreeNode *     bimforge_tree_node_get_root               (BimforgeTreeNode *        self);
BIMFORGE_AVAILABLE_IN_ALL
gboolean             bimforge_tree_node_holds                  (BimforgeTreeNode *        self,
                                                              GType                   type);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_node_insert_after           (BimforgeTreeNode *        node,
                                                              BimforgeTreeNode *        parent,
                                                              BimforgeTreeNode *        previous_sibling);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_node_insert_before          (BimforgeTreeNode *        node,
                                                              BimforgeTreeNode *        parent,
                                                              BimforgeTreeNode *        next_sibling);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_node_insert_sorted          (BimforgeTreeNode *        self,
                                                              BimforgeTreeNode *        child,
                                                              BimforgeTreeNodeCompare   cmpfn);
BIMFORGE_AVAILABLE_IN_ALL
void                 bimforge_tree_node_traverse               (BimforgeTreeNode *        self,
                                                              GTraverseType           traverse_type,
                                                              GTraverseFlags          traverse_flags,
                                                              gint                    max_depth,
                                                              BimforgeTreeTraverseFunc  traverse_func,
                                                              gpointer                user_data);

G_END_DECLS
